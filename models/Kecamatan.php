<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "m_kecamatan".
 *
 * @property int $id
 * @property int $kabupaten_id
 * @property string $name
 */
class Kecamatan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'm_kecamatan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'kabupaten_id'], 'required'],
            [['id', 'kabupaten_id'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kabupaten_id' => 'Kabupaten ID',
            'name' => 'Name',
        ];
    }

    public static function getDataKecamatan($id_kabupaten) {
        $role = Yii::$app->user->identity->role->role;

        if ($role == 'super-admin' || $role == 'admin-regional') {
            return ArrayHelper::map(self::find()->where(['kabupaten_id'=>$id_kabupaten])->orderBy(['name'=>SORT_ASC])->all(), 'name', 'name');
        } else {
            $area = Yii::$app->user->identity->team->area;
            $kabupaten_id = Kabupaten::find()->where(['name' => $area])->one()['id'];
            return ArrayHelper::map(self::find()->where(['kabupaten_id'=>$kabupaten_id])->orderBy(['name'=>SORT_ASC])->all(), 'name', 'name');
        }
    }

    public static function getDataKecamatanByName($name) {
        $kabupaten = Kabupaten::find()->where(['name'=>$name])->one();
        return ArrayHelper::map(self::find()->where(['kabupaten_id'=>$kabupaten['id']])->orderBy(['name'=>SORT_ASC])->all(), 'name', 'name');
    }

    public static function getDataKecamatanAll() {
        return ArrayHelper::map(self::find()->orderBy(['name'=>SORT_ASC])->all(), 'name', 'name');
    }
}
