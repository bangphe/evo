<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "team_leader".
 *
 * @property int $id
 * @property int $team_id
 * @property string $username
 * @property string $password
 * @property string|null $nama
 * @property string|null $created_at
 *
 * @property Team $team
 */
class TeamLeader extends \yii\db\ActiveRecord implements yii\web\IdentityInterface
{
    public $authKey;
    public $accessToken;
    public $users;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'team_leader';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['team_id', 'username', 'password'], 'required'],
            [['team_id'], 'integer'],
            [['created_at'], 'safe'],
            [['username'], 'string', 'max' => 50],
            [['password', 'nama'], 'string', 'max' => 100],
            [['team_id'], 'exist', 'skipOnError' => true, 'targetClass' => Team::className(), 'targetAttribute' => ['team_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'team_id' => 'Team ID',
            'username' => 'Username',
            'password' => 'Password',
            'nama' => 'Nama',
            'created_at' => 'Created At',
        ];
    }

    /**
     * Gets query for [[Team]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTeam()
    {
        return $this->hasOne(Team::className(), ['id' => 'team_id']);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    public function validatePassword($password)
    {
        return md5($password) === $this->password;
    }

    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }
}
