<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "m_kabupaten".
 *
 * @property int $id
 * @property int $provinsi_id
 * @property string $name
 */
class Kabupaten extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'm_kabupaten';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'provinsi_id'], 'required'],
            [['id', 'provinsi_id'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'provinsi_id' => 'Provinsi ID',
            'name' => 'Name',
        ];
    }

    public static function getDataKabupaten() {
        // return ArrayHelper::map(self::find()->where(['in', 'id', ['3516', '3578']])->orderBy(['name'=>SORT_ASC])->all(), 'name', 'name');
        return ArrayHelper::map(self::find()->orderBy(['name'=>SORT_ASC])->all(), 'name', 'name');
    }

    public static function getDataKabupatenById($id_kabupaten) {
        return ArrayHelper::map(self::find()->where(['id'=>$id_kabupaten])->orderBy(['name'=>SORT_ASC])->all(), 'id', 'name');
    }
}
