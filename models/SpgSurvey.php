<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "spg_survey".
 *
 * @property int $id
 * @property int $spg_id
 * @property int|null $journey_id
 * @property int|null $week
 * @property string|null $area
 * @property string|null $product
 * @property string|null $kecamatan
 * @property string|null $lokasi
 * @property string|null $venue
 * @property string|null $tgl_input
 * @property string $nama_pelanggan
 * @property string $umur
 * @property string $jenis_kelamin
 * @property string $alamat
 * @property string $pekerjaan
 * @property string $no_telpon
 * @property int $jumlah_beli
 * @property string $rokok_yang_dipakai
 * @property string $tempat_beli_rokok
 * @property string $tahu_rokok_evo
 * @property string $komentar_harga
 * @property string|null $komentar_rasa
 * @property string $komentar_kemasan
 * @property string|null $rayon
 * @property string|null $latitude
 * @property string|null $longitude
 * @property string|null $created_at
 *
 * @property Spg $spg
 */
class SpgSurvey extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'spg_survey';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['spg_id', 'lokasi', 'nama_pelanggan', 'umur', 'jenis_kelamin', 'alamat', 'pekerjaan', 'no_telpon'], 'required', 'message' => '{attribute} harus diisi.'],
            [['spg_id', 'journey_id', 'week', 'jumlah_beli'], 'integer'],
            [['tgl_input', 'created_at'], 'safe'],
            [['created_at'], 'default', 'value'=> date('Y-m-d H:i:s')],
            [['alamat'], 'string'],
            [['area', 'product', 'lokasi', 'nama_pelanggan', 'pekerjaan', 'rokok_yang_dipakai', 'tempat_beli_rokok', 'tahu_rokok_evo', 'komentar_harga', 'komentar_rasa', 'komentar_kemasan', 'rayon'], 'string', 'max' => 50],
            [['jenis_kelamin', 'umur', 'no_telpon'], 'string', 'max' => 20],
            [['kecamatan'], 'string', 'max' => 255],
            [['venue', 'latitude', 'longitude'], 'string', 'max' => 100],
            [['spg_id'], 'exist', 'skipOnError' => true, 'targetClass' => Spg::className(), 'targetAttribute' => ['spg_id' => 'id']],
            [['journey_id'], 'exist', 'skipOnError' => true, 'targetClass' => SpgJourney::className(), 'targetAttribute' => ['journey_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'spg_id' => 'SPG',
            'journey_id' => 'Journey',
            'week' => 'Week',
            'area' => 'Area',
            'product' => 'Product',
            'kecamatan' => 'Kecamatan',
            'lokasi' => 'Lokasi',
            'venue' => 'Venue',
            'tgl_input' => 'Tgl Input',
            'nama_pelanggan' => 'Nama Pelanggan',
            'umur' => 'Umur',
            'jenis_kelamin' => 'Jenis Kelamin',
            'alamat' => 'Alamat',
            'pekerjaan' => 'Pekerjaan',
            'no_telpon' => 'No Telpon',
            'jumlah_beli' => 'Jumlah Beli',
            'rokok_yang_dipakai' => 'SOB',
            'tempat_beli_rokok' => 'Tempat Beli Rokok',
            'tahu_rokok_evo' => 'Brand Awareness',
            'komentar_harga' => 'Komentar Harga',
            'komentar_rasa' => 'Komentar Rasa',
            'komentar_kemasan' => 'Komentar Kemasan',
            'rayon' => 'Rayon',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'created_at' => 'Created At',
        ];
    }

    /**
     * Gets query for [[Spg]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSpg()
    {
        return $this->hasOne(Spg::className(), ['id' => 'spg_id']);
    }

    /**
     * Gets query for [[Journey]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJourney()
    {
        return $this->hasOne(SpgJourney::className(), ['id' => 'journey_id']);
    }
}
