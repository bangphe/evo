<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the model class for table "team".
 *
 * @property int $id
 * @property string $nama_team
 * @property string $area
 * @property string $kecamatan
 * @property string|null $status
 * @property string|null $created_at
 *
 * @property Spg[] $spgs
 * @property SpgJourney[] $spgJourneys
 * @property TeamLeader[] $teamLeaders
 */
class Team extends \yii\db\ActiveRecord
{
    const STATUS_NON_ACTIVE = 'non-active';
    const STATUS_ACTIVE = 'active';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'team';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_team', 'area'], 'required', 'message' => '{attribute} harus diisi.'],
            [['created_at'], 'safe'],
            [['nama_team', 'area', 'kecamatan'], 'string', 'max' => 50],
            [['status'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_team' => 'Nama Team',
            'area' => 'Area',
            'kecamatan' => 'Kecamatan',
            'status' => 'Status',
            'created_at' => 'Created At',
        ];
    }

    /**
     * Gets query for [[Spgs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSpgs()
    {
        return $this->hasMany(Spg::className(), ['team_id' => 'id']);
    }

    /**
     * Gets query for [[SpgJourneys]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSpgJourneys()
    {
        return $this->hasMany(SpgJourney::className(), ['team_id' => 'id']);
    }

    /**
     * Gets query for [[TeamLeaders]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTeamLeaders()
    {
        return $this->hasMany(TeamLeader::className(), ['team_id' => 'id']);
    }

    public static function getDataTeam() {
        $role = Yii::$app->user->identity->role->role;
        
        if ($role == 'super-admin') {
            return ArrayHelper::map(self::find()->all(), 'id', 'nama_team');
        } elseif ($role == 'admin-regional') {
            $locations = explode(',', Yii::$app->user->identity->location);
            $teams_id = [];
            // get query teams per location
            $query = Team::find();
            foreach($locations as $loc) {
                $query->orFilterWhere(['like', 'area', $loc]);
            }
            $teams = $query->all();
            foreach($teams as $team) {
                array_push($teams_id, $team->id);
            }
            
            return ArrayHelper::map(self::find()->where(['id' => $teams_id])->all(), 'id', 'nama_team');
        } else {
            // $team_id = Yii::$app->user->identity->team_id;
            $area = Yii::$app->user->identity->team->area;
            return ArrayHelper::map(self::find()->where(['like', 'area', $area])->all(), 'id', 'nama_team');
        }
    }

    public static function getActionButton($id){
        $view = Html::a('<i class="fa fa-search"></i>',['view','id'=>$id],['class'=>'btn btn-secondary m-btn m-btn--icon m-btn--icon-only']);
        $update = Html::a('<i class="fa fa-pencil-alt"></i>',['update','id'=>$id],['class'=>'btn btn-success m-btn m-btn--icon m-btn--icon-only']);
        $delete = Html::a('<i class="fa fa-trash"></i>',['delete','id'=>$id],
            [
                'class'=>'btn btn-danger m-btn m-btn--icon m-btn--icon-only',
                'data'=>[
                    'method'=>'post',
                    'confirm'=>'Apakah Anda yakin ingin menghapus data ini?',
                ]
            ]);

        return $view . ' ' . $update . ' ' .$delete;
    }

    public static function getLabelStatus($status){
        switch ($status) {
            case self::STATUS_ACTIVE:
                return '<span class="label label-success">AKTIF</span>';
                break;
            case self::STATUS_NON_ACTIVE:
                return '<span class="label label-warning">NON-AKTIF</span>';
                break;
            default:
                # code...
                break;
        }
    }

    public static function getDataStatus() {
        return [
            self::STATUS_ACTIVE => 'Active',
            self::STATUS_NON_ACTIVE => 'Non-Active',
        ];
    }
}
