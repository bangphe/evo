<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property string $first_name
 * @property string $last_name
 * @property int $address
 * @property int $phone_number
 * @property string|null $status
 * @property string|null $created_date
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['address', 'phone_number'], 'required'],
            [['address', 'phone_number'], 'integer'],
            [['created_date'], 'safe'],
            [['username', 'first_name', 'last_name'], 'string', 'max' => 20],
            [['password'], 'string', 'max' => 100],
            [['status'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'address' => 'Address',
            'phone_number' => 'Phone Number',
            'status' => 'Status',
            'created_date' => 'Created Date',
        ];
    }
}
