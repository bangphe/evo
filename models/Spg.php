<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "spg".
 *
 * @property int $id
 * @property int $team_id
 * @property string $username
 * @property string $password
 * @property string $nama_spg
 * @property string $kecamatan
 * @property string $location
 * @property string|null $status
 * @property string|null $created_at
 *
 * @property Team $team
 */
class Spg extends ActiveRecord implements IdentityInterface
{
    const STATUS_NON_ACTIVE = 'non-active';
    const STATUS_ACTIVE = 'active';

    public $authKey;
    public $accessToken;
    public $id_team;
    public $id_spgs;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'spg';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['team_id', 'username', 'nama_spg'], 'required', 'message' => '{attribute} harus diisi.'],
            [['password'], 'required', 'on' => 'create'],
            [['team_id'], 'integer'],
            [['created_at'], 'safe'],
            [['username'], 'string', 'max' => 20],
            [['password', 'nama_spg'], 'string', 'max' => 50],
            [['kecamatan', 'location'], 'string', 'max' => 100],
            [['status'], 'string', 'max' => 10],
            [['team_id'], 'exist', 'skipOnError' => true, 'targetClass' => Team::className(), 'targetAttribute' => ['team_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Nama SPG',
            'team_id' => 'Tim',
            'username' => 'Username',
            'password' => 'Password',
            'nama_spg' => 'Nama SPG',
            'kecamatan' => 'Kecamatan',
            'location' => 'Lokasi',
            'status' => 'Status',
            'created_at' => 'Created At',
            'id_team' => 'Tim',
            'id_spgs' => 'Pilih SPG',
        ];
    }

    /**
     * Gets query for [[Team]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTeam()
    {
        return $this->hasOne(Team::className(), ['id' => 'team_id']);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    public function validatePassword($password)
    {
        return md5($password) === $this->password;
    }

    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    public static function getActionButton($id){
        $view = Html::a('<i class="fa fa-search"></i>',['view','id'=>$id],['class'=>'btn btn-secondary m-btn m-btn--icon m-btn--icon-only']);
        $update = Html::a('<i class="fa fa-pencil-alt"></i>',['update','id'=>$id],['class'=>'btn btn-success m-btn m-btn--icon m-btn--icon-only']);
        $delete = Html::a('<i class="fa fa-trash"></i>',['delete','id'=>$id],
            [
                'class'=>'btn btn-danger m-btn m-btn--icon m-btn--icon-only',
                'data'=>[
                    'method'=>'post',
                    'confirm'=>'Apakah Anda yakin ingin menghapus data ini?',
                ]
            ]);

        return $view . ' ' . $update . ' ' .$delete;
    }

    public static function getLabelStatus($status){
        switch ($status) {
            case self::STATUS_ACTIVE:
                return '<span class="label label-success">AKTIF</span>';
                break;
            case self::STATUS_NON_ACTIVE:
                return '<span class="label label-warning">NON-AKTIF</span>';
                break;
            default:
                # code...
                break;
        }
    }

    public static function getDataSpg() {
        return ArrayHelper::map(self::find()->all(), 'id', 'nama_spg');
    }

    public static function getDataStatus() {
        return [
            self::STATUS_ACTIVE => 'Active',
            self::STATUS_NON_ACTIVE => 'Non-Active',
        ];
    }
}
