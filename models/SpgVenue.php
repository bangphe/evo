<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "spg_venue".
 *
 * @property int $id
 * @property int $journey_id
 * @property string|null $kecamatan
 * @property string|null $venue
 * @property string|null $created_at
 *
 * @property SpgJourney $journey
 */
class SpgVenue extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'spg_venue';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['journey_id'], 'required'],
            [['journey_id'], 'integer'],
            [['created_at'], 'safe'],
            [['kecamatan', 'venue'], 'string', 'max' => 100],
            [['journey_id'], 'exist', 'skipOnError' => true, 'targetClass' => SpgJourney::className(), 'targetAttribute' => ['journey_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'journey_id' => 'Journey ID',
            'kecamatan' => 'Kecamatan',
            'venue' => 'Venue',
            'created_at' => 'Created At',
        ];
    }

    /**
     * Gets query for [[Journey]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJourney()
    {
        return $this->hasOne(SpgJourney::className(), ['id' => 'journey_id']);
    }
}
