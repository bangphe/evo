<?php

namespace app\models;

use Yii;

use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the model class for table "spg_journey".
 *
 * @property int $id
 * @property int $spg_id
 * @property int $team_id
 * @property int $week
 * @property string $journey_date
 * @property string|null $journey_day
 * @property string $kecamatan
 * @property int $stock
 * @property string|null $created_at
 *
 * @property Spg $spg
 * @property Team $team
 * @property SpgLocation[] $spgLocations
 * @property SpgSurvey[] $spgSurveys
 * @property SpgVenue[] $spgVenues
 */
class SpgJourney extends \yii\db\ActiveRecord
{
    public $venue;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'spg_journey';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['week', 'journey_date', 'stock'], 'required', 'message' => '{attribute} harus diisi.'],
            [['spg_id', 'week', 'stock'], 'integer'],
            [['journey_date', 'created_at'], 'safe'],
            [['journey_day'], 'string', 'max' => 20],
            [['spg_id'], 'exist', 'skipOnError' => true, 'targetClass' => Spg::className(), 'targetAttribute' => ['spg_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'spg_id' => 'SPG',
            'team_id' => 'Team',
            'week' => 'Week',
            'venue' => 'Venue',
            'journey_date' => 'Tanggal Journey',
            'journey_day' => 'Hari',
            'kecamatan' => 'Kecamatan',
            'stock' => 'Stock',
            'created_at' => 'Created At',
        ];
    }

    /**
     * Gets query for [[Spg]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSpg()
    {
        return $this->hasOne(Spg::className(), ['id' => 'spg_id']);
    }

    /**
     * Gets query for [[Team]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTeam()
    {
        return $this->hasOne(Team::className(), ['id' => 'team_id']);
    }

    /**
     * Gets query for [[SpgLocations]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSpgLocations()
    {
        return $this->hasMany(SpgLocation::className(), ['journey_id' => 'id']);
    }

    /**
     * Gets query for [[SpgSurveys]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSpgSurveys()
    {
        return $this->hasMany(SpgSurvey::className(), ['journey_id' => 'id']);
    }

    /**
     * Gets query for [[SpgVenues]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSpgVenues()
    {
        return $this->hasMany(SpgVenue::className(), ['journey_id' => 'id']);
    }

    public static function getActionButton($id){
        // $view = Html::a('<i class="fa fa-search"></i>',['view','id'=>$id],['class'=>'btn btn-secondary m-btn m-btn--icon m-btn--icon-only']);
        $update = Html::a('<i class="fa fa-pencil-alt"></i>',['update','id'=>$id],['class'=>'btn btn-success m-btn m-btn--icon m-btn--icon-only']);
        $delete = Html::a('<i class="fa fa-trash"></i>',['delete','id'=>$id],
            [
                'class'=>'btn btn-danger m-btn m-btn--icon m-btn--icon-only',
                'data'=>[
                    'method'=>'post',
                    'confirm'=>'Apakah Anda yakin ingin menghapus data ini?',
                ]
            ]);

        return $update . ' ' .$delete;
    }

    public function getVenues(){
        $venues = [];
        if (!empty($this->spgVenues)) {
            foreach($this->spgVenues as $p) {
                $venues[] = [
                    'id'=>$p->id,
                    'kecamatan'=>$p->kecamatan,
                    'venue'=>$p->venue,
                ];
            }
        }

        return $venues;
    }
}
