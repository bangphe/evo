<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "spg_location".
 *
 * @property int $id
 * @property int $spg_id
 * * @property int $journey_id
 * @property string|null $longitude
 * @property string|null $latitude
 * @property string|null $kecamatan_device
 * @property string|null $kecamatan_survey
 * @property string|null $created_at
 *
 * @property Spg $spg
 */
class SpgLocation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'spg_location';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['spg_id', 'journey_id'], 'required'],
            [['spg_id', 'journey_id'], 'integer'],
            [['created_at'], 'safe'],
            [['created_at'], 'default', 'value'=> date('Y-m-d H:i:s')],
            [['longitude', 'latitude', 'kecamatan_device', 'kecamatan_survey'], 'string', 'max' => 50],
            [['spg_id'], 'exist', 'skipOnError' => true, 'targetClass' => Spg::className(), 'targetAttribute' => ['spg_id' => 'id']],
            [['journey_id'], 'exist', 'skipOnError' => true, 'targetClass' => SpgJourney::className(), 'targetAttribute' => ['journey_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'spg_id' => 'Nama SPG',
            'journey_id' => 'Journey',
            'longitude' => 'Longitude',
            'latitude' => 'Latitude',
            'kecamatan_device' => 'Kecamatan',
            'kecamatan_survey' => 'Kecamatan',
            'created_at' => 'Created At',
        ];
    }

    /**
     * Gets query for [[Spg]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSpg()
    {
        return $this->hasOne(Spg::className(), ['id' => 'spg_id']);
    }

    /**
     * Gets query for [[Journey]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJourney()
    {
        return $this->hasOne(SpgJourney::className(), ['id' => 'journey_id']);
    }

    public static function getUnread()
    {
        // $userid = Yii::$app->user->identity->id;
        // $model = self::find()
        //     ->where(['user_penerima'=>$userid])
        //     ->andWhere(['status'=>Notifikasi::UNREAD])
        //     ->orderBy(['created_at'=>SORT_DESC])
        //     ->all();'
        $role = Yii::$app->user->identity->role->role;
        $team_id = Yii::$app->user->identity->team_id;

        if ($role == 'super-admin') {
            $commandLokasi = Yii::$app->db->createCommand("SELECT * FROM spg_location WHERE id IN ( SELECT max(id) FROM spg_location GROUP BY spg_id, journey_id ) AND is_read IS NULL")->queryAll();
        } elseif ($role == 'admin-regional') {
            $locations = explode(',', Yii::$app->user->identity->location);
            $teams_id = [];
            $teams = Team::find()->where(['area' => $locations])->all();
            foreach($teams as $team) {
                array_push($teams_id, $team->id);
            }

            $dataJourneyPerTeam = (new \yii\db\Query())
                ->select(['id'])
                ->from('spg_journey')
                ->where(['team_id' => $teams_id])
                ->all();
            
            $journeyIds = [];
            if ($dataJourneyPerTeam != NULL) {
                foreach($dataJourneyPerTeam as $row) {
                    array_push($journeyIds, $row['id']);
                }
            }
            
            $journeyId = "'" . implode("','", $journeyIds) . "'";
            
            $commandLokasi = Yii::$app->db->createCommand("SELECT * FROM spg_location WHERE id IN ( SELECT max(id) FROM spg_location GROUP BY spg_id, journey_id ) AND journey_id IN (" . $journeyId . ") AND is_read IS NULL")->queryAll();
        } else {
            $dataJourneyPerTeam = (new \yii\db\Query())
                ->select(['id'])
                ->from('spg_journey')
                ->where(['team_id' => $team_id])
                ->all();
            
            $journeyIds = [];
            if ($dataJourneyPerTeam != NULL) {
                foreach($dataJourneyPerTeam as $row) {
                    array_push($journeyIds, $row['id']);
                }
            }
            $journeyId = "'" . implode("','", $journeyIds) . "'";
            
            $commandLokasi = Yii::$app->db->createCommand("SELECT * FROM spg_location WHERE id IN ( SELECT max(id) FROM spg_location GROUP BY spg_id, journey_id ) AND journey_id IN (" . $journeyId . ") AND is_read IS NULL")->queryAll();
        }
        
        $model = $commandLokasi;

        return $model;
    }

    public static function getAllNotif()
    {
        // $userid = Yii::$app->user->identity->id;
        // $model = self::find()
        //     ->where(['user_penerima'=>$userid])
        //     ->andWhere(['status'=>Notifikasi::UNREAD])
        //     ->orderBy(['created_at'=>SORT_DESC])
        //     ->all();'
        $role = Yii::$app->user->identity->role->role;
        $team_id = Yii::$app->user->identity->team_id;

        if ($role == 'super-admin') {
            $commandLokasi = Yii::$app->db->createCommand("SELECT * FROM spg_location WHERE id IN ( SELECT max(id) FROM spg_location GROUP BY spg_id, journey_id )")->queryAll();
        } elseif ($role == 'admin-regional') {
            $locations = explode(',', Yii::$app->user->identity->location);
            $teams_id = [];
            $teams = Team::find()->where(['area' => $locations])->all();
            foreach($teams as $team) {
                array_push($teams_id, $team->id);
            }

            $dataJourneyPerTeam = (new \yii\db\Query())
                ->select(['id'])
                ->from('spg_journey')
                ->where(['team_id' => $teams_id])
                ->all();
            
            $journeyIds = [];
            if ($dataJourneyPerTeam != NULL) {
                foreach($dataJourneyPerTeam as $row) {
                    array_push($journeyIds, $row['id']);
                }
            }
            
            $journeyId = "'" . implode("','", $journeyIds) . "'";
            
            $commandLokasi = Yii::$app->db->createCommand("SELECT * FROM spg_location WHERE id IN ( SELECT max(id) FROM spg_location GROUP BY spg_id, journey_id ) AND journey_id IN (" . $journeyId . ") ")->queryAll();
        } else {
            $dataJourneyPerTeam = (new \yii\db\Query())
                ->select(['id'])
                ->from('spg_journey')
                ->where(['team_id' => $team_id])
                ->all();
            
            $journeyIds = [];
            if ($dataJourneyPerTeam != NULL) {
                foreach($dataJourneyPerTeam as $row) {
                    array_push($journeyIds, $row['id']);
                }
            }
            $journeyId = "'" . implode("','", $journeyIds) . "'";
            
            $commandLokasi = Yii::$app->db->createCommand("SELECT * FROM spg_location WHERE id IN ( SELECT max(id) FROM spg_location GROUP BY spg_id, journey_id ) AND journey_id IN (" . $journeyId . ") ")->queryAll();
        }
        
        $model = $commandLokasi;

        return $model;
    }

    public static function showDashboard($role, $area) {
        if ($role == 'super-admin') {
            echo 'display: block;';
        } elseif ($role == 'admin-regional') {
            $locations = explode(',', Yii::$app->user->identity->location);
            if (in_array($area, $locations)) {
                echo 'display: block;';
            } else {
                echo 'display: none;';
            }
        } else {
            if (Yii::$app->user->identity->team->area == $area) {
                echo 'display: block;';
            } else {
                echo 'display: none;';
            }
        }
    }
}
