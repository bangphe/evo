import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import * as RootNavigation from '../navigation/RootNavigation';

import { 
    BASE_URL,
    KEY_STORAGE_USER_NAME, 
    KEY_STORAGE_USER,
    KEY_STORAGE_TEAMNAME,
    KEY_STORAGE_USERTYPE,
    KEY_STORAGE_KABUPATEN_ID,
    KEY_STORAGE_KABUPATEN_NAME
} from '../store/constant';

import { Messaging } from '../common';

const axiosInstance = axios.create({
    baseURL: BASE_URL,
});

axiosInstance.interceptors.request.use(
    async (config) => {
        // let JWT_TOKEN = await AsyncStorage.getItem(KEY_STORAGE_TOKEN);
        // if (JWT_TOKEN != null) {
        //     config.headers.Authorization = `Bearer ${JWT_TOKEN}`;
        // }
        console.log('axios request', config);
        return config;
    },
    (error) => {
        console.warn('error');
        return Promise.reject(error);
    },
);

axiosInstance.interceptors.response.use(
    (response) => {
        console.log('axios response', response);
        // if (response.status != 200) {
        //     if (response.statusText) {
        //         console.warn({
        //             message: response.statusText,
        //             description:
        //                 'Something wrong, please contact your administrator. \n- ' + response.statusText + ' with code: ' + response.status,
        //             type: 'danger'
        //         });
        //     } else {
        //         console.warn({
        //             message: 'Error',
        //             description:
        //                 'Something wrong, please contact your administrator. \n- ' + response.status,
        //             type: 'danger'
        //         });
        //     }
        // }
        if (!response.data.success && response.data.message == 'Access token is invalid') {
            AsyncStorage.multiRemove([
                KEY_STORAGE_USER_NAME,
                KEY_STORAGE_USER,
                KEY_STORAGE_TEAMNAME,
                KEY_STORAGE_USERTYPE,
                KEY_STORAGE_KABUPATEN_ID,
                KEY_STORAGE_KABUPATEN_NAME
            ]).then(() => {
                // RootNavigation.replace('Login');
            });
            response.data.message = 'Your session has expired'
        }
        return response;
    },
    (error) => {
        console.warn('axios error', error);
        console.log('errr ax', error.response);
        if (typeof error.response !== undefined) {
            if (error.response.status == 401) {
                AsyncStorage.multiRemove([
                    KEY_STORAGE_USER_NAME,
                    KEY_STORAGE_USER,
                    KEY_STORAGE_TEAMNAME,
                    KEY_STORAGE_USERTYPE,
                    KEY_STORAGE_KABUPATEN_ID,
                    KEY_STORAGE_KABUPATEN_NAME
                ]).then(() => {
                    // RootNavigation.replace('Login');
                });
            } else if (error.response.status == 422) {
                return {
                    ...error.response,
                    error: true
                }
            }
        } else {
            // Messaging.showMessage({
            //     message: 'Fatal Error',
            //     description: error.message,
            //     type: Messaging.types.DANGER,
            // });
        }

        return typeof error.response != 'undefined'
            ? error.response
            : { data: { status: false, message: error.message, data: [] } };
    },
);

const $axios = axiosInstance;

export default $axios;
