export default {
  primary: '#455A64',
  primaryDark: '#37474F',
  light: '#5f5fc4',
  accent: '#607D8B',
  textPrimary: '#212121',
  textSecondary: '#757575',
  textLight: '#FAFAFA',
  bgWindow: '#F6F7F9',
  navBar: '#455A64',
  statusBar: '#37474F',
  grey: '#757575',
  greyLight: '#D7D7D7',
  greyLightest: '#EEEEEE',

  urlColor: '#1565C0',

  materialBaseColor: '#9E9E9E',
  materialTintColor: '#546E7A',

  errorColor: '#C62828',

  navbarTitleColor: '#FAFAFA',
};
