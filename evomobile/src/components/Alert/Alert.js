import React from 'react';

import {
    StyleSheet,
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome5'

import CustomAlertComponent from './Libs/CustomAlertComponent';

import AlertManager from "./AlertManager";

function srid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }

    return `${s4()}-${s4()}-${s4()}`;
}

/**
 * Global function to handle show alert that can be called anywhere in your app
 * Pass some `title` object as first attribute to display alert in your app
 *
 * ```
 *  showAlert({title, detail = "", type = "default", ok = {}, cancel = {}})
 * ```
 */
export function showAlert(options) {
    const ref = AlertManager.getDefault();
    if (!!ref) {
        ref.showAlert(options);
    }
}

/**
 * Global function to programmatically close alert that can be called anywhere in your app
 *
 * ```
 *  closeAlert()
 * ```
 */
export function closeAlert() {
    const ref = AlertManager.getDefault();
    if (!!ref) {
        ref.closeAlert();
    }
}

export default class Alert extends React.Component {
    constructor(props) {
        super(props);

        // this.closeAlert = this.closeAlert.bind(this);
        if (!this._id) this._id = srid();

        this.state = {
            isOpen: false,
            theme: 'default',
            title: '',
            detail: '',
            ok: null,
            cancel: null,
            closeOnTouchOutside: true,
            closeOnHardwareBackPress: true
        };
    }

    componentDidMount() {
        AlertManager.register(this);
    }
    componentWillUnmount() {
        AlertManager.unregister(this);
    }

    icon = () => {
        switch (this.state.theme) {
            case 'success':
                return (
                    <Icon name='check-circle' style={styles.icon} />
                );
            case 'danger':
                return (
                    <Icon name='exclamation-triangle' style={styles.icon} />
                );
            case 'warning':
                return (
                    <Icon name='exclamation-triangle' style={styles.icon} />
                );
            case 'info':
                return (
                    <Icon name='info' style={styles.icon} />
                );
            default:
                return null;
        }
    }

    showAlert = ({ title, detail = "", type = 'default', ok = null, cancel = null, closeOnTouchOutside = true, closeOnHardwareBackPress = true }) => {
        this.setState({
            isOpen: true,
            title: title,
            detail: detail,
            theme: type,
            ok: ok,
            cancel: cancel,
            closeOnTouchOutside: closeOnTouchOutside,
            closeOnHardwareBackPress: closeOnHardwareBackPress
        })
    }
    closeAlert = () => {
        this.setState({ isOpen: false })
    }
    btnHandler = (onPress) => {
        if (typeof onPress == 'function') {
            onPress();
        }
        this.closeAlert();
    }

    render() {
        return (
            <CustomAlertComponent
                show={this.state.isOpen}
                showProgress={false}
                title={this.state.title}
                message={this.state.detail}
                closeOnTouchOutside={this.state.closeOnTouchOutside}
                closeOnHardwareBackPress={this.state.closeOnHardwareBackPress}
                showCancelButton={this.state.cancel ? true : false}
                showConfirmButton={true}
                useNativeDriver={true}
                cancelText={this.state.cancel && this.state.cancel.text ? this.state.cancel.text : 'Cancel'}
                confirmText={this.state.ok && this.state.ok.text ? this.state.ok.text : 'OK'}
                icon={this.icon()}
                onCancelPressed={() => {
                    this.btnHandler(this.state.cancel ? this.state.cancel.onPress : null)
                }}
                onConfirmPressed={() => {
                    this.btnHandler(this.state.ok ? this.state.ok.onPress : null)
                }}
            />
        )
    }

    _id;
}

const styles = StyleSheet.create({
    icon: {
        fontSize: 18,
        color: '#626262',
        marginRight: 8
    },
});
