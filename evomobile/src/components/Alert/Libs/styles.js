import { StyleSheet, Dimensions } from 'react-native';

const { height, width } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute'
  },
  overlay: {
    width: width,
    height: height,
    position: 'absolute',
    backgroundColor: 'rgba(52,52,52,0.5)'
  },
  contentContainer: {
    maxWidth: '80%',
    borderRadius: 4,
    backgroundColor: 'white',
    padding: 8
  },
  content: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    padding: 8
  },
  action: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    marginTop: 16
  },
  titleContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  title: {
    color: '#626262',
    fontSize: 16,
    fontWeight: 'bold'
  },
  message: {
    paddingTop: 8,
    color: '#7b7b7b',
    fontSize: 14
  },
  button: {
    paddingHorizontal: 18,
    paddingVertical: 8
  },
  buttonText: {
    color: '#fff',
    fontSize: 14
  },
  buttonTextOk: {
    color: '#1565C0'
  },
  buttonTextCancel: {
    color: '#78909C'
  }
});

export default styles;
