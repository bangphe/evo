import React from 'react';
import { StyleSheet, TouchableOpacity, Text } from 'react-native';
import { font } from '../../values';

const Button = ({
    text,
    onPress,
    textColor = 'rgba(0,0,0,.7)',
    color = 'white',
    outline = false,
    marginLeft = 0,
    marginRight = 0,
    marginBottom = 0,
    marginTop = 0,
    size = 'sm',
    borderRadius = 3,
    paddingHorizontal = null,
    paddingVertical = null,
    containerStyle = {},
    textStyle = {},
    onLayout,
    disabled = false
}) => {
    let defaultButtonStyling = {
        backgroundColor: color,
        marginLeft,
        marginBottom,
        marginRight,
        marginTop,
        borderRadius,
    };
    let defaultTextStyling = { 
        color: textColor, 
        fontSize: font.size.normal, 
        fontWeight: 'normal'
    };

    if (outline) {
        defaultButtonStyling.borderColor = color;
        defaultButtonStyling.borderWidth = StyleSheet.hairlineWidth;
        defaultButtonStyling.backgroundColor = 'transparent';
    }

    defaultButtonStyling.paddingHorizontal = 8;
    defaultButtonStyling.paddingVertical = 4;
    if (size == 'md') {
        defaultButtonStyling.paddingHorizontal = 16;
        defaultButtonStyling.paddingVertical = 8;
        defaultTextStyling.fontSize = 14;
    } else if (size == 'lg') {
        defaultButtonStyling.paddingHorizontal = 16;
        defaultButtonStyling.paddingVertical = 16;
        defaultTextStyling.fontSize = 14;
        defaultTextStyling.fontWeight = 'bold';
    }
    if (paddingHorizontal && !isNaN(paddingHorizontal)) {
        defaultButtonStyling.paddingHorizontal = paddingHorizontal;
    }
    if (paddingVertical && !isNaN(paddingVertical)) {
        defaultButtonStyling.paddingVertical = paddingVertical;
    }

    return (
        <TouchableOpacity
            disabled={disabled}
            delayPressIn={0}
            onLayout={onLayout}
            activeOpacity={0.75}
            onPress={onPress}
            style={{ ...defaultButtonStyling, ...styles.button, ...containerStyle }}>
            <Text style={{ ...defaultTextStyling, ...styles.text, ...textStyle }}>{text}</Text>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    button: {
        alignItems: 'center',
    },
});

export default Button;
