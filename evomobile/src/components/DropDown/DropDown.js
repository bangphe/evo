import React, {useState, useEffect, useRef} from 'react';
import {Animated, View, Text, StyleSheet} from 'react-native';
import RNPickerSelect from './react-native-picker-select';
import styles from './styles/dropdown';
import {font, color} from '../../values';

const TextField = ({
    fontSize,
    labelFontSize,
    textColor,
    baseColor,
    value,
    onChangeText,
    label = '',
    error = '',
    items = [],
}) => {
    // fadeAnim will be used as the value for opacity. Initial Value: 0
    const fadeAnim = useRef(new Animated.Value(0)).current;

    const fadeIn = () => {
        Animated.timing(fadeAnim, {
            toValue: 1,
            duration: 150,
            useNativeDriver: true,
        }).start();
    };

    const fadeOut = () => {
        Animated.timing(fadeAnim, {
            toValue: 0,
            duration: 250,
            useNativeDriver: true,
        }).start();
    };

    const [text, setText] = useState('');
    const [errorText, setErrorText] = useState('');

    const localOnChangeText = (textInput) => {
        setText(textInput);
        if (onChangeText) {
            onChangeText(textInput);
        }
    };

    useEffect(() => {
        if (value) {
            setText(value);
        }

        return () => {};
    }, []);

    useEffect(() => {
        if (text) {
            fadeIn();
        } else {
            fadeOut();
        }
        return () => {};
    }, [text]);

    if (error != errorText) {
        setErrorText(error);
    }

    const pickerSelectStyles = StyleSheet.create({
        inputIOS: {
            paddingTop: 0,
            paddingBottom: 4,
            paddingHorizontal: 4,
            fontSize: fontSize || font.size.normal,
            color: textColor || color.textPrimary,
        //   paddingRight: 30, // to ensure the text is never behind the icon
        },
        inputAndroid: {
            paddingTop: 0,
            paddingBottom: 4,
            paddingHorizontal: 4,
            fontSize: fontSize || font.size.normal,
            color: textColor || color.textPrimary,
        //   paddingRight: 30, // to ensure the text is never behind the icon
        },
      });

    return (
        <View style={styles.mainContainer}>
            <Animated.View
                style={[
                    styles.labelContainerStyle,
                    {
                        opacity: fadeAnim,
                    },
                ]}>
                <Text
                    style={[
                        styles.labelStyle,
                        {
                            fontSize: labelFontSize || font.size.inputLabel,
                            color: errorText
                                ? color.errorColor
                                : baseColor || color.materialBaseColor,
                        },
                    ]}>
                    {label}
                </Text>
            </Animated.View>
            {/* placeholder={label || 'Select an item'} */}
            {/* labelStyle={[
                    {
                        fontSize: fontSize || font.size.normal,
                        color: textColor || color.textPrimary,
                    }
                ]} */}
                {/* placeholderStyle={{color: baseColor || color.materialBaseColor}} */}
                {/* borderWidth: errorText ? 1 : 0.5,
                    borderColor: errorText
                        ? color.errorColor
                        : baseColor || color.materialBaseColor, */}
            <View
                style={[
                    styles.inputContainerStyle,
                    {
                        borderWidth: errorText ? 1 : 0.5,
                        borderColor: errorText
                            ? color.errorColor
                            : baseColor || color.materialBaseColor,
                    },
                ]}>
                <RNPickerSelect
                    style={pickerSelectStyles}
                    useNativeAndroidPickerStyle={false}
                    placeholder={{
                        label: label || 'Select an item',
                        value: null,
                        color: baseColor || color.materialBaseColor,
                    }}
                    items={items}
                    value={value}
                    onValueChange={(itemValue, itemPosition) => {
                        localOnChangeText(itemValue);
                    }}
                >
                </RNPickerSelect>
            </View>

            {!!errorText && (
                <Text
                    style={[
                        styles.errorStyle,
                        {
                            opacity: 1,
                        },
                    ]}>
                    {errorText || ''}
                </Text>
            )}
        </View>
    );
};

export default TextField;
