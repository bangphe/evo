import {StyleSheet, StatusBar} from 'react-native';
import {color, font} from '../../../values';

export default StyleSheet.create({
    mainContainer: {
        marginBottom: 8,
        flexDirection: 'column',
        top: -6
    },
    labelContainerStyle: {
        flexDirection: 'row',
        top: 6,
        marginLeft: 6,
        zIndex: 1,
    },
    labelStyle: {
        paddingHorizontal: 4,
        backgroundColor: 'white',
    },
    inputContainerStyle: {
        flexDirection: 'column',
        borderStyle: 'dashed',
        borderRadius: 4,
        borderWidth: 1.5,
        backgroundColor: '#ffffff',
        paddingVertical: 8,
        paddingHorizontal: 8,
        justifyContent: 'center',
        alignItems: 'center'
        // paddingBottom: 4,
        // paddingTop: 6,
        // paddingHorizontal: 8,
    },
    placeHolderContainer: {
        flex: 1,
        padding: 32,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    placeHolderIcon: {
        fontSize: 24,
        marginRight: 8
    },
    placeHolderText: {
        fontSize: font.size.normal
    },

    imageContainer: {
        
    },
    imagePreview: {
        resizeMode: 'contain',
        height: 256,
        width: 256,
        marginBottom: 8
    },
    imageInfo: {
        color: color.textPrimary,
        fontSize: font.size.smaller,
        marginBottom: 4,
        textAlign: 'center'
    },
    
    errorStyle: {
        paddingHorizontal: 4,
        marginTop: 4,
        fontSize: font.size.inputError,
        color: color.errorColor,
    },
});
