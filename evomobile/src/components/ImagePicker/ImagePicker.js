import React, {useState, useEffect, useRef} from 'react';
import {
    Animated, 
    View, 
    Text, 
    TouchableOpacity,
    Platform
} from 'react-native';

import Image from 'react-native-fast-image'

import DocumentPicker from 'react-native-document-picker';
import RNFetchBlob from 'rn-fetch-blob';
import RNImagePicker from 'react-native-image-picker';

import styles from './styles/image-picker';
import {font, color} from '../../values';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { Helper } from '../../common';

const ImagePicker = ({
    labelFontSize,
    baseColor,
    onChange,
    value,
    label = '',
    error = '',
    placeHolder = ''
}) => {

    const [selectedItem, setSelectedItem] = useState(null);
    const [errorText, setErrorText] = useState('');

    const [imgSource, setImgSource] = useState(null);

    if (error != errorText) {
        setErrorText(error);
    }

    const methods = {
        pickImage: async () => {
            const options = {
                customButtons: [],
                cameraType: 'front',
                mediaType: 'photo'
            };

            RNImagePicker.showImagePicker(options, (response) => {
                if (response.didCancel) {

                } else if (response.error) {
                    console.log('ImagePicker Error: ', response.error);
                } else if (response.customButton) {
                    console.log('User tapped custom button: ', response.customButton);
                } else {
                    let base64 = response.data
                    // setModel({...model, 
                    //     picture: response.fileName,
                    //     logoContent: base64,
                    //     logoMIME: response.type
                    // });
                    
                    let existingAttachment = {
                        fileName: response.fileName,
                        fileType: response.type,
                        fileSize: response.fileSize,
                        base64: base64,
                        uri: response.uri
                    };

                    if (Platform.OS === "ios") {
                        let path = existingAttachment.uri;
                        path = "~" + path.substring(path.indexOf("/Documents"));
                        if (!existingAttachment.fileName) {
                            existingAttachment.fileName = path.split("/").pop();
                        }
                    }
                    
                    setSelectedItem({...existingAttachment});
                    setImgSource({
                        uri: response.uri
                    })
                    methods.localOnChange(existingAttachment);
                }
            });
        },
        localOnChange: (item) => {
            let emitter;
            if (item) {
                emitter = item;
            } else {
                
            }
            if (onChange) {
                onChange(emitter);
            }
        },
        isSelectedItemAvailable: () => {
            return !!(selectedItem)
        }
    };

    return (
        <View style={styles.mainContainer}>
            <Animated.View
                style={[
                    styles.labelContainerStyle,
                    {
                        opacity: 1,
                    },
                ]}>
                <Text
                    style={[
                        styles.labelStyle,
                        {
                            fontSize: labelFontSize || font.size.inputLabel,
                            color: errorText
                                ? color.errorColor
                                : baseColor || color.materialBaseColor,
                        },
                    ]}>
                    {label}
                </Text>
            </Animated.View>
            <TouchableOpacity
                delayPressIn={0}
                style={[
                    styles.inputContainerStyle,
                    {
                        borderColor: errorText
                            ? color.errorColor
                            : baseColor || color.materialBaseColor,
                    },
                ]}
                onPress={() => methods.pickImage()} 
                activeOpacity={0.5}>
                
                    {!methods.isSelectedItemAvailable() &&
                        <View style={{flexDirection: 'row'}}>
                            <View style={styles.placeHolderContainer}>
                                <Icon
                                    style={[
                                        styles.placeHolderIcon,
                                        {
                                            color: baseColor || color.materialBaseColor,
                                        },
                                    ]}
                                    name='camera' />
                                <Text
                                    style={[
                                        styles.placeHolderText,
                                        {
                                            color: baseColor || color.materialBaseColor,
                                        },
                                    ]}>
                                    {placeHolder || label}
                                </Text>
                            </View>
                        </View>
                    }
                    {methods.isSelectedItemAvailable() && (
                        <>
                            <Image style={styles.imagePreview} 
                                source={imgSource}/>
                            <Text style={styles.imageInfo}>
                                {`${selectedItem.fileName} (${Helper.formatBytes(selectedItem.fileSize, 2)})`}
                            </Text>  
                        </>
                    )}
                
            </TouchableOpacity>

            {!!errorText && (
                <Text
                    style={[
                        styles.errorStyle,
                        {
                            opacity: 1,
                        },
                    ]}>
                    {errorText || ''}
                </Text>
            )}
        </View>
    );
};

export default ImagePicker;
