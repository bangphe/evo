import { applyMiddleware, createStore } from 'redux';
import thunk from "redux-thunk";

import rootReducer from './reducer';

// Redux: Store
const store = createStore(
    rootReducer,
    applyMiddleware(thunk),
);


// Exports
export {store};
