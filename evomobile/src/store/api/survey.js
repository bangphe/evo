import axios from '../../plugin/axios';

export default {
    create: async ({
        spg_id,
        week,
        area,
        product,
        kecamatan,
        lokasi,
        tgl_input,
        nama_pelanggan,
        umur,
        jenis_kelamin,
        alamat,
        pekerjaan,
        no_telpon,
        jumlah_beli,
        rokok_yang_dipakai,
        tempat_beli_rokok,
        tahu_rokok_evo,
        komentar_harga,
        komentar_rasa,
        komentar_kemasan,
        rayon,
        journey_id,
        venue,
        latitude,
        longitude
    }) => {        
        let response = await axios.post(
            `/apisurvey`, {
                spg_id,
                week,
                area,
                product,
                kecamatan,
                lokasi,
                tgl_input,
                nama_pelanggan,
                umur,
                jenis_kelamin,
                alamat,
                pekerjaan,
                no_telpon,
                jumlah_beli,
                rokok_yang_dipakai,
                tempat_beli_rokok,
                tahu_rokok_evo,
                komentar_harga,
                komentar_rasa,
                komentar_kemasan,
                rayon,
                journey_id,
                venue,
                latitude,
                longitude
            }
        );
        return response;
    },
};