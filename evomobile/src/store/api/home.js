import axios from '../../plugin/axios';

export default {
    getJourney: async ({spg_id, team_id, date}) => {
        let response = await axios.get(
            `/apijourney?spg_id=${spg_id}&team_id=${team_id}&journey_date=${date}`
        );
        return response;
    },
    getKecamatan: async ({kabupaten}) => {
        let response = await axios.get(
            `/apikecamatan?kabupaten_id=${kabupaten}`
        );
        return response;
    },
    getSurvey: async ({journey_id, spg_id}) => {
        let response = await axios.get(
            `/apisurvey?journey_id=${journey_id}&spg_id=${spg_id}`
        );
        return response;
    },
    setLocation: async ({
        spg_id,
        journey_id,
        longitude,
        latitude,
        kecamatan_device,
        kecamatan_survey
    }) => {        
        let response = await axios.post(
            `/apilocation`, {
                spg_id,
                journey_id,
                longitude,
                latitude,
                kecamatan_device,
                kecamatan_survey,
            }
        );
        return response;
    },
    getSurveySummary: async ({spg_id}) => {
        let response = await axios.get(
            `/apisurvey/dashboard?spg_id=${spg_id}`
        );
        return response;
    },
    getSurveySummaryTeamLead: async ({team_id}) => {
        let response = await axios.get(
            `/apisurvey/dashboard-team-leader?team_id=${team_id}`
        );
        return response;
    },
};