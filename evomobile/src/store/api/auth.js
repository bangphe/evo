import axios from '../../plugin/axios';

export default {
    singin: async ({ username, password }) => {        
        let response = await axios.post(
            `/apispg/login`, {
                username,
                password
            }
        );
        return response;
    },
    singOut: async () => {
        let response = await axios.post(
            `/logout`
        );
        return response;
    }
};