import { 
    PROFILE_SET_ME
} from '../constant';

const initialState = {
    id: null,
    team_id: null,
    username: null,
    password: null,
    nama_spg: null,
    kecamatan: null,
    location: null,
    longitude: null,
    latitude: null,
    status: null,
    created_at: null,
    team_name: null,
    user_type: null,
    nama: null
};

const profileReducer = (state = initialState, action) => {
    switch (action.type) {
        case PROFILE_SET_ME:
            return { 
                ...state, 
                id: action.payload.id,
                team_id: action.payload.team_id,
                username: action.payload.username,
                password: action.payload.password,
                nama_spg: action.payload.nama_spg,
                kecamatan: action.payload.kecamatan,
                location: action.payload.location,
                longitude: action.payload.longitude,
                latitude: action.payload.latitude,
                status: action.payload.status,
                created_at: action.payload.created_at,
                team_name: action.payload.team_name,
                nama: action.payload.nama,
                user_type: action.payload.user_type
            };
        default:
            return state;
    }
};

export default profileReducer;
