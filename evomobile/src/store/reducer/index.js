// Imports: Dependencies
import {combineReducers} from 'redux';
// Imports: Reducers
import globalReducer from './globalReducer';
import homeReducer from './homeReducer';
import profileReducer from './profileReducer';

// Redux: Root Reducer
const rootReducer = combineReducers({
    global: globalReducer,
    home: homeReducer,
    profile: profileReducer
});
// Exports
export default rootReducer;
