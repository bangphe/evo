import { Messaging } from '../../common';
import { showAlert } from '../../components/Alert';
import { HOME_SET_JOURNEY, HOME_SET_KECAMATAN, HOME_SET_SURVEY, HOME_SET_SURVEY_SUMMARY, HOME_SET_SURVEY_SUMMARY_TEAMLEAD } from '../constant';

const initialState = {
    journey: [],
    kecamatan: [],
    survey: [],
    summary: {}
};

const homeReducer = (state = initialState, action) => {
    switch (action.type) {
        case HOME_SET_JOURNEY: {
            let data = [];
            if (action.payload.data.length) {
                data = action.payload.data;
            } else if (action.payload.data.name == "Not Found") {
                showAlert({
                    title: "Warning",
                    detail: "Data Journey tidak ditemukan. Mohon refresh ulang",
                    type: "warning"
                })
            }
            return { 
                ...state, 
                journey: data
            };
        }
        case HOME_SET_KECAMATAN: {
            let data = [];
            if (action.payload.data.length) {
                data = action.payload.data;
            }
            return { 
                ...state, 
                kecamatan: data
            };
        }
        case HOME_SET_SURVEY: {
            let data = [];
            // let existingSum = state.summary;
            // let totalSurvey = 0;
            if (action.payload && action.payload.length) {
                action.payload.forEach(p => {
                    if (p.data.length) {
                        data.push(...p.data);
                        // totalSurvey += p.data.filter(dt => dt.tahu_rokok_evo || dt.tempat_beli_rokok || dt.komentar_harga || dt.komentar_rasa || dt.komentar_kemasan).length
                    }       
                });
            }
            return { 
                ...state, 
                survey: data,
                // summary: {
                //     ...existingSum,
                //     // totalSurvey
                // }
            };
        }
        case HOME_SET_SURVEY_SUMMARY: {
            let data = {};
            let existing = state.summary;
            if (action.payload.data) {
                data = action.payload.data;
            }
            return { 
                ...state, 
                summary: {
                    ...existing,
                    ...data
                }
            };
        }
        case HOME_SET_SURVEY_SUMMARY_TEAMLEAD: {
            let data = {};
            let existing = state.summary;
            if (action.payload.data) {
                data = action.payload.data;
            }
            return { 
                ...state, 
                summary: {
                    ...existing,
                    ...data
                }
            };
        }
        default:
            return state;
    }
};

export default homeReducer;
