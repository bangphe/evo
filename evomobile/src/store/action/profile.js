import { 
    PROFILE_SET_ME
} from "../constant";

export const setMe = (param) => async dispatch => {
    dispatch({ 
        type: PROFILE_SET_ME, 
        payload: param,
    });
};