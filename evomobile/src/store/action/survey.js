
import api from '../api/survey';

export const create = (param = {
    spg_id,
    week,
    area,
    product,
    kecamatan,
    lokasi,
    tgl_input,
    nama_pelanggan,
    umur,
    jenis_kelamin,
    alamat,
    pekerjaan,
    no_telpon,
    jumlah_beli,
    rokok_yang_dipakai,
    tempat_beli_rokok,
    tahu_rokok_evo,
    komentar_harga,
    komentar_rasa,
    komentar_kemasan,
    rayon,
    journey_id,
    venue,
    latitude,
    longitude
}) => async dispatch => {
    let res = await api.create(param)
    return res;
};
