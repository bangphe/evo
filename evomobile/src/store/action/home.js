
import api from '../api/home';
import { 
    HOME_SET_JOURNEY, HOME_SET_KECAMATAN, HOME_SET_SURVEY, HOME_SET_SURVEY_SUMMARY, HOME_SET_SURVEY_SUMMARY_TEAMLEAD,
} from '../constant';
import { Helper } from '../../common';

export const getKecamatan = ({kabupaten}) => async dispatch => {
    let res = await api.getKecamatan({
        kabupaten
    });
    dispatch({ type: HOME_SET_KECAMATAN, payload: res });
};

export const getJourney = ({spg_id, team_id, date}) => async dispatch => {
    let res = await api.getJourney({
        spg_id,
        date,
        team_id
    });
    dispatch({ type: HOME_SET_JOURNEY, payload: res });
};

export const getSurvey = ({journey_ids, spg_id}) => async dispatch => {
    let result = []
    for(let i = 0; i < journey_ids.length; i++) {
        let res = await api.getSurvey({
            journey_id: journey_ids[i],
            spg_id
        });
        result.push(res);
    }
    
    dispatch({ type: HOME_SET_SURVEY, payload: result });
};

export const setLocation = (param = {
    spg_id,
    journey_id,
    longitude,
    latitude,
    kecamatan_device,
    kecamatan_survey
}) => async dispatch => {
    let res = await api.setLocation(param)
    return res;
};

export const getSurveySummary = ({spg_id}) => async dispatch => {
    let res = await api.getSurveySummary({
        spg_id
    });
    
    dispatch({ type: HOME_SET_SURVEY_SUMMARY, payload: res });
};

export const getSurveySummaryTeamLead = ({team_id}) => async dispatch => {
    let res = await api.getSurveySummaryTeamLead({
        team_id
    });
    
    dispatch({ type: HOME_SET_SURVEY_SUMMARY_TEAMLEAD, payload: res });
};