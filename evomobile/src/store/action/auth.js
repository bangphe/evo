
import api from '../api/auth';
import { 
    PROFILE_SET_ME,
    PROFILE_SET_USER_DATA,
    KEY_STORAGE_ROLES,
} from '../constant';

import AsyncStorage from '@react-native-community/async-storage';

export const authLogin = ({username, password}) => async dispatch => {
    let {data} = await api.singin({
        username: username,
        password: password
    })
    return data;
};

export const authLogout = () => async dispatch => {
    let {data} = await api.singOut();
    return data;
};


export const authClearAllState = () => async dispatch => {
    dispatch({ 
        type: PROFILE_SET_ME, 
        payload: {
            id: null,
            nrp: "",
            name: "",
            email: "",
            no_telp: "",
            fcm: "",
            satuan_id: null,
            jajaran_id: null,
            image_url: "",
            point: {}
        },
    });
    dispatch({ 
        type: PROFILE_SET_USER_DATA, 
        payload: {
            data: {}
        },
    });
    //#region HOME
    
    //#endregion

};