import {
    PermissionsAndroid
} from 'react-native';

import { Messaging } from '.';
import { showAlert } from '../components/Alert';

const Permissions = {
    requestStoragePermission: async() => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
                {
                    title: 'Permission',
                    message:
                    'WD needs access to read your external storage ' +
                    'so you can choose file from your storage.',
                    buttonPositive: 'OK'
                }
            );
            switch (granted) {
                case PermissionsAndroid.RESULTS.GRANTED:
                    return true
                case PermissionsAndroid.RESULTS.DENIED:
                    
                case PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN:
                    
                default:
                    Messaging.showMessage({
                        message: 'Warning',
                        description: 'Unable to read file from storage, please check your permission setting for this app',
                        type: Messaging.types.WARNING
                    })
                    return false;
            }
        } catch (err) {
            console.warn(err);
            return false;
        }
    },
    requestCameraPermission: async() => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.CAMERA,
                {
                    title: 'Permission',
                    message: 'WD needs access to camera ',
                    buttonPositive: 'OK'
                }
            );
            switch (granted) {
                case PermissionsAndroid.RESULTS.GRANTED:
                    return true
                case PermissionsAndroid.RESULTS.DENIED:
                    
                case PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN:
                    
                default:
                    Messaging.showMessage({
                        message: 'Warning',
                        description: 'Unable to launch camera, please check your permission setting for this app',
                        type: Messaging.types.WARNING
                    })
                    return false;
            }
        } catch (err) {
            console.warn(err);
            return false;
        }
    },
    requestLocationPermission: async() => {
        try {
            let allowed = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
            if (allowed) {
                return true;
            }
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                {
                    title: 'Permission',
                    message: 'WD needs access to read your location.',
                    buttonPositive: 'OK'
                }
            );
            switch (granted) {
                case PermissionsAndroid.RESULTS.GRANTED:
                    return true
                case PermissionsAndroid.RESULTS.DENIED:
                    
                case PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN:
                    
                default:
                    showAlert({
                        title: 'Warning',
                        detail: 'Unable to get location, please check your permission setting for this app.',
                        type: 'danger'
                    })
                    return false;
            }
        } catch (err) {
            console.warn(err);
            return false;
        }
    },
};

export default Permissions;