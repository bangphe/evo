import RNFetchBlob from 'rn-fetch-blob';
import { Platform } from 'react-native';
import { Messaging } from '.';
// import { showLocation } from 'react-native-map-link'

const Helper = {
    sleep: (ms) => {
        return new Promise(resolve => setTimeout(resolve, ms));
    },
    clone: (obj) => {
        return JSON.parse(JSON.stringify(obj));
    },
    formatBytes: (bytes, decimals = 2) => {
        if (bytes === 0) return '0 Bytes';
    
        const k = 1024;
        const dm = decimals < 0 ? 0 : decimals;
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    
        const i = Math.floor(Math.log(bytes) / Math.log(k));
    
        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    },
    previewFile: async (uri, id) => {
        try {
            let temp = uri.split("/");
            let filename = temp[temp.length - 1];
            if (id) {
                filename = id + filename;
            }
            let res = await RNFetchBlob.config({
                // add this option that makes response data to be stored as a file,
                // this is much more performant.
                path: `${RNFetchBlob.fs.dirs.CacheDir}/simotorbaja/${filename}`
            })
            .fetch('GET', uri, {});
            let info = await res.info();
            try {
                if (Platform.OS == 'android') {
                    await RNFetchBlob.android.actionViewIntent(res.path(), info.headers['Content-Type'])
                } else if (Platform.OS == 'ios') {
                    RNFetchBlob.ios.previewDocument(res.path());
                }
            } catch(viewErr) {
                Messaging.showMessage({
                    message: 'Can\'t open file',
                    description: 'No app associated with this file',
                    type: 'warning'
                });
            }
        } catch (err) {
            console.log('error getting the file', err);
        } finally {
            
        }
    },
    // openMap: (longitude, latitude) => {
    //     showLocation({
    //         latitude: latitude,
    //         longitude: longitude
    //     })
    // },
    distance: (lat1, lon1, lat2, lon2) => {
        const deg2rad = (deg) => {
            return deg * (Math.PI / 180);
        };
        var R = 6371; // Radius of the earth in km
        var dLat = deg2rad(lat2 - lat1); // deg2rad below
        var dLon = deg2rad(lon2 - lon1);
        var a =
            Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(deg2rad(lat1)) *
                Math.cos(deg2rad(lat2)) *
                Math.sin(dLon / 2) *
                Math.sin(dLon / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c; // Distance in km
        return d * 1000; // return in meters
    },
};

export default Helper;