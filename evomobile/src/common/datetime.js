import moment from 'moment';
const DateTime = {
    getMonthName: (month) => {
        return [
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        ][+month - 1]
    },
    fullStringFormat: (date) => {
        if (date && moment(date).isValid()) {
            let dMoment = moment(date);
            return `${dMoment.format('DD')} ${DateTime.getMonthName(dMoment.format('M'))} ${dMoment.format('YYYY')} Pukul ${dMoment.format('HH:mm')}`
        }
        return '';
    },
    fullDateFormat: (date) => {
        if (date && moment(date).isValid()) {
            let dMoment = moment(date);
            return `${dMoment.format('DD')} ${DateTime.getMonthName(dMoment.format('M'))} ${dMoment.format('YYYY')}`
        }
        return '';
    },
    dateTimeFormat: (date) => {
        if (date && moment(date).isValid()) {
            let dMoment = moment(date);
            return `${dMoment.format('DD')} ${DateTime.getMonthName(dMoment.format('M'))} ${dMoment.format('YYYY')} ${dMoment.format('HH:mm')}`
        }
        return '';
    },
};

export default DateTime;