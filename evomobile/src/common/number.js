const Helper = {
    format: function(num) {
        if (num === null || isNaN(num)) {
            return '';
        }
        let numStr = num.toString().split('.');
        return numStr[0].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') + (numStr.length > 1 ? ',' + numStr[1] : '');
    },
    
};

export default Helper;