import React from 'react';
import {
    StyleSheet,
    Dimensions,
    View,
    TouchableOpacity
} from 'react-native';

import {createStackNavigator} from '@react-navigation/stack';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Image from 'react-native-fast-image';

import Login from '../screen/Auth/LoginLayout';
import Home from '../screen/Home';
import CreateSurvey from '../screen/CreateSurvey';

import { color, font } from '../values';
import { KEY_STORAGE_TEAMNAME, KEY_STORAGE_USER, KEY_STORAGE_USER_NAME, KEY_STORAGE_USERTYPE, KEY_STORAGE_KABUPATEN_ID, KEY_STORAGE_KABUPATEN_NAME } from '../store/constant';
import AsyncStorage from '@react-native-community/async-storage';

const Stack = createStackNavigator();

const HomeRightNav = (navigation) => {
    const logout = async () => {
        AsyncStorage.multiRemove([
            KEY_STORAGE_USER,
            KEY_STORAGE_USER_NAME,
            KEY_STORAGE_TEAMNAME,
            KEY_STORAGE_USERTYPE,
            KEY_STORAGE_KABUPATEN_ID,
            KEY_STORAGE_KABUPATEN_NAME
        ]).then(() => {
            navigation.replace('Login');
        });
    };
    return (
      <TouchableOpacity onPress={() => {logout()} } activeOpacity={0.5} delayPressIn={0}>
        <Icon name="logout-variant" style={styles.rightNavIcon} />
      </TouchableOpacity>
    );
  };

const RootStack = () => {
    const width = Dimensions.get("window").width;
    return (
        <Stack.Navigator 
            initialRouteName="Login"
            screenOptions={{
                headerTintColor: color.navbarTitleColor,
                headerStyle: { 
                    backgroundColor: color.navBar,
                },
                headerTitleStyle: {
                    color: color.navbarTitleColor,
                    fontSize: font.size.navbarTitle
                }
            }}
        >
            <Stack.Screen
                name="Login"
                component={Login}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="HomeLayout"
                component={Home}
                options={({ navigation }) => ({
                    headerTitle: () => {
                        return (
                            // <View style={styles.middleNavContainer}>
                                <Image
                                    style={[
                                        styles.logo
                                    ]}
                                    source={require('../assets/images/evo-text.png')}
                                    resizeMode='contain'
                                />
                            // </View>
                        );
                    },
                    headerTitleAlign: 'center',
                    headerLeft: () => null,
                    headerRight: () => HomeRightNav(navigation),
                })}
                
            />
            <Stack.Screen
                name="CreateSurvey"
                component={CreateSurvey}
                options={{
                    headerTitle: "Tambah Survey"
                }}
                
            />
            
        </Stack.Navigator>
    );
};

const styles = StyleSheet.create({
    middleNavContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    logo: {
        alignSelf: 'center',
        width: 120,
        resizeMode: 'center',
        height: 50
    },
    rightNavIcon: {
        fontSize: 24,
        color: "#ffffff",
        marginRight: 16
    }
})
export default RootStack;
