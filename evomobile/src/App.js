/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import 'react-native-gesture-handler';
import React, { useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { Provider } from 'react-redux';
import SplashScreen from 'react-native-splash-screen'

import './config'

import RootStack from './navigation/RootStack';
import { navigationRef, isMountedRef } from './navigation/RootNavigation';
import { store } from './store';

import FlashMessage from "react-native-flash-message";
import Alert from './components/Alert';

const App = () => {
  useEffect(() => {
    SplashScreen.hide();
    isMountedRef.current = true;
    return () => (isMountedRef.current = false);
  }, []);

  return (
    <Provider store={store}>
      <NavigationContainer ref={navigationRef}>
        <RootStack />
      </NavigationContainer>
      <FlashMessage position="top" animated={true} />
      <Alert />
    </Provider>
  );
};

export default App;
