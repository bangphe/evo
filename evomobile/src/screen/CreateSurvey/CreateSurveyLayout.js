import React, {useEffect, useState} from 'react';
import {
    View,
    ScrollView,
    Text,
    TouchableOpacity
} from 'react-native';

//#region from 3rd party
import {connect} from 'react-redux';
import moment from 'moment';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
//#endregion

//#region custom component
import Button from '../../components/Button';
import TextField from '../../components/TextField';
import DropDown from '../../components/DropDown';
//#region 

//#region action & reducers
import {
    create as createSurvey
} from '../../store/action/survey';
//#endregion

import { SET_LOADING, KEY_STORAGE_KABUPATEN_NAME } from '../../store/constant';
import { Validator, Helper, Permissions } from '../../common';
import { wrapperNoScroll } from '../../hoc';

import styles from './styles/create-survey';
import { showAlert } from '../../components/Alert';
import { color } from '../../values';
import Geolocation from 'react-native-geolocation-service';
import SinglePicker from '../../components/SinglePicker/SinglePicker';

const CreateSurveyLayout = ({
    navigation,
    route,

    profile,
    home,
    global,
    
    createSurvey,

    setLoading,
}) => {

    const [areaByUserLoc, setAreaByUserLoc] = useState({
        name: ''
    });

    const [venueData, setVenueData] = useState([]);
    const [selectedJourney, setSelectedJourney] = useState(null);
    const [isAddingVenue, setIsAddingVenue] = useState(false);
    const [newVenueText, setNewVenueText] = useState('');

    const [error, setError] = useState({
        lokasi: null,
        nama_pelanggan: null,
        umur: null,
        jenis_kelamin: null,
        venue: null,
        alamat: null,
        pekerjaan: null,
        no_telpon: null,
        jumlah_beli: null,
        rokok_yang_dipakai: null,
        tempat_beli_rokok: null,
        tahu_rokok_evo: null,
        komentar_harga: null,
        komentar_rasa: null,
        komentar_kemasan: null,
    });

    const [model, setModel] = useState({
        lokasi: null,
        nama_pelanggan: null,
        umur: null,
        jenis_kelamin: null,
        venue: null,
        alamat: null,
        pekerjaan: null,
        no_telpon: null,
        jumlah_beli: null,
        rokok_yang_dipakai: null,
        tempat_beli_rokok: null,
        tahu_rokok_evo: null,
        komentar_harga: null,
        komentar_rasa: null,
        komentar_kemasan: null,
        latitude: 0,
        longitude: 0
    });

    useEffect(() => {
        methods.findLocation().then(({latitude, longitude}) => {
            setModel({
                ...model,
                latitude,
                longitude
            })
            methods.calculateNearestLocation(latitude, longitude)
        });
        return () => {};
    }, []);

    useEffect(() => {
        methods.onAreaChange();
        return () => {};
    }, [areaByUserLoc]);

    const kecamatanFiltered = () => {
        let activeKecamatan = [];
        if (home.journey && home.journey.length) {
            home.journey.forEach(x => {
                if (x.kecamatan) {
                    activeKecamatan.push(...x.kecamatan.split(","));
                }
            });
        }
        return home.kecamatan && home.kecamatan.length ? home.kecamatan.filter(x => activeKecamatan.indexOf(x.name) >= 0) : []
    };
    const methods = {
        onAreaChange: () => {
            let journey = null;
            if (home.journey && home.journey.length) {
                let obj = home.journey.find(x => x.kecamatan.split(",").indexOf(areaByUserLoc.name) >= 0);
                if (obj) {
                    journey = obj;
                }
            }
            setSelectedJourney(journey);
            let venues = [];
            if (journey && journey.venues && journey.venues.length) {
                venues = journey.venues.filter(x => x.kecamatan === areaByUserLoc.name).map(x => ({label: x.venue, value: x.venue}))
            }
            setVenueData(venues);
        },
        changeLocation: (item) => {
            setAreaByUserLoc(item);
        },
        calculateNearestLocation: (latitude, longitude) => {
            let nearestLocation = {name: ''};
            kecamatanFiltered().map((val, idx) => {
                let distance = Helper.distance(
                    latitude,
                    longitude,
                    +val.latitude,
                    +val.longitude,
                );
                if (idx == 0) {
                    nearestLocation = {
                        ...val,
                        distance,
                    };
                } else {
                    if (distance < nearestLocation.distance) {
                        nearestLocation = {
                            ...val,
                            distance,
                        };
                    }
                }
            });
            setAreaByUserLoc(nearestLocation);
        },
        findLocation: (skipPrompt) => {
            return new Promise(resolve => {
                Permissions.requestLocationPermission().then(locationPermission => {
                    if (locationPermission) {
                        Geolocation.getCurrentPosition(({ coords: { latitude, longitude } }) => {
                            resolve({
                                latitude: latitude,
                                longitude: longitude
                            });
                        }, (err) => {
                            if (!skipPrompt) {
                                showAlert({
                                    title: 'Warning',
                                    detail: 'Tidak dapat menemukan lokasi.',
                                    type: 'danger'
                                })
                                const PERMISSION_DENIED = err.PERMISSION_DENIED;
                                if (err.code == PERMISSION_DENIED) {
                                    
                                }
                            }
                            resolve({
                                latitude: 0,
                                longitude: 0
                            });
                        }, {
                            enableHighAccuracy: true
                        })
                    } else {
                        resolve({
                            latitude: 0,
                            longitude: 0
                        });
                    }
                });
            });
        },
        save: async () => {
            try {
                setLoading(true);
                let validationSuccess = true;
                let err = {};
                //#region validation
                // if (!Validator.required(model.lokasi)) {
                //     err.lokasi = 'Lokasi harus diisi'
                //     validationSuccess = false;
                // }
                // if (!Validator.required(model.nama_pelanggan)) {
                //     err.nama_pelanggan = 'Nama Pelanggan harus diisi'
                //     validationSuccess = false;
                // }
                // if (!Validator.required(model.jumlah_beli)) {
                //     err.jumlah_beli = 'Jumlah beli harus diisi'
                //     validationSuccess = false;
                // }
                // if (!Validator.required(model.jenis_kelamin)) {
                //     err.jenis_kelamin = 'Gender harus diisi'
                //     validationSuccess = false;
                // }
                //#endregion

                if (!validationSuccess) {
                    setError(err);
                } else {
                    let kabName = await AsyncStorage.getItem(KEY_STORAGE_KABUPATEN_NAME)
                    let res = await createSurvey({
                        ...model,
                        jumlah_beli: +model.jumlah_beli || 0,
                        spg_id: profile.id,
                        tgl_input: moment().format(),
                        week: selectedJourney ? selectedJourney.week || 0 : 0,
                        area: kabName,
                        product: 'WD',
                        kecamatan: areaByUserLoc.name,
                        rayon: kabName,
                        journey_id: selectedJourney ? selectedJourney.id || 0 : 0,
                        longitude: '' + model.longitude,
                        latitude: '' + model.latitude,
                    });
                    if ((''+res.status)[0] == '2') {
                        if (typeof route.params.onSuccess == 'function') {
                            route.params.onSuccess();
                        }
                        navigation.goBack();
                    } else {
                        let msg = "";
                        if (res.data.length) {
                            res.data.forEach(x => {
                                if (x.message) {
                                    msg = x.message;
                                    return;
                                }
                            });
                        } else if (res.data.message) {
                            msg = res.data.message;
                        }
                        showAlert({
                            title: "Warning",
                            detail: msg || 'Something went wrong.',
                            type: "warning"
                        })
                    }
                
                }
            } catch(err) {
                console.log('err', err);
            } finally {
                setLoading(false);
            }
        },
        toggleAddingVenue: () => {
            setNewVenueText('');
            setIsAddingVenue(!isAddingVenue);
        },
        addVenue: () => {
            let newVenue = newVenueText;
            if (!newVenue) {
                return;
            }
            setVenueData([
                ...venueData,
                {label: newVenue, value: newVenue}
            ]);
            setNewVenueText('');
            setIsAddingVenue(false);
            
            setError({...error, venue: null});
            setModel({...model, venue: newVenue});
        }
    };

    return (
        <ScrollView 
            style={styles.mainWrapper}
            >
            <View style={styles.container}>
                <View style={styles.areaContainer}>
                    <Text style={styles.areaCaption}>Area : </Text>
                    <Text style={styles.areaValue}>{areaByUserLoc.name}</Text>
                    <SinglePicker
                        label="Area"
                        textExtractor={(item) => {
                            return item ? item.name : '';
                        }}
                        containerStyle={styles.areaChangeContainer}
                        data={kecamatanFiltered()}
                        onSelect={(selected) => {
                            methods.changeLocation(selected)
                        }}
                        renderChild={
                            <Text style={styles.areaChangeText}>Change</Text>
                        }
                        />
                </View>
                <View style={styles.dash} />
                <View style={styles.inputContainer}>
                    <View style={[styles.twoColumnContainer, {alignItems: 'center'}]}>
                        <View style={[styles.twoColumn1, styles.twoColumnLeft]}>
                            {!isAddingVenue ? (
                                <DropDown
                                    label="Venue"
                                    dropDownMaxHeight={320}
                                    items={venueData}
                                    error={error.venue}
                                    value={model.venue}
                                    onChangeText={(text) => {
                                        setError({...error, venue: null});
                                        setModel({...model, venue: text});
                                    }}
                                />
                            ) : (
                                <TextField
                                    label="Venue"
                                    error={error.venue}
                                    value={newVenueText}
                                    onChangeText={(text) => {
                                        setError({...error, lokasi: null});
                                        setNewVenueText(text);
                                    }}
                                    returnKeyType="next"
                                    onSubmitEditing={() => {methods.addVenue()}}
                                    onBlur={() => {methods.addVenue()}}
                                    focusOnFirstRender={true}
                                />
                            )}
                        </View>
                        <TouchableOpacity style={{paddingLeft: 8, paddingRight: 4, paddingVertical: 4, justifyContent: 'center', alignItems: 'center'}} onPress={() => {methods.toggleAddingVenue()}}>
                            <Icon name={isAddingVenue ? 'close' : 'plus'} style={{color: color.urlColor, fontSize: 24}} />
                        </TouchableOpacity>
                    </View>

                    <TextField
                        label="Lokasi"
                        error={error.lokasi}
                        value={model.lokasi}
                        onChangeText={(text) => {
                            setError({...error, lokasi: null});
                            setModel({...model, lokasi: text});
                        }}
                        returnKeyType="next"
                    />
                    
                    <TextField
                        label="Nama Pelanggan"
                        autoCapitalize="words"
                        error={error.nama_pelanggan}
                        value={model.nama_pelanggan}
                        onChangeText={(text) => {
                            setError({...error, nama_pelanggan: null});
                            setModel({...model, nama_pelanggan: text});
                        }}
                    />

                    <View style={styles.twoColumnContainer}>
                        <View style={[styles.twoColumn1, styles.twoColumnLeft]}>
                            <DropDown
                                label="Umur"
                                dropDownMaxHeight={320}
                                items={[
                                    {
                                        label: '18-25',
                                        value: '18-25'
                                    },
                                    {
                                        label: '26-35',
                                        value: '26-35'
                                    },
                                    {
                                        label: '36-43',
                                        value: '36-43'
                                    },
                                    {
                                        label: '44 >',
                                        value: '44 >'
                                    },
                                ]}
                                error={error.umur}
                                value={model.umur}
                                onChangeText={(text) => {
                                    setError({...error, umur: null});
                                    setModel({...model, umur: text});
                                }}
                            /> 
                        </View>
                        <View style={[styles.twoColumn1, styles.twoColumnRigth]}>
                            <DropDown
                                label="Gender"
                                dropDownMaxHeight={320}
                                items={[
                                    {
                                        label: 'Male',
                                        value: 'Male'
                                    },
                                    {
                                        label: 'Female',
                                        value: 'Female'
                                    }
                                ]}
                                error={error.jenis_kelamin}
                                value={model.jenis_kelamin}
                                onChangeText={(text) => {
                                    setError({...error, jenis_kelamin: null});
                                    setModel({...model, jenis_kelamin: text});
                                }}
                            />
                        </View>
                    </View>

                    <TextField
                        label="Alamat"
                        multiline={true}
                        error={error.alamat}
                        value={model.alamat}
                        onChangeText={(text) => {
                            setError({...error, alamat: null});
                            setModel({...model, alamat: text});
                        }}
                    />
                    <DropDown
                        label="Pekerjaan"
                        dropDownMaxHeight={320}
                        items={[
                            {
                                label: 'Karyawan Swasta',
                                value: 'Karyawan Swasta'
                            },
                            {
                                label: 'Karyawan Pabrik',
                                value: 'Karyawan Pabrik'
                            },
                            {
                                label: 'Mahasiswa',
                                value: 'Mahasiswa'
                            },
                            {
                                label: 'Pengusaha / Pedagang',
                                value: 'Pengusaha / Pedagang'
                            },
                            {
                                label: 'PNS',
                                value: 'PNS'
                            },
                            {
                                label: 'Wiraswasta',
                                value: 'Wiraswasta'
                            },
                            {
                                label: 'TNI / POLRI',
                                value: 'TNI / POLRI'
                            },
                            {
                                label: 'Other',
                                value: 'Other'
                            },
                        ]}
                        error={error.pekerjaan}
                        value={model.pekerjaan}
                        onChangeText={(text) => {
                            setError({...error, pekerjaan: null});
                            setModel({...model, pekerjaan: text});
                        }}
                    />
                    <View style={styles.twoColumnContainer}>
                        <View style={[styles.twoColumn2, styles.twoColumnLeft]}>
                            <TextField
                                label="Telepon"
                                keyboardType="phone-pad"
                                error={error.no_telpon}
                                value={model.no_telpon}
                                onChangeText={(text) => {
                                    setError({...error, no_telpon: null});
                                    setModel({...model, no_telpon: text});
                                }}
                            />
                        </View>
                        <View style={[styles.twoColumn1, styles.twoColumnLeft]}>
                            <DropDown
                                label="Jumlah"
                                dropDownMaxHeight={320}
                                items={[
                                    {
                                        label: '0',
                                        value: '0'
                                    },
                                    {
                                        label: '1',
                                        value: '1'
                                    },
                                    {
                                        label: '2',
                                        value: '2'
                                    }
                                ]}
                                error={error.jumlah_beli}
                                value={model.jumlah_beli}
                                onChangeText={(text) => {
                                    setError({...error, jumlah_beli: null});
                                    setModel({...model, jumlah_beli: text});
                                }}
                            />
                            {/* <TextField
                                label="Jumlah"
                                keyboardType="numeric"
                                error={error.jumlah_beli}
                                value={model.jumlah_beli}
                                onChangeText={(text) => {
                                    setError({...error, jumlah_beli: null});
                                    setModel({...model, jumlah_beli: text});
                                }}
                            /> */}
                        </View>
                    </View>
                    <DropDown
                        label="SOB"
                        dropDownMaxHeight={320}
                        items={[
                            {
                                label: "A mild",
                                value: "A mild"
                            },
                            {
                                label: "Magnum mild",
                                value: "Magnum mild"
                            },
                            {
                                label: "Class mild",
                                value: "Class mild"
                            },
                            {
                                label: "LA lights",
                                value: "LA lights"
                            },
                            {
                                label: "Djarum black",
                                value: "Djarum black"
                            },
                            {
                                label: "Dunhill mild",
                                value: "Dunhill mild"
                            },
                            {
                                label: "Esse mild",
                                value: "Esse mild"
                            },
                            {
                                label: "U mild",
                                value: "U mild"
                            },
                            {
                                label: "Geo mild",
                                value: "Geo mild"
                            },
                            {
                                label: "Surya pro",
                                value: "Surya pro"
                            },
                            {
                                label: "Pro mild",
                                value: "Pro mild"
                            },
                            {
                                label: "GG mild",
                                value: "GG mild"
                            },
                            {
                                label: "Diplomat mild",
                                value: "Diplomat mild"
                            },
                            {
                                label: "Wismilak Diplomat",
                                value: "Wismilak Diplomat"
                            },
                            {
                                label: "Surya ",
                                value: "Surya "
                            },
                            {
                                label: "Inter",
                                value: "Inter"
                            },
                            {
                                label: "Djisamsoe",
                                value: "Djisamsoe"
                            },
                            {
                                label: "GG Djaja",
                                value: "GG Djaja"
                            },
                            {
                                label: "Djarum 76",
                                value: "Djarum 76"
                            },
                            {
                                label: "Djarum Super",
                                value: "Djarum Super"
                            },
                            {
                                label: "Magnum",
                                value: "Magnum"
                            },
                            {
                                label: "Others",
                                value: "Others"
                            },
                        ]}
                        error={error.rokok_yang_dipakai}
                        value={model.rokok_yang_dipakai}
                        onChangeText={(text) => {
                            setError({...error, rokok_yang_dipakai: null});
                            setModel({...model, rokok_yang_dipakai: text});
                        }}
                    />

                    <Text style={styles.groupCaption}>Survey</Text>
                    <View style={[styles.dash, {marginBottom: 12}]} />
                    <DropDown
                        label="Brand Awareness"
                        dropDownMaxHeight={320}
                        items={[
                            {
                                label: 'Ya',
                                value: 'Ya'
                            },
                            {
                                label: 'Tidak',
                                value: 'Tidak'
                            },
                        ]}
                        error={error.tahu_rokok_evo}
                        value={model.tahu_rokok_evo}
                        onChangeText={(text) => {
                            setError({...error, tahu_rokok_evo: null});
                            setModel({...model, tahu_rokok_evo: text});
                        }}
                    />
                    <DropDown
                        label="Tempat membeli rokok"
                        dropDownMaxHeight={320}
                        items={[
                            {
                                label: 'Alfamart / Indomaret',
                                value: 'Alfamart / Indomaret'
                            },
                            {
                                label: 'Toko Retail',
                                value: 'Toko Retail'
                            },
                            {
                                label: 'Warkop',
                                value: 'Warkop'
                            },
                            {
                                label: 'Agen',
                                value: 'Agen'
                            },
                        ]}
                        error={error.tempat_beli_rokok}
                        value={model.tempat_beli_rokok}
                        onChangeText={(text) => {
                            setError({...error, tempat_beli_rokok: null});
                            setModel({...model, tempat_beli_rokok: text});
                        }}
                    />
                    <DropDown
                        label="Komentar harga"
                        dropDownMaxHeight={320}
                        items={[
                            {
                                label: 'Murah',
                                value: 'Murah'
                            },
                            {
                                label: 'Mahal',
                                value: 'Mahal'
                            },
                            {
                                label: 'Standart',
                                value: 'Standart'
                            },
                        ]}
                        error={error.komentar_harga}
                        value={model.komentar_harga}
                        onChangeText={(text) => {
                            setError({...error, komentar_harga: null});
                            setModel({...model, komentar_harga: text});
                        }}
                    />
                    <View style={styles.twoColumnContainer}>
                        <View style={[styles.twoColumn1, styles.twoColumnLeft]}>
                            <DropDown
                                label="Rasa"
                                dropDownMaxHeight={320}
                                items={[
                                    {
                                        label: 'Ampang',
                                        value: 'Ampang'
                                    },
                                    {
                                        label: 'Aromatic',
                                        value: 'Aromatic'
                                    },
                                    {
                                        label: 'Halus',
                                        value: 'Halus'
                                    },
                                    {
                                        label: 'Mantap',
                                        value: 'Mantap'
                                    },
                                    {
                                        label: 'Serik',
                                        value: 'Serik'
                                    },
                                ]}
                                error={error.komentar_rasa}
                                value={model.komentar_rasa}
                                onChangeText={(text) => {
                                    setError({...error, komentar_rasa: null});
                                    setModel({...model, komentar_rasa: text});
                                }}
                            />
                        </View>
                        <View style={[styles.twoColumn1, styles.twoColumnLeft]}>
                            <DropDown
                                label="Kemasan"
                                dropDownMaxHeight={320}
                                items={[
                                    {
                                        label: 'Standart',
                                        value: 'Standart'
                                    },
                                    {
                                        label: 'Menarik',
                                        value: 'Menarik'
                                    },
                                ]}
                                error={error.komentar_kemasan}
                                value={model.komentar_kemasan}
                                onChangeText={(text) => {
                                    setError({...error, komentar_kemasan: null});
                                    setModel({...model, komentar_kemasan: text});
                                }}
                            />
                        </View>
                    </View>

                    <Button
                        marginTop={16}
                        text="Submit"
                        color={color.primary}
                        textColor="#ffffff"
                        size="lg"
                        textStyle={{fontWeight: 'normal'}}
                        paddingVertical={12}
                        disabled={global.loading}
                        onPress={() => methods.save()}
                    />
                </View>
            </View>
        </ScrollView>
    );
};

const mapStateToProps = (state) => {
    return {
        profile: state.profile,
        home: state.home,
        global: state.global
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        createSurvey: async (payload) => await dispatch(createSurvey(payload)),
        setLoading: (payload) => dispatch({ type: SET_LOADING, payload }),
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(wrapperNoScroll(CreateSurveyLayout));
