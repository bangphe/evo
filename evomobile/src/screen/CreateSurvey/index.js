import {wrapperNoScroll} from '../../hoc';
import CreateSurveyLayout from './CreateSurveyLayout';

export default wrapperNoScroll(CreateSurveyLayout);
