import {StyleSheet, Dimensions} from 'react-native';
import {color, font} from '../../../values';

export default StyleSheet.create({
    mainWrapper: {
        backgroundColor: '#ffffff',
        flex: 1,
        flexDirection: 'column',
    },
    container: {
        padding: 16
    },
    inputContainer: {
        marginTop: 8
    },
    twoColumnContainer: {
        flexDirection: 'row'
    },
    twoColumn1: {
        flex: 1,
        flexDirection: 'column'
    },
    twoColumn2: {
        flex: 2,
        flexDirection: 'column'
    },
    twoColumn3: {
        flex: 2,
        flexDirection: 'column'
    },
    twoColumnLeft: {
        marginRight: 8
    },
    twoColumnRigth: {
        marginLeft: 8
    },
    areaContainer: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    areaCaption: {
        fontSize: font.size.normal,
        color: color.textPrimary
    },
    areaValue: {
        flex: 1,
        fontSize: font.size.normal,
        color: color.primaryDark,
        fontWeight: 'bold'
    },
    areaChangeContainer: {
        padding: 8,
    },
    areaChangeText: {
        fontSize: font.size.normal,
        color: color.urlColor,
    },

    groupCaption: {
        fontSize: font.size.bigger,
        color: color.textSecondary,
        fontWeight: 'bold',
        marginTop: 24,
        marginBottom: 12
    },
    dash: {
        height: 1,
        backgroundColor: color.greyLight
    },

    overlay: {
        ...StyleSheet.absoluteFill,
        top: 0,
        backgroundColor: '#000000' + '7f',
        zIndex: 999,
        justifyContent: 'center',
        alignItems: 'center'
    },
    center: {
        justifyContent: 'center',
        backgroundColor: color.bgWindow,
        paddingVertical: 0,
        paddingHorizontal: 0,
        borderRadius: 12,
        margin: 16
    },
    innerCenter: {
        justifyContent: 'center',
        alignItems: 'flex-start',
        marginBottom: 8,
        maxHeight: Dimensions.get('window').height * 0.75,
    },
    headerChooser: {
        paddingHorizontal: 32,
        paddingVertical: 16,
        borderBottomColor: color.greyLight,
        borderBottomWidth: 1,
    },
    hederText: {
        fontSize: font.size.bigger,
        fontWeight: 'bold'
    },
    locationListContainer: {
        paddingVertical: 16,
        paddingHorizontal: 16,
        borderBottomColor: color.greyLight,
        borderBottomWidth: 1,
        flexDirection: 'row'
    },
    locationListText: {
        flex: 1,
        fontSize: font.size.normal,
        color: color.textPrimary
    },
    noDataContainer: {
        flex: 1,
        paddingVertical: 24,
        justifyContent: 'center',
        alignItems: 'center',
    },
    noDataText: {
        fontWeight: '700',
        fontSize: font.size.normal,
        color: color.textSecondary
    },
});
