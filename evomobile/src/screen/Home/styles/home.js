import { StyleSheet } from 'react-native'
import { color, font } from '../../../values';

export default StyleSheet.create({
    container: {
        backgroundColor: color.bgWindow,
        flex: 1,
    },
    rowContainer: {
        paddingHorizontal: 16,
        paddingVertical: 8,
        flexDirection: 'column',
        backgroundColor: '#ffffff',
        marginVertical: 8
    },
    row: {
        flexDirection: 'row'
    },

    leftTitle: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center'
    },
    myTicket: {
        fontSize: font.size.normal
    },
    myUsername : {
        fontSize: font.size.normal,
        fontWeight: 'bold',
        color: color.primary
    },
    history: {
        paddingLeft: 16,
        flexDirection: 'row',
        alignItems: 'center' 
    },
    historyText: {
        color: color.primary,
        marginRight: 4
    },
    historyIcon: {
        color: color.primary
    },

    myTicketButtons: {
        marginTop: 16,
        justifyContent: 'space-around'
    },

    topButton: {
        flex: 1,
        marginTop: 14
    },

    createTicketContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 8
    },
    createTicketCenter: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    createTicketText: {
        marginLeft: 8
    },
    
    todayText: {
        fontSize: 12
    },
    dateText: {
        color: color.textSecondary,
        fontSize: 12
    },
    bottomMenus: {
        flexDirection: 'column',
        marginTop: 16
    },
    touchableBottomMenu: {
        flexDirection: 'row',
        alignItems: 'center' 
    },
    captionText: {
        flex: 1,
        marginLeft: 12,
        marginVertical: 12
    },
    counterText: {
        color: color.textSecondary,
        textTransform: 'capitalize'
    },

    divider: {
        height: 0.5,
        backgroundColor: color.textSecondary
    },

    cardListWrapper: {
        flexBasis: '25%',
        flexDirection: 'column',
        paddingHorizontal: 4,
        marginVertical: 8,
    },
    cardWrapper: {
        flexBasis: '50%',
        flexDirection: 'column',
        paddingHorizontal: 4,
        marginVertical: 8,
    },
    cardContainer: {
        backgroundColor: '#ffffff',
        borderRadius: 8
    },
    cardTitle: {
        fontSize: font.size.normal,
        color: color.textLight,
        fontWeight: 'bold',
        marginVertical: 8,
        marginHorizontal: 8
    },
    cardValue: {
        marginVertical: 8,
        marginHorizontal: 8,
        fontSize: 32,
        color: color.textLight,
        fontWeight: 'bold',
        alignSelf: 'center'
    },
    itemContainer: {
        paddingVertical: 16,
    },
    itemHeader: {
        fontSize: font.size.normal,
        fontWeight: 'bold',
        color: color.textPrimary
    },
    itemCaption: {
        fontSize: font.size.normal,
        color: color.textSecondary
    },
    itemValue: {
        fontSize: 20,
        fontWeight: 'bold',
    }
});