import React, {useEffect, useState} from 'react';
import {
    View,
    Text,
    ScrollView,
    RefreshControl,
    TouchableOpacity
} from 'react-native';

//#region from 3rd party
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';
//#endregion

//#region 

//#region action & reducers
import {
    getJourney,
    getKecamatan,
    getSurvey,
    setLocation,
    getSurveySummary,
    getSurveySummaryTeamLead
} from '../../store/action/home';
import {
    setMe
} from '../../store/action/profile';

import { authClearAllState } from '../../store/action/auth';
//#endregion

import styles from './styles/home';
import { SET_LOADING, KEY_STORAGE_USER, KEY_STORAGE_TEAMNAME, KEY_STORAGE_USER_NAME, KEY_STORAGE_USERTYPE, KEY_STORAGE_KABUPATEN_ID, KEY_STORAGE_KABUPATEN_NAME } from '../../store/constant';
import Geolocation from 'react-native-geolocation-service';
import { Helper, Permissions } from '../../common';
import { showAlert } from '../../components/Alert';
import { store } from '../../store';


const ScreenLayout = ({
    navigation,
    route,

    home,
    profile,

    authClearAllState,
    setMe,
    setLocation,
    getJourney,
    getKecamatan,
    getSurvey,
    getSurveySummary,
    getSurveySummaryTeamLead,
    setLoading,
}) => {
    useEffect(() => {
        Promise.all([
          AsyncStorage.getItem(KEY_STORAGE_USER_NAME),
          AsyncStorage.getItem(KEY_STORAGE_USER),
          AsyncStorage.getItem(KEY_STORAGE_TEAMNAME),
          AsyncStorage.getItem(KEY_STORAGE_KABUPATEN_ID),
          AsyncStorage.getItem(KEY_STORAGE_KABUPATEN_NAME),
        ]).then(res => {
            let allInfoFulfilled = true;
            res.forEach((x, idx) => {
                allInfoFulfilled = allInfoFulfilled && (x !== undefined && x !== null ? true : false)
            });
            if (allInfoFulfilled) {
                methods.refreshData();
            } else {
              authClearAllState();
              Helper.sleep(750).then(() => {
                navigation.replace('Login');
              });
              
            }
        });
        
        return () => {};
    }, []);

    useEffect(() => {
      if (home.journey && home.journey.length) {
        methods.getSurveyData();
      }
      return () => {};
    }, [home.journey]);

    useEffect(() => {
      methods.calculateStock();
      return () => {};
    }, [home.survey]);

    useEffect(() => {
      methods.processSummaryData();
      return () => {};
    }, [home.summary]);

    const [watchID, setWatchID] = useState(null);
    const [stock, setStock] = useState(0);
    const [summaryList, setSummaryList] = useState([]);

    const getKecamatanJourney = (journeyList) => {
      let activeKecamatan = [];
      if (journeyList && journeyList.length) {
          journeyList.forEach(x => {
              if (x.kecamatan) {
                  activeKecamatan.push(...x.kecamatan.split(","));
              }
          });
      }
      return activeKecamatan;
    };
    const kecamatanFiltered = () => {
      const state = store.getState();
      let activeKecamatan = getKecamatanJourney(state.home.journey);
      return state.home.kecamatan && state.home.kecamatan.length ? state.home.kecamatan.filter(x => activeKecamatan.indexOf(x.name) >= 0) : []
    };


    const methods = {
        processSummaryData: () => {
          let res = [];
          let spgIds = [];
          if (home.summary.rowsVol && home.summary.rowsVol.length) {
            home.summary.rowsVol.forEach(row => {
              let idxSpg = spgIds.indexOf(row.spg_id)
              let vol = +(row.jumlah_beli || 0);
              let cc = 0;
              let ecc = 0;
              if (vol > 0) {
                ecc = 1;
              } else {
                cc = 1;
              }

              if (idxSpg < 0) {
                spgIds.push(row.spg_id);
                res.push({
                  spg_id: row.spg_id,
                  nama_spg: row.nama_spg,
                  cc,
                  ecc,
                  vol
                });
              } else {
                res[idxSpg].cc += cc;
                res[idxSpg].ecc += ecc;
                res[idxSpg].vol += vol;
              }
            });

            spgIds.forEach((x, idx) => {
              const countCC = (home.summary.rowsCC || []).filter(y => y.spg_id == x).length;
              const countEC = (home.summary.rowsEC || []).filter(y => y.spg_id == x).length;
              const countTotalSurvey = (home.summary.rowsSurvey || []).filter(y => y.spg_id == x).length;
              res[idx].cc = countCC;
              res[idx].ecc = countEC;
              res[idx].totalSurvey = countTotalSurvey;
            });
          }
          setSummaryList([...res.sort((a, b) => a.vol < b.vol ? 1 : a.vol > b.vol ? -1 : 0)]);
        },
        getSurveyData: async () => {
          setLoading(true);
          try{
            await getSurvey({
              journey_ids: home.journey.map(x => x.id),
              spg_id: profile.id
            });
          }finally{
            setLoading(false);
          }
        },
        calculateStock: () => {
          let allStock = 0;
          if (home.journey && home.journey.length) {
            home.journey.forEach(j => {
              allStock += j.stock || 0;
            });
          }

          let sold = 0;
          if (home.survey && home.survey.length) {
            home.survey.forEach(s => {
              sold += s.jumlah_beli || 0;
            });
          }

          setStock(allStock - sold);
        },
        getProfile: async () => {
            let res = null;
            let str = await AsyncStorage.getItem(KEY_STORAGE_USER)
            let teamName = await AsyncStorage.getItem(KEY_STORAGE_TEAMNAME)
            let userType = await AsyncStorage.getItem(KEY_STORAGE_USERTYPE)

            if (str) {
                try {
                    res = JSON.parse(str);
                    setMe({
                      ...res,
                      team_name: teamName,
                      user_type: userType
                    });
                } catch (e) {
                    res = null;
                }
            }
            return {
              ...res,
              team_name: teamName,
              user_type: userType
            };
        },
        refreshData: async () => {
            setLoading(true);
            try {
                let userProfile = await methods.getProfile();
                
                if (userProfile.user_type == "team_leader") {
                  await getSurveySummaryTeamLead({
                    team_id: userProfile.team_id
                  });
                } else {
                  let kabID = await AsyncStorage.getItem(KEY_STORAGE_KABUPATEN_ID)
                  let arr = [
                    getJourney({
                      spg_id: userProfile.id,
                      team_id: userProfile.team_id,
                      date: moment(new Date()).format("YYYY-MM-DD")
                    }),
                    getKecamatan({
                      kabupaten: kabID
                    }),
                    getSurveySummary({
                      spg_id: userProfile.id
                    })
                  ];
                  await Promise.all(arr);
                }
                
                setTimeout(() => {
                  methods.watchPosition();
                }, 500);
            } catch (err) {

            } finally {
                setLoading(false);
            }
        },
        calculateNearestLocation: (latitude, longitude) => {
          let nearestLocation = {name: ''};
          home.kecamatan.map((val, idx) => {
            let distance = Helper.distance(
              latitude,
              longitude,
              +val.latitude,
              +val.longitude,
            );
            if (idx == 0) {
              nearestLocation = {
                ...val,
                distance,
              };
            } else {
              if (distance < nearestLocation.distance) {
                nearestLocation = {
                  ...val,
                  distance,
                };
              }
            }
          });
          return nearestLocation;
        },
        calculateNearestJourney: (latitude, longitude) => {
          let nearestLocation = {name: ''};
          kecamatanFiltered().map((val, idx) => {
            let distance = Helper.distance(
              latitude,
              longitude,
              +val.latitude,
              +val.longitude,
            );
            if (idx == 0) {
              nearestLocation = {
                ...val,
                distance,
              };
            } else {
              if (distance < nearestLocation.distance) {
                nearestLocation = {
                  ...val,
                  distance,
                };
              }
            }
          });
          return nearestLocation;
        },
        saveLocation: async(latitude, longitude) => {
          const state = store.getState();
          if (state.home.journey && state.home.journey.length) {
            let nearestLocation = methods.calculateNearestLocation(latitude, longitude);

            let activeKecamatan = getKecamatanJourney(state.home.journey);
            
            if (activeKecamatan.indexOf(nearestLocation.name) < 0) {
              let nearestJourney = methods.calculateNearestJourney(latitude, longitude);
              let journey = null;
              if (state.home.journey && state.home.journey.length) {
                  let obj = state.home.journey.find(x => x.kecamatan.split(",").indexOf(nearestJourney.name) >= 0);
                  if (obj) {
                      journey = obj;
                  }
              }
              setLocation({
                spg_id: state.profile.id,
                journey_id: journey.id,
                longitude: ''+latitude,
                latitude: ''+longitude,
                kecamatan_device: "",
                kecamatan_survey: nearestJourney.name
              });
            }
          }
        },
        watchPosition: async() => {
          if (watchID) {
            Geolocation.clearWatch(watchID);
          }
          let permission = await Permissions.requestLocationPermission();
          if (permission) {
            let id = Geolocation.watchPosition(
              ({ coords: { latitude, longitude } }) => {
                methods.saveLocation(latitude, longitude);
              },
              (error) => {
                console.log(error);
              },
              {
                accuracy: {
                  android: 'balanced',
                  ios: 'hundredMeters',
                },
                enableHighAccuracy: true,
                distanceFilter: 100,
                interval: 30000,
                fastestInterval: 5000,
                forceRequestLocation: true,
                showLocationDialog: true,
              },
            );
            setWatchID(id);
          }
        },
        createSurvey: async() => {
          if (!(home.journey && home.journey.length)) {
            showAlert({
                title: "Warning",
                detail: "Data Journey tidak ditemukan. Mohon refresh ulang",
                type: "warning"
            });
          } else {
            navigation.navigate("CreateSurvey", {
              onSuccess: async () => {
                methods.getSurveyData();
                let userProfile = await methods.getProfile();
                getSurveySummary({
                  spg_id: userProfile.id
                })
              }
            })
          }
        }
    }

    return (
        <>
      {/*  */}
      <ScrollView style={styles.container}
        refreshControl={
          <RefreshControl
            refreshing={false}
            onRefresh={() => {
              methods.refreshData()
            }}
          />
        }>
        <View style={styles.rowContainer}>
          <View style={styles.row}>
            <View style={styles.leftTitle}>
              <Text style={styles.myUsername} >{profile.nama_spg || profile.nama}</Text>
            </View>
          </View>
        </View>
        
        <View style={styles.rowContainer}>
          <View style={styles.row}>
            <Text style={[styles.leftTitle, styles.todayText]}>{home.journey && home.journey.length ? `Week ${home.journey[0].week || 0}` : ''}</Text>
            <Text style={styles.dateText}>{moment(new Date()).format('DD MMM YYYY')}</Text>
          </View>
          <View style={styles.bottomMenus}>
            <View style={[styles.touchableBottomMenu]} onPress={() => {  } }>
              <Icon name="account-group" style={{color: '#00695C', fontSize: 20}} />
              <Text style={styles.captionText}>Team</Text>
              <Text style={styles.counterText}>{profile.team_name}</Text>
            </View>
            {profile.user_type !== "team_leader" && (
              <>
                <View style={styles.divider} />
                <View style={[styles.touchableBottomMenu]} onPress={() => {  } }>
                  <Icon name="cigar" style={{color: '#6C91E9', fontSize: 20}} />
                  <Text style={styles.captionText}>Jumlah Stock</Text>
                  <Text style={styles.counterText}>{stock}</Text>
                </View>
                <View style={styles.divider} />
                <View style={[styles.touchableBottomMenu]} onPress={() => {  } }>
                  <Icon name="map-marker-multiple" style={{color: '#CB5C5A', fontSize: 20}}/>
                  <View style={{
                    flexDirection: 'column',
                    flex: 1,
                    marginLeft: 12,
                    marginVertical: 12,
                    justifyContent: 'center'
                  }} >
                    <Text style={[styles.captionText, {marginLeft: 0, marginVertical: 0, textAlignVertical: 'center'}]} >Kecamatan</Text>
                  </View>
                  <View style={{
                    flexDirection: 'column',
                    flex: 1,
                  }} >
                    <Text style={[styles.counterText, {textAlign: 'right'}]} >{getKecamatanJourney(home.journey).join(", ")}</Text>
                  </View>
                </View>
              </>
            )}
          </View>
        </View>

        {profile.user_type !== "team_leader" && (
          <View style={styles.rowContainer}>
              <TouchableOpacity style={styles.createTicketContainer} onPress={() => { methods.createSurvey() } }>
                  <View style={styles.createTicketCenter}>
                  <Icon style={{fontSize: 18}} name="plus" />
                  <Text style={styles.createTicketText} >Create New</Text>
                  </View>
                  <Icon style={{fontSize: 18}} name="chevron-right" />
              </TouchableOpacity>
          </View>
        )}

        <View style={[styles.rowContainer, styles.row, {backgroundColor: 'transparent', flexWrap: 'wrap', paddingVertical: 0}]}>
          <View style={styles.cardWrapper}>
            <View style={[styles.cardContainer, {backgroundColor: '#78909C'}]}>
              <Text style={styles.cardTitle} >CC</Text>
              <View style={[styles.divider, {backgroundColor: '#ffffff'}]} />
              <Text style={styles.cardValue} >{home.summary.totalCC || 0}</Text>
            </View>
          </View>
          <View style={styles.cardWrapper}>
            <View style={[styles.cardContainer, {backgroundColor: '#4DB6AC'}]}>
              <Text style={styles.cardTitle} >ECC</Text>
              <View style={[styles.divider, {backgroundColor: '#ffffff'}]} />
              <Text style={styles.cardValue} >{home.summary.totalEC || 0}</Text>
            </View>
          </View>
          <View style={styles.cardWrapper}>
            <View style={[styles.cardContainer, {backgroundColor: '#9FA8DA'}]}>
              <Text style={styles.cardTitle} >VOL</Text>
              <View style={[styles.divider, {backgroundColor: '#ffffff'}]} />
              <Text style={styles.cardValue} >{home.summary.totalVol || 0}</Text>
            </View>
          </View>
          <View style={styles.cardWrapper}>
            <View style={[styles.cardContainer, {backgroundColor: '#00B8D4'}]}>
              <Text style={styles.cardTitle} >SURVEY</Text>
              <View style={[styles.divider, {backgroundColor: '#ffffff'}]} />
              <Text style={styles.cardValue} >{home.summary.totalSurvey || 0}</Text>
            </View>
          </View>
        </View>

        {profile.user_type === "team_leader" && (
          <View style={[styles.rowContainer, {paddingTop: 0}]}>
            {summaryList.map((row, idx) => (
              <>
                <View style={styles.itemContainer} key={`itms-${idx}`}>
                  <Text style={styles.itemHeader}>
                    {row.nama_spg}
                  </Text>
                  <View style={[styles.row, {marginTop: 8}]}>
                    <View style={styles.cardListWrapper}>
                      <Text style={styles.itemCaption}>
                        CC
                      </Text>
                      <Text style={[styles.itemValue, {color: '#78909C'}]}>
                        {row.cc}
                      </Text>
                    </View>
                    <View style={styles.cardListWrapper}>
                      <Text style={styles.itemCaption}>
                        ECC
                      </Text>
                      <Text style={[styles.itemValue, {color: '#4DB6AC'}]}>
                        {row.ecc}
                      </Text>
                    </View>
                    <View style={styles.cardListWrapper}>
                      <Text style={styles.itemCaption}>
                        VOL
                      </Text>
                      <Text style={[styles.itemValue, {color: '#9FA8DA'}]}>
                        {row.vol}
                      </Text>
                    </View>
                    <View style={styles.cardListWrapper}>
                      <Text style={styles.itemCaption}>
                        SURVEY
                      </Text>
                      <Text style={[styles.itemValue, {color: '#00B8D4'}]}>
                        {row.totalSurvey}
                      </Text>
                    </View>
                  </View>
                </View>
                <View style={styles.divider} key={`dvds-${idx}`}/>
              </>
            )
            )}
            
            {/* <View style={styles.itemContainer}>
              <Text style={styles.itemHeader}>
                Nama SPG
              </Text>
              <View style={[styles.row, {marginTop: 8}]}>
                <View style={styles.cardWrapper}>
                  <Text style={styles.itemCaption}>
                    CC
                  </Text>
                  <Text style={[styles.itemValue, {color: '#78909C'}]}>
                    24
                  </Text>
                </View>
                <View style={styles.cardWrapper}>
                  <Text style={styles.itemCaption}>
                    ECC
                  </Text>
                  <Text style={[styles.itemValue, {color: '#4DB6AC'}]}>
                    24
                  </Text>
                </View>
                <View style={styles.cardWrapper}>
                  <Text style={styles.itemCaption}>
                    VOL
                  </Text>
                  <Text style={[styles.itemValue, {color: '#9FA8DA'}]}>
                    24
                  </Text>
                </View>
              </View>
            </View>
            <View style={styles.divider} /> */}
          </View>
        )}
      </ScrollView>
    </>
    );
};

const mapStateToProps = (state) => {
    return {
        home: state.home,
        profile: state.profile
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        setLoading: (payload) => dispatch({ type: SET_LOADING, payload }),
        getJourney: async ({spg_id, team_id, date}) => await dispatch(getJourney({spg_id, team_id, date})),
        getKecamatan: async ({kabupaten}) => await dispatch(getKecamatan({kabupaten})),
        getSurvey: async ({journey_ids, spg_id}) => await dispatch(getSurvey({journey_ids, spg_id})),
        getSurveySummary: async ({spg_id}) => await dispatch(getSurveySummary({spg_id})),
        getSurveySummaryTeamLead: async ({team_id}) => await dispatch(getSurveySummaryTeamLead({team_id})),
        setMe: (param) => dispatch(setMe(param)),
        setLocation: async (payload) => await dispatch(setLocation(payload)),
        authClearAllState: () => dispatch(authClearAllState()),
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ScreenLayout);