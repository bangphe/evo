import React, {useEffect, useState, useRef} from 'react';
import {
    View, 
    Keyboard,
    Dimensions,
    Animated
} from 'react-native';

//#region from 3rd party
import AsyncStorage from '@react-native-community/async-storage';
import Image from 'react-native-fast-image'
import {connect} from 'react-redux';
//#endregion

//#region custom component
import Button from '../../components/Button';
import TextField from '../../components/TextField';
import DropDown from '../../components/DropDown';
//#region 

//#region action & reducers
import {
    authLogin,
    authClearAllState
} from '../../store/action/auth';
//#endregion

import { KEY_STORAGE_USER_NAME, SET_LOADING, KEY_STORAGE_USER, KEY_STORAGE_TEAMNAME, KEY_STORAGE_USERTYPE, KEY_STORAGE_KABUPATEN_ID, KEY_STORAGE_KABUPATEN_NAME } from '../../store/constant';
import { Validator, Helper } from '../../common';
import { wrapperNoScroll } from '../../hoc';

import styles from './styles/login';
import { showAlert } from '../../components/Alert';
import { color } from '../../values';

const LoginLayout = ({
    navigation,
    authLogin,
    authClearAllState,

    setLoading,
}) => {
    const [error, setError] = useState({
        username: null,
        password: null,
    });

    const [model, setModel] = useState({
        username: '',
        password: '',
    });

    const dimension = Dimensions.get('screen');

    const [inputContainerHeight, setInputContainerHeight] = useState(128);
    const [loginButtonHeight, setLoginButtonHeight] = useState(12);

    let keyboardWillShowListener;
    let keyboardWillHideListener;

    const mgBtAnim = useRef(new Animated.Value(16)).current;

    const mgBtChange = (to) => {
        Animated.timing(mgBtAnim, {
            toValue: to,
            duration: 250
        }).start();
    };

    useEffect(() => {
        setLoading(true);
        Promise.all([
            AsyncStorage.getItem(KEY_STORAGE_USER_NAME),
            AsyncStorage.getItem(KEY_STORAGE_USER),
            AsyncStorage.getItem(KEY_STORAGE_TEAMNAME),
            AsyncStorage.getItem(KEY_STORAGE_KABUPATEN_ID),
            AsyncStorage.getItem(KEY_STORAGE_KABUPATEN_NAME),
        ]).then(res => {
            let allInfoFulfilled = true;
            res.forEach((x, idx) => {
                allInfoFulfilled = allInfoFulfilled && (x !== undefined && x !== null ? true : false)
            });
            if (allInfoFulfilled) {
                Helper.sleep(750).then(() => {
                    setLoading(false);
                    navigation.replace('HomeLayout');
                });
            } else {
                setLoading(false);
                authClearAllState();
            }
        });
        keyboardWillShowListener = Keyboard.addListener(
            'keyboardWillShow',
            _keyboardWillShow,
        );
        keyboardWillHideListener = Keyboard.addListener(
            'keyboardWillHide',
            _keyboardWillHide,
        );
        return () => {
            keyboardWillShowListener.remove();
            keyboardWillHideListener.remove();
        };
    }, []);

    const _keyboardWillShow = (e) => {
        mgBtChange(e.endCoordinates.height);
    };

    const _keyboardWillHide = () => {
        mgBtChange(16);
    };

    const methods = {
        login: async () => {
            let validationSuccess = true;
            let err = {};
            //#region validation
            if (!Validator.required(model.username)) {
                err.username = 'Username harus diisi'
                validationSuccess = false;
            }
            if (!Validator.required(model.password)) {
                err.password = 'Password harus diisi'
                validationSuccess = false;
            }
            //#endregion

            if (!validationSuccess) {
                setError(err);
            } else {
                try {
                    setLoading(true);
                    let res = await authLogin({...model, username: model.username.toLowerCase()});
                    if (!res.errors) {
                        await AsyncStorage.setItem(KEY_STORAGE_USER_NAME, ''+res.user.username);
                        await AsyncStorage.setItem(KEY_STORAGE_USER, JSON.stringify(res.user));
                        await AsyncStorage.setItem(KEY_STORAGE_TEAMNAME, res.team_name);
                        await AsyncStorage.setItem(KEY_STORAGE_USERTYPE, res.user_type);
                        await AsyncStorage.setItem(KEY_STORAGE_KABUPATEN_ID, '' + res.kabupaten?.id || '');
                        await AsyncStorage.setItem(KEY_STORAGE_KABUPATEN_NAME, res.kabupaten?.name || '');
                        
                        navigation.replace('HomeLayout');
                    } else {
                        if (res.errors && res.errors.password) {
                            let msg = res.errors.password[0];
                            showAlert({
                                title: "Warning",
                                detail: msg,
                                type: "warning"
                            })
                        } else {
                            showAlert({
                                title: "Warning",
                                detail: 'Something went wrong.',
                                type: "warning"
                            })
                        }
                    }
                } catch(err) {

                } finally {
                    setLoading(false);
                }
            }
        },
        onLayout: (event, type) => {
            const {
                height,
            } = event.nativeEvent.layout;
            switch (type) {
                case 'inputContainer':
                    setInputContainerHeight(height);       
                    break;
                case 'loginButton':
                    setLoginButtonHeight(height);
                    break;
                default:
                    break;
            }
        }
    };

    return (
        <View 
            style={styles.mainWrapper}
            >
            <View style={{backgroundColor: 'transparent', flex: 1, flexDirection: 'column-reverse'}} >
                <View style={styles.logoContainer}>
                    <Image
                        style={[
                            styles.logo, 
                            {
                                width: dimension.width,
                                height: dimension.height,
                            }
                        ]}
                        source={require('../../assets/images/login-slog.png')}
                    />
                </View>
                <Animated.View style={[styles.inputContainer, {marginBottom: mgBtAnim}]} onLayout={(event) => methods.onLayout(event, 'inputContainer')}>
                    <TextField
                        label="Username"
                        error={error.username}
                        value={model.username}
                        autoCapitalize="none"
                        onChangeText={(text) => {
                            setError({...error, username: null});
                            setModel({...model, username: text});
                        }}
                    />
                    
                    <TextField
                        label="Password"
                        error={error.password}
                        value={model.password}
                        secureTextEntry={true}
                        onChangeText={(text) => {
                            setError({...error, password: null});
                            setModel({...model, password: text});
                        }}
                        onSubmitEditing={() => methods.login()}
                        returnKeyType='done'
                    />

                    <Button
                        onLayout={(event) => methods.onLayout(event, 'loginButton')}
                        marginTop={14}
                        text="LOGIN"
                        color={color.primary}
                        textColor="#ffffff"
                        size="lg"
                        textStyle={{fontWeight: 'normal'}}
                        paddingVertical={12}
                        borderRadius={loginButtonHeight / 2}
                        onPress={() => methods.login()}
                    />

                </Animated.View>
            </View>
        </View>
    );
};

const mapStateToProps = (state) => {
    return {
        
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        authLogin: async (payload) => await dispatch(authLogin(payload)),
        authClearAllState: () => dispatch(authClearAllState()),
        setLoading: (payload) => dispatch({ type: SET_LOADING, payload }),
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(wrapperNoScroll(LoginLayout, true));
