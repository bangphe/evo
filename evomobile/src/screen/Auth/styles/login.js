import {StyleSheet} from 'react-native';
import {color, font} from '../../../values';

export default StyleSheet.create({
    mainWrapper: {
        backgroundColor: '#ffffff',
        flex: 1,
        flexDirection: 'column',
    },
    logoContainer: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        flexDirection: 'row',
    },
    logo: {
        resizeMode: "cover",
    },
    inputContainer: {
        borderRadius: 8,
        // position: 'absolute',
        // bottom: 16,
        // left: 16,
        // right: 16,
        marginHorizontal: 16,
        backgroundColor: '#ffffff',
        padding: 16,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.5,
        shadowRadius: 1.0,

        elevation: 8,

        zIndex: 1,
    },
});
