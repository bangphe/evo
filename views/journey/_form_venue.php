<?php

use yii\helpers\Html;
use wbraganca\dynamicform\DynamicFormWidget;
use app\models\Kecamatan;

?>

<?php DynamicFormWidget::begin([
    'widgetContainer' => 'dynamicform_inner',
    'widgetBody' => '.container-venues',
    'widgetItem' => '.venue-item',
    // 'limit' => 4,
    'min' => 1,
    'insertButton' => '.add-venue',
    'deleteButton' => '.remove-venue',
    'model' => $modelsSpgVenue[0],
    'formId' => 'dynamic-form',
    'formFields' => [
        'kecamatan',
        'venue'
    ],
]); ?>

<table class="table table-bordered">
    <thead>
        <tr>
            <th class="text-center" style="vertical-align: middle; width: 30%;">Kecamatan</th>
            <th class="text-center" style="vertical-align: middle;">Venue</th>
            <th class="text-center">
                <button type="button" class="add-venue btn btn-success m-btn m-btn--icon m-btn--icon-only"><i class="glyphicon glyphicon-plus"></i></button>
            </th>
        </tr>
    </thead>
    <tbody class="container-venues">
    <?php foreach ($modelsSpgVenue as $indexVenue => $modelVenue): ?>
        <?= $form->errorSummary($modelVenue); ?>
        <?php
            // die(var_dump($indexVenue));
            // if (!$modelVenue->isNewRecord) {
            //     echo Html::activeHiddenInput($modelVenue, "[{$indexJourney}][{$indexVenue}]id");
            // }
        ?>
        <tr class="venue-item">
            <td class="vcenter">
                <?php if ($modelSpg->isNewRecord) { ?>
                <?= $form->field($modelVenue, "[{$indexJourney}][{$indexVenue}]kecamatan")->label(false)->dropDownList(Kecamatan::getDataKecamatan(3516), ['class'=>'form-control select2']) ?>
                <?php } else { ?>
                    <?= $form->field($modelVenue, "[{$indexJourney}][{$indexVenue}]kecamatan")->label(false)->dropDownList(Kecamatan::getDataKecamatan($kabupaten->id), ['class'=>'form-control select2']) ?>
                <?php } ?>
            </td>
            <td class="vcenter">
                <?= $form->field($modelVenue, "[{$indexJourney}][{$indexVenue}]venue")->label(false)->textInput(['maxlength' => true, 'class' => 'form-control']) ?>
            </td>
            <td class="text-center vcenter" style="width: 90px;">
                <button type="button" class="remove-venue btn btn-danger m-btn m-btn--icon m-btn--icon-only"><i class="glyphicon glyphicon-minus"></i></button>
            </td>
        </tr>
     <?php endforeach; ?>
    </tbody>
</table>
<?php DynamicFormWidget::end(); ?>