<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SpgJourney */

$this->title = 'Create SPG Journey';
$this->params['breadcrumbs'][] = ['label' => 'Data SPG Journey', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="spg-journey-create">
    <div class="m-portlet m-portlet--tab">  
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="fa fa-users"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        <?= $this->title; ?>
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <?= $this->render('_form', [
                'modelSpg' => $modelSpg,
                'modelsSpgJourney' => $modelsSpgJourney,
                'modelsSpgVenue' => $modelsSpgVenue,
            ]) ?>
        </div>
    </div>
</div>
