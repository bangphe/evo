<?php

use app\models\Kabupaten;
use app\models\Kecamatan;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SpgJourney */

$this->title = 'Update Spg Journey: ' . $model->spg->nama_spg;
$this->params['breadcrumbs'][] = ['label' => 'Data Journey SPG', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<div class="spg-journey-update">
    <div class="m-portlet m-portlet--tab">  
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="fa fa-users"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        <?= $this->title; ?>
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="team-form">
                <?php $form = ActiveForm::begin([
                    'id' => 'team-form',
                    'options' => [
                        'class' => 'm-form m-form--fit m-form--label-align-right'
                    ]
                ]); ?>

                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'week')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'journey_date')->textInput(['maxlength' => true, 'class' => 'form-control datepicker']) ?>
                    </div>
                </div>
                
                <div class="row">
                    <!-- <div class="col-sm-6">
                        <?//= $form->field($model, 'kecamatan')->dropDownList(Kecamatan::getDataKecamatanByName($model->spg->team->area), ['class'=>'form-control select2', 'multiple'=>'multiple']) ?>
                    </div> -->
                    <div class="col-sm-12">
                        <?= $form->field($model, 'stock')->textInput(['maxlength' => true, 'type' => 'number']) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center" style="vertical-align: middle; width: 30%;">Kecamatan</th>
                                    <th class="text-center" style="vertical-align: middle;">Venue</th>
                                </tr>
                            </thead>
                            <tbody class="container-venues">
                            <?php foreach ($modelVenues as $indexVenue => $modelVenue): ?>
                                <?= $form->errorSummary($modelVenue); ?>
                                <?php
                                    if (! $modelVenue->isNewRecord) {
                                        echo Html::activeHiddenInput($modelVenue, "[{$indexVenue}]id");
                                    }
                                ?>
                                <tr class="venue-item">
                                    <td class="vcenter">
                                        <?= $form->field($modelVenue, "[{$indexVenue}]kecamatan")->label(false)->dropDownList(Kecamatan::getDataKecamatan($kabupaten['id']), ['class'=>'form-control select2']) ?>
                                    </td>
                                    <td class="vcenter">
                                        <?= $form->field($modelVenue, "[{$indexVenue}]venue")->label(false)->textInput(['maxlength' => true, 'class' => 'form-control']) ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="m-form__actions m-form__actions">
                    <div class="row">
                        <div class="col-lg-3"></div>
                        <div class="col-lg-6">
                            <?= Html::submitButton('<i class="fa fa-check"></i> Save', ['class' => 'btn btn-success']) ?>
                            <button type="button" class="btn btn-secondary" onclick="goBack()"><i class="fa fa-times"></i> Cancel</button>
                        </div>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
