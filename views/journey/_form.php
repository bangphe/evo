<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;
use app\models\Kecamatan;
use app\models\Spg;
use app\models\Team;

/* @var $this yii\web\View */
/* @var $model app\models\SpgJourney */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- <style>
.datepicker {
    width: 100% !important;
}
</style> -->

<div class="spg-journey-form">
    <?php $form = ActiveForm::begin([
        'id' => 'dynamic-form',
        'options' => [
            'class' => 'm-form m-form--fit m-form--label-align-right'
        ]
    ]); ?>

    <div class="row">
        <div class="col-sm-6">
        <?php if($modelSpg->isNewRecord) { ?>
            <?= $form->field($modelSpg, 'id_team')->dropDownList(Team::getDataTeam(), ['prompt'=>'-- Pilih Tim --', 'class'=>'form-control m-input m-input--square', 'required'=>'required', 'onchange'=>'getTeam(this.value, "new")']) ?>
        <?php } else { ?>
            <?= $form->field($modelSpg, 'team_id')->dropDownList(Team::getDataTeam(), ['prompt'=>'-- Pilih Tim --', 'class'=>'form-control m-input m-input--square', 'required'=>'required', 'disabled'=>'disabled']) ?>
        <?php } ?>
        </div>
        <?php if($modelSpg->isNewRecord) { ?>
        <div class="col-sm-6">
        <?= $form->field($modelSpg, 'id_spgs')->dropDownList([], ['prompt'=>'Pilih Tim Dahulu', 'class'=>'form-control m-input m-input--square select2', 'required'=>'required', 'multiple'=>'multiple']) ?>
        </div>
        <?php } ?>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading"><h4><i class="m-menu__link-icon fa fa-map-marker-alt"></i> Journey SPG</h4></div>
        <div class="panel-body">
             <?php DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-items', // required: css class selector
                'widgetItem' => '.item', // required: css class
                'limit' => 7, // the maximum times, an element can be cloned (default 999)
                'min' => 1, // 0 or 1 (default 1)
                'insertButton' => '.add-item', // css class
                'deleteButton' => '.remove-item', // css class
                'model' => $modelsSpgJourney[0],
                'formId' => 'dynamic-form',
                'formFields' => [
                    'week',
                    'journey_date',
                    'kecamatan',
                    'stock',
                ],
            ]); ?>

            <div class="container-items">
            <?php foreach ($modelsSpgJourney as $indexJourney => $modelSpgJourney): ?>
                <?= $form->errorSummary($modelSpgJourney); ?>
                <div class="item panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title-idx"></h3>
                        <?php if($modelSpgJourney->isNewRecord) : ?>
                        <div class="pull-right">
                            <button type="button" class="add-item btn btn-success m-btn m-btn--icon m-btn--icon-only"><i class="glyphicon glyphicon-plus"></i></button>
                            <button type="button" class="remove-item btn btn-danger m-btn m-btn--icon m-btn--icon-only"><i class="glyphicon glyphicon-minus"></i></button>
                        </div>
                        <?php endif; ?>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <?php
                            // necessary for update action
                            if (!$modelSpgJourney->isNewRecord) {
                                echo Html::activeHiddenInput($modelSpgJourney, "[{$indexJourney}]id");
                            }
                        ?>
                        <div class="row">
                            <div class="col-sm-6">
                                <?= $form->field($modelSpgJourney, "[{$indexJourney}]week")->textInput(['maxlength' => true, 'type' => 'number']) ?>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($modelSpgJourney, "[{$indexJourney}]journey_date")->textInput(['maxlength' => true, 'class' => 'form-control datepicker']) ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <?= $form->field($modelSpgJourney, "[{$indexJourney}]stock")->textInput(['maxlength' => true, 'type' => 'number']) ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <?= $modelSpg->isNewRecord ? $this->render('_form_venue', [
                                    'form' => $form,
                                    'indexJourney' => $indexJourney,
                                    'modelSpg' => $modelSpg,
                                    'modelsSpgVenue' => $modelsSpgVenue[$indexJourney],
                                ]) : $this->render('_form_venue', [
                                    'form' => $form,
                                    'indexJourney' => $indexJourney,
                                    'modelSpg' => $modelSpg,
                                    'modelsSpgVenue' => $modelsSpgVenue[$indexJourney],
                                    'kabupaten' => $kabupaten,
                                ]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
            </div>
            <?php DynamicFormWidget::end(); ?>
        </div>
    </div>

    <div class="m-form__actions m-form__actions">
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <?= Html::submitButton('<i class="fa fa-check"></i> Save', ['class' => 'btn btn-success']) ?>
                <button type="button" class="btn btn-secondary" onclick="goBack()"><i class="fa fa-times"></i> Cancel</button>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>

<?php
$js = <<<'EOD'

jQuery(".dynamicform_wrapper").on("afterInsert", function(e, item) {
    $("#"+$(item).find('.select2')[0].id).empty();
    $("#"+$(item).find('.select2')[0].id).select2({data: model.data()});
    
    jQuery(".dynamicform_wrapper").each(function(index) {
        $(".datepicker").datepicker({
            format: "yyyy-mm-dd",
            todayHighlight: true,
            autoclose: true,
        });
    });
});

jQuery(".dynamicform_inner").on("afterInsert", function(e, item) {
    $("#"+$(item).find('.select2')[0].id).empty();
    $("#"+$(item).find('.select2')[0].id).select2({data: model.data()});
});

EOD;

$this->registerJs($js);
?>

<script>
    var url = '<?= Url::to(['/journey/getteam']); ?>';
    var model = {};
    model.data = ko.observableArray([]);
    model.dataSpg = ko.observableArray([]);
    function getTeam(id, isUpdate=null) {        
        $.ajax({
            url: url,
            type: "GET",
            cache: false,
            data: { "team_id": id },
            success: function (res) {
                $("#team-spg").val(res.team + ' - ' + res.area);
                var data = $.map(res.data_kecamatan, function(obj) {
                    return { id: obj.name, text: obj.name };
                });
                var dataSpg = $.map(res.data_spg, function(obj) {
                    return { id: obj.id, text: obj.nama_spg };
                });
                model.data(data);
                model.dataSpg(dataSpg);
                // console.log("dt :: ", data);
                if (isUpdate) {
                    $(".select2").empty();
                    $(".select2").select2({data: data});

                    $("#spg-id_spgs").empty();
                    $("#spg-id_spgs").select2({data: dataSpg});
                }
            },
            error: function (data) {
                console.log(data);
            }
        });
    }
    
    $(document).ready(function () {
        var elementExists = document.getElementById("spg-team_id");
        if (elementExists) {
            getTeam($('#spg-team_id').val());
        }
    });
</script>