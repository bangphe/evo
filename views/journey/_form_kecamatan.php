<?php

use yii\helpers\Html;
use wbraganca\dynamicform\DynamicFormWidget;

?>
<?php DynamicFormWidget::begin([
    'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
    'widgetBody' => '.container-items', // required: css class selector
    'widgetItem' => '.item', // required: css class
    'limit' => 7, // the maximum times, an element can be cloned (default 999)
    'min' => 1, // 0 or 1 (default 1)
    'insertButton' => '.add-item', // css class
    'deleteButton' => '.remove-item', // css class
    'model' => $modelsSpgJourney[0],
    'formId' => 'dynamic-form',
    'formFields' => [
        'week',
        'journey_date',
        'kecamatan',
        'stock',
    ],
]); ?>

    <table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Kecamatan</th>
            <th style="width: 450px;">Venue</th>
            <th class="text-center" style="width: 90px;">
                <button type="button" class="add-item btn btn-success btn-xs"><span class="fa fa-plus"></span></button>
            </th>
        </tr>
    </thead>
    <tbody class="container-items">
        <?php foreach ($modelsSpgJourney as $indexJourney => $modelJourney): ?>
            <tr class="item">
                <td class="vcenter">
                    <?php
                        // necessary for update action.
                        if (! $modelJourney->isNewRecord) {
                            echo Html::activeHiddenInput($modelJourney, "[{$indexJourney}]id");
                        }
                    ?>
                    <?= $form->field($modelJourney, "[{$indexJourney}]week")->label(false)->textInput(['maxlength' => true]) ?>
                </td>
                <td>
                    <?= $this->render('_form_venue', [
                        'form' => $form,
                        'indexJourney' => $indexJourney,
                        'modelsSpgVenue' => $modelsSpgVenue[$indexJourney],
                    ]) ?>
                </td>
                <td class="text-center vcenter" style="width: 90px; verti">
                    <button type="button" class="remove-house btn btn-danger btn-xs"><span class="fa fa-minus"></span></button>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

<?php DynamicFormWidget::end(); ?>