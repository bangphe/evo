<?php

use app\models\Kabupaten;
use app\models\Kecamatan;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SpgJourney */

$this->title = 'Update Spg Journey: ' . $modelSpg->nama_spg;
$this->params['breadcrumbs'][] = ['label' => 'Data Journey SPG', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>

<div class="spg-journey-update">
    <div class="m-portlet m-portlet--tab">  
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="fa fa-users"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        <?= $this->title; ?>
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <?= $modelSpg->isNewRecord ? $this->render('_form', [
                'modelSpg' => $modelSpg,
                'modelsSpgJourney' => $modelsSpgJourney,
                'modelsSpgVenue' => $modelsSpgVenue,
            ]) : $this->render('_form', [
                'modelSpg' => $modelSpg,
                'modelsSpgJourney' => $modelsSpgJourney,
                'modelsSpgVenue' => $modelsSpgVenue,
                'kabupaten' => $kabupaten,
            ]) ?>
        </div>
    </div>
</div>
