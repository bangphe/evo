<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\Spg;
use app\models\SpgLocation;
use app\components\MyFormatter;

AppAsset::register($this);
$theme = Url::base().'/themes/metronic';
$request = Yii::$app->request;
if ($request->enableCsrfValidation) {
    $this->registerMetaTag(['name' => 'csrf-param', 'content' => $request->csrfParam]);
    $this->registerMetaTag(['name' => 'csrf-token', 'content' => $request->getCsrfToken()]);
}

// Get unread message
$notifUnread = SpgLocation::getUnread();
$allNotif = SpgLocation::getAllNotif();
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
    <!-- begin::Head -->
    <head>
        <meta charset="utf-8" />
        <title>EVO | <?= $this->title; ?></title>
        <meta name="description" content="evo.com">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

        <?php $this->head() ?>
        <!--begin::Web font -->

        <script src="<?= $theme; ?>/app/js/knockout.js"></script>
        <script src="<?= $theme; ?>/app/js/lodash.js"></script>
        <script src="<?= $theme; ?>/app/mapbox/mapbox-gl.js"></script>
        <!--<script src="<?= $theme; ?>/app/mapbox/mapbox-gl-geocoder.min.js"></script>-->
        <script src="<?= $theme; ?>/app/mapbox/mapbox-gl-directions.js"></script>
        <script src="<?= $theme; ?>/app/mapbox/mapbox-gl-csp.js"></script>

        <script>
            model = {
                test: ko.observable(true),
            }
            mapboxgl.workerUrl = "<?= $theme; ?>/app/mapbox/mapbox-gl-csp-worker.js";
        </script>
        
        <!--begin::Web font -->
        <script src="<?= $theme; ?>/app/js/webfont.js"></script>
        <script>
            WebFont.load({
                google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
                active: function() {
                    sessionStorage.fonts = true;
                }
            });
        </script>
        <!--end::Web font -->

        <!--begin::Base Styles -->  
        <link href="<?= $theme; ?>/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
        <link href="<?= $theme; ?>/demo/demo11/base/style.bundle.css" rel="stylesheet" type="text/css" />
        <!--end::Base Styles -->

        <!--begin::Page Vendors -->
        <link href="<?= $theme; ?>/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
        <link href="<?= $theme; ?>/app/mapbox/mapbox-gl.css" rel="stylesheet" type="text/css" />
        <!--<link href="<?= $theme; ?>/app/mapbox/mapbox-gl-geocoder.css" rel="stylesheet" type="text/css" />-->
        <link rel="stylesheet" href="<?= $theme; ?>/app/mapbox//mapbox-gl-directions.css" type="text/css"/>
        <!--end::Page Vendors -->

        <!-- KnockOut JS -->
        <script src="<?= $theme; ?>/knockout/knockout-min.js"></script>
        <script src="<?= $theme; ?>/knockout/knockout.mapping-latest.js"></script>
        <script src="<?= $theme; ?>/knockout/knockout-switch-case.min.js"></script>
        <script src="<?= $theme; ?>/kendo/kendo.all.min.js"></script>
        <script src="<?= $theme; ?>/moment/moment.min.js" type="text/javascript"></script>
        
        <link rel="shortcut icon" href="<?= $theme; ?>/favicon.ico" />
        
        <style>
            form div.required label.control-label:after {
                content:" * ";
                color:red;
            }
            .panel-title-tunggakan {
                font-size: 15px;
                font-weight: 500;
            }
        </style>
    </head>
    <!-- end::Head -->

    <!-- begin::Body -->
    <body class="m-content--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-light m-aside--offcanvas-default">
        <?php $this->beginBody() ?>
        <!-- begin:: Page -->
        <div class="m-grid m-grid--hor m-grid--root m-page">
        <!-- BEGIN: Header -->
        <header id="m_header" class="m-grid__item m-header" m-minimize-offset="200" m-minimize-mobile-offset="200" style="background-color: #fff !important;">
            <div class="m-container m-container--fluid m-container--full-height">
                <div class="m-stack m-stack--ver m-stack--desktop">     
                    <!-- BEGIN: Brand -->
                    <div class="m-stack__item m-brand  m-brand--skin-light " style="background-color: #fff !important;">
                        <div class="m-stack m-stack--ver m-stack--general m-stack--fluid">
                            <div class="m-stack__item m-stack__item--middle m-brand__logo">
                                <a href="<?= Url::to(['/index']); ?>" class="m-brand__logo-wrapper">
                                    <img alt="" src="<?= $theme; ?>/app/media/img/logo.png" width="60px" />
                                </a>  
                            </div>
                            <div class="m-stack__item m-stack__item--middle m-brand__tools">
                                <!-- BEGIN: Responsive Aside Left Menu Toggler -->
                                <a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
                                    <span></span>
                                </a>
                                <!-- END -->
                                
                                <!-- BEGIN: Topbar Toggler -->
                                <a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
                                    <i class="flaticon-more"></i>
                                </a>
                                <!-- BEGIN: Topbar Toggler -->
                            </div>
                        </div>
                    </div>
                    <!-- END: Brand -->         
                    <div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
                        <!-- BEGIN: Topbar -->
                        <div id="m_header_topbar" class="m-topbar m-stack m-stack--ver m-stack--general">
                            <div class="m-stack__item m-topbar__nav-wrapper">
                                <ul class="m-topbar__nav m-nav m-nav--inline">
                                    <li class="m-nav__item m-dropdown m-dropdown--large m-dropdown--arrow m-dropdown--align-center m-dropdown--mobile-full-width m-dropdown--skin-light m-list-search m-list-search--skin-light" 
                                    m-dropdown-toggle="click" id="m_quicksearch" m-quicksearch-mode="dropdown" m-dropdown-persistent="1">
                                    
                                    <a href="#" class="m-nav__link m-dropdown__toggle">
                                        <span class="m-nav__link-icon"><span class="m-nav__link-icon-wrapper"><i class="flaticon-search-1"></i></span></span>
                                    </a>
                                    <div class="m-dropdown__wrapper">                   
                                        <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                                        <div class="m-dropdown__inner ">
                                            <div class="m-dropdown__header">
                                                <form  class="m-list-search__form">
                                                    <div class="m-list-search__form-wrapper">
                                                        <span class="m-list-search__form-input-wrapper">
                                                            <input id="m_quicksearch_input" autocomplete="off" type="text" name="q" class="m-list-search__form-input" value="" placeholder="Search...">
                                                        </span>
                                                        <span class="m-list-search__form-icon-close" id="m_quicksearch_close">
                                                            <i class="la la-remove"></i>
                                                        </span>
                                                    </div>
                                                </form>  
                                            </div>
                                            <div class="m-dropdown__body">
                                                <div class="m-dropdown__scrollable m-scrollable" data-scrollable="true" data-height="300" data-mobile-height="200">
                                                    <div class="m-dropdown__content">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>           
                                <li class="m-nav__item m-topbar__notifications m-dropdown m-dropdown--large m-dropdown--arrow m-dropdown--align-center  m-dropdown--mobile-full-width" m-dropdown-toggle="click" m-dropdown-persistent="1">
                                    <a href="#" class="m-nav__link m-dropdown__toggle" id="m_topbar_notification_icon">
                                        <span class="m-nav__link-icon">
                                            <span class="m-nav__link-icon-wrapper"><i class="flaticon-music-2"></i></span>
                                            <?= count($notifUnread) != 0 ? '<span class="m-nav__link-badge m-badge m-badge--danger" id="badge-notif" style="margin-bottom:20px;">'.count($notifUnread).'</span>' : ''; ?>
                                        </span>
                                    </a>
                                    <div class="m-dropdown__wrapper">
                                        <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                                        <div class="m-dropdown__inner">
                                            <div class="m-dropdown__body">              
                                                <div class="m-dropdown__content">
                                                    <ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand" role="tablist">
                                                        <li class="nav-item m-tabs__item">
                                                            <a class="nav-link m-tabs__link active" data-toggle="tab" href="#topbar_notifications_notifications" role="tab">
                                                                Notifikasi
                                                            </a>
                                                        </li>
                                                    </ul>
                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="topbar_notifications_notifications" role="tabpanel">
                                                            <div class="m-scrollable" data-scrollable="true" data-height="250" data-mobile-height="200">
                                                                <div class="m-list-timeline m-list-timeline--skin-light">
                                                                    <?php if (count($allNotif) > 0) { ?>
                                                                    <div class="m-list-timeline__items">
                                                                        <?php foreach ($allNotif as $value) { ?>
                                                                        <?php $spg = Spg::findOne($value['spg_id']); ?>
                                                                        <div class="m-list-timeline__item">
                                                                            <span class="m-list-timeline__badge"></span>
                                                                            <span class="m-list-timeline__text">SPG <b><?= $spg['nama_spg']; ?></b> tidak sesuai dengan lokasi tujuan. <a href="<?= Url::to(['site/view','id'=>$value['id']]); ?>" class="m-link">Buka link</a></span>
                                                                            <span class="m-list-timeline__time"><?= MyFormatter::setTimeElapse($value['created_at']); ?></span>
                                                                        </div>
                                                                        <?php } ?>
                                                                    </div>
                                                                    <?php } else { ?>
                                                                        <span>Tidak ada notifikasi baru</span>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                            <!-- <span class="text-center"><button class="btn btn-info" onclick="markAsRead()">Tandai sudah dibaca</button></span> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="m-nav__item m-topbar__user-profile  m-dropdown m-dropdown--medium m-dropdown--arrow  m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" m-dropdown-toggle="click">
                                    <a href="#" class="m-nav__link m-dropdown__toggle">
                                        <!-- <span class="m-topbar__userpic">
                                            <img src="<?//= $theme; ?>/app/media/img/users/user4.jpg" class="m--img-rounded m--marginless m--img-centered" alt=""/>
                                        </span> -->
                                        <span class="m-nav__link-icon m-topbar__usericon">
                                            <span class="m-nav__link-icon-wrapper"><i class="flaticon-user-ok"></i></span>
                                        </span>
                                        <span class="m-topbar__username" style="color: #000 !important;"><?= Yii::$app->user->identity == null ? "" : Yii::$app->user->identity->name; ?></span>                    
                                    </a>
                                    <div class="m-dropdown__wrapper">
                                        <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                        <div class="m-dropdown__inner">
                                            <div class="m-dropdown__header m--align-center">
                                                <div class="m-card-user m-card-user--skin-light">
                                                    <!-- <div class="m-card-user__pic">
                                                        <img src="<?//= $theme; ?>/app/media/img/users/user4.jpg" class="m--img-rounded m--marginless" alt=""/>
                                                    </div> -->
                                                    <div class="m-card-user__details">
                                                        <span class="m-card-user__name m--font-weight-500"><?= Yii::$app->user->identity == null ? 'BELUM LOGIN' : Yii::$app->user->identity->username; ?></span>
                                                        <a href="" class="m-card-user__email m--font-weight-300 m-link"><?= Yii::$app->user->identity == null ? 'BELUM LOGIN' : Yii::$app->user->identity->name; ?></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="m-dropdown__body">
                                                <div class="m-dropdown__content">
                                                    <ul class="m-nav m-nav--skin-light">
                                                        <li class="m-nav__section m--hide">
                                                            <span class="m-nav__section-text">Section</span>
                                                        </li>
                                                        <!-- <li class="m-nav__item">
                                                            <a href="profile.html" class="m-nav__link">
                                                                <i class="m-nav__link-icon flaticon-profile-1"></i>
                                                                <span class="m-nav__link-title">  
                                                                    <span class="m-nav__link-wrap">      
                                                                        <span class="m-nav__link-text">My Profile</span>      
                                                                    </span>
                                                                </span>
                                                            </a>
                                                        </li> -->
                                                        <li class="m-nav__separator m-nav__separator--fit">
                                                        </li>
                                                        <li class="m-nav__item">
                                                            <?= Html::a(
                                                                'Logout',
                                                                ['/site/logout'],
                                                                ['data-method' => 'post', 'class' => 'btn m-btn--pill btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder']
                                                            ) ?>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- END: Topbar -->
                </div>
                </div>
            </div>
        </header>
        <!-- END: Header -->

        <!-- begin::Body -->
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
            
            <!-- BEGIN: Left Aside -->
            <button class="m-aside-left-close  m-aside-left-close--skin-light " id="m_aside_left_close_btn"><i class="la la-close"></i></button>
            
            <div id="m_aside_left" class="m-grid__item  m-aside-left  m-aside-left--skin-light ">   
                <!-- BEGIN: Aside Menu -->
                <div 
                id="m_ver_menu" 
                class="m-aside-menu  m-aside-menu--skin-light m-aside-menu--submenu-skin-light " 
                m-menu-vertical="1"
                m-menu-scrollable="0" m-menu-dropdown-timeout="500"  
                >       
                <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
                    <li class="m-menu__section m-menu__section--first">
                        <h4 class="m-menu__section-text">Administrator</h4>
                        <i class="m-menu__section-icon flaticon-more-v3"></i>
                    </li>
                    <li class="m-menu__item <?= Yii::$app->controller->id == 'site' ? 'm-menu__item--active' : ''; ?>" aria-haspopup="true" m-menu-link-redirect="1">
                        <a href="<?= Url::to(['/site/index']); ?>" class="m-menu__link ">
                            <i class="m-menu__link-icon fa fa-home"></i>
                            <span class="m-menu__link-text">Dashboard</span>
                        </a>
                    </li>
                    <li class="m-menu__item <?= Yii::$app->controller->id == 'journey' ? 'm-menu__item--active' : ''; ?>" aria-haspopup="true" m-menu-link-redirect="1">
                        <a href="<?= Url::to(['/journey/index']); ?>" class="m-menu__link ">
                            <i class="m-menu__link-icon fa fa-map-marker-alt"></i>
                            <span class="m-menu__link-text">Data Journey SPG</span>
                        </a>
                    </li>
                    <li class="m-menu__item <?= Yii::$app->controller->id == 'survey' ? 'm-menu__item--active' : ''; ?>" aria-haspopup="true" m-menu-link-redirect="1">
                        <a href="<?= Url::to(['/survey/index']); ?>" class="m-menu__link ">
                            <i class="m-menu__link-icon fa fa-clipboard"></i>
                            <span class="m-menu__link-text">Data Survey SPG</span>
                        </a>
                    </li>
                    <li class="m-menu__item <?= Yii::$app->controller->id == 'tracking' ? 'm-menu__item--active' : ''; ?>" aria-haspopup="true" m-menu-link-redirect="1">
                        <a href="<?= Url::to(['/tracking/index']); ?>" class="m-menu__link ">
                            <i class="m-menu__link-icon fa fa-map-signs"></i>
                            <span class="m-menu__link-text">Data Tracking SPG</span>
                        </a>
                    </li>
                    <li class="m-menu__item  m-menu__item--submenu <?= Yii::$app->controller->id == 'spg' || Yii::$app->controller->id == 'team' ? 'm-menu__item--active m-menu__item--open m-menu__item--expanded' : ''; ?>" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="javascript:;" class="m-menu__link m-menu__toggle">
                            <i class="m-menu__link-icon fa fa-server"></i>
                            <span class="m-menu__link-text">Data Master</span>
                            <i class="m-menu__ver-arrow la la-angle-right"></i>
                        </a>
                        <div class="m-menu__submenu ">
                            <span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">
                                <li class="m-menu__item m-menu__item--parent" aria-haspopup="true" >
                                    <span class="m-menu__link">
                                        <span class="m-menu__link-text">Data Master</span>
                                    </span>
                                </li>
                                <?php if (Yii::$app->user->identity->role->role == 'super-admin') { ?>
                                <li class="m-menu__item <?= (Yii::$app->controller->id == 'team') ? 'm-menu__item--active' : ''; ?>" aria-haspopup="true" >
                                    <a href="<?= Url::to(['/team']); ?>" class="m-menu__link ">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                        <span class="m-menu__link-text">Tim</span>
                                    </a>
                                </li>
                                <?php } ?>
                                <li class="m-menu__item <?= (Yii::$app->controller->id == 'spg') ? 'm-menu__item--active' : ''; ?>" aria-haspopup="true"  m-menu-link-redirect="1">
                                    <a href="<?= Url::to(['/spg']); ?>" class="m-menu__link ">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                        <span class="m-menu__link-text">SPG</span>
                                    </a>
                                </li>
                                <!-- <li class="m-menu__item " aria-haspopup="true"  m-menu-link-redirect="1">
                                    <a href="<?//= Url::to(['#']); ?>" class="m-menu__link ">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                        <span class="m-menu__link-text">Cetak Termin</span>
                                    </a>
                                </li> -->
                            </ul>
                        </div>
                    </li>
                    <!-- <li class="m-menu__item <?= Yii::$app->controller->id == 'laporan' ? 'm-menu__item--active' : ''; ?>" aria-haspopup="true"  m-menu-link-redirect="1">
                        <a href="<?= Url::to(['/laporan/index']); ?>" class="m-menu__link ">
                            <i class="m-menu__link-icon fa fa-print"></i>
                            <span class="m-menu__link-text">Laporan</span>
                        </a>
                    </li> -->
                </ul>
                </div>
                <!-- END: Aside Menu -->
            </div>
            <!-- END: Left Aside -->                            
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                
                <!-- BEGIN: Subheader -->
                <div class="m-subheader ">
                    <div class="d-flex align-items-center">
                        <div class="mr-auto">
                            <h3 class="m-subheader__title m-subheader__title--separator">EVO</h3>            
                            <?= Breadcrumbs::widget([
                                'homeLink' => [
                                    'label' => '<i class="m-nav__link-icon la la-home"></i>',
                                    'url' => Yii::$app->getHomeUrl() . 'site/index',
                                    'template' => '<li class="m-nav__item m-nav__item--home">{link}</li><li class="m-nav__separator">-</li>',
                                    'class'=>'m-nav__link m-nav__link--icon',
                                    'encode' => false,
                                ],
                                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                                'activeItemTemplate' => '<li class="m-nav__item">{link}</li>',
                                'itemTemplate' => '<li class="m-nav__item">{link}</li><li class="m-nav__separator">-</li>', // template for all links
                                'options' => ['class'=>'m-subheader__breadcrumbs m-nav m-nav--inline'],
                            ]); ?>
                        </div>
                    </div>
                </div>
                <!-- END: Subheader -->

                    <div class="m-content">
                        <?= $content; ?>
                    </div>
                </div>
            </div>
            <!-- end:: Body -->
            
            <!-- begin::Footer -->
            <footer class="m-grid__item m-footer ">
                <div class="m-container m-container--fluid m-container--full-height m-page__container">
                    <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
                        <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
                            <span class="m-footer__copyright">
                                <?= date('Y'); ?> &copy; evo.com
                            </span>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- end::Footer --> 
        </div>
        <!-- end:: Page -->

        <!-- begin::Scroll Top -->
        <div id="m_scroll_top" class="m-scroll-top">
            <i class="la la-arrow-up"></i>
        </div>
        <!-- end::Scroll Top -->
        
        <!--begin::Base Scripts -->
        <script src="<?= $theme; ?>/vendors/base/vendors.bundle.js" type="text/javascript"></script>
        <script src="<?= $theme; ?>/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
        <!--end::Base Scripts -->
        
        <!--begin::Page Vendors -->
        <script src="<?= $theme; ?>/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
        <script src="<?= $theme; ?>/demo/default/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
        <script src="<?= $theme; ?>/demo/default/custom/crud/forms/widgets/bootstrap-daterangepicker.js" type="text/javascript"></script>
        <!--end::Page Vendors -->

        <script>
        $(document).ready(function() {
            ko.applyBindings(model);
            $(".datepicker").datepicker({
                format: "yyyy-mm-dd",
                todayHighlight: true,
                autoclose: true,
            });
            $(".select2").select2();

            var save_value_url = "<?= Url::to(['perusahaan/save-detail']); ?>";
            const deadline_input = $('.datepicker-only');
            deadline_input.change(function(e){
                var id = $(this).data('id');
                var value = $(this).val();
                var column = 'tanggal_deadline';
                $.ajax({
                    url: save_value_url,
                    data: 'id='+id+'&value='+value+'&column='+column,
                    type: 'get',
                    beforeSend: function(){},
                    success: function(data){
                        console.log(data);
                        toastMessage("Data berhasil disimpan.", "Selamat!", 1000);
                    }
                });
            });

            getData();
            /* Start the inital request */
            // setTimeout('getNotifications()', 1100);
            // setInterval('getNotifications()', 200);

            $('.iuran').on('change', function(e) {
                const value = this.value.replace(/,/g, '');
                this.value = parseFloat(value).toLocaleString('id-ID', {
                    style: 'decimal',
                });
            });
        });

        function getNotifications(){
            var url = '<?= Url::to(['site/checkdata']); ?>';
            $.ajax({
                type: "GET",
                url: url,
                async: true, /* If set to non-async, browser shows page as "Loading.."*/
                cache: false,
                timeout: 50000, /* Timeout in ms */
                success: function(data) {
                    var details = data.details;
                    // console.log("data :: ", details);
                    if (data.message == "success") {
                        $.each(details, function (key, val) {
                            // console.log("val :: ", val);
                            var msg = "SPG <b>" + val.nama_spg + "</b> tidak sesuai dengan lokasi tujuan. Sedang berada di <b>" + val.data.latitude + "," + val.data.longitude + "</b>";
                            toastMessage(msg, "Pemberitahuan", 0, 'error');
                        });
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "timeOut": "100",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };
                    toastr.danger("Error! " + errorThrown);
                    setTimeout(
                        getNotifications, /* Try again after.. */
                    15000); /* milliseconds (15seconds) */
                }
            });
        };

        function toastMessage(message, title, timeout, type='success') {
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "timeOut": timeout,
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
            if (type == 'success') {
                toastr.success(message, title);
            } else if (type == 'warning') {
                toastr.warning(message, title);
            } else if (type == 'info') {
                toastr.info(message, title);
            } else {
                toastr.error(message, title);
            }
        }

        function formatDate(date) {
            var monthNames = [
                "Januari", "Februari", "Maret",
                "April", "Mei", "Juni", "Juli",
                "Agustus", "September", "Oktober",
                "November", "Desember"
            ];
            var dates = new Date(date);

            var day = dates.getDate();
            var monthIndex = dates.getMonth();
            var year = dates.getFullYear();

            return day + ' ' + monthNames[monthIndex] + ' ' + year;
        }

        function getData() {
            var ctrl = '<?= Yii::$app->controller->id; ?>';
            var url = '<?= Url::to([Yii::$app->controller->id.'/'.'getdata']); ?>';
            $('#tablegrid').DataTable({
                "scrollX": true,
                "ajax": {
                    "url": url,
                    "data": function(d){
                        return JSON.stringify(d.data);
                    },
                },
                "lengthMenu": [
                    [5,10, 25, -1],
                    [5,10, 25, "All"]
                ],
                "pageLength": 10,
                "columnDefs": [{
                    'orderable': false,
                    'targets': [0]
                }, {
                    "searchable": true,
                    "targets": [0]
                }],
                "order": [
                    [0, "asc"]
                ],
                "processing": true,
            });
        }

        function markAsRead() {
            var url = "<?= Url::to(['perusahaan/save-detail']); ?>";
            $.ajax({
                url: url,
                data: 'id='+id+'&value='+value+'&column='+column,
                type: 'post',
                beforeSend: function(){},
                success: function(data){
                    console.log(data);
                    toastMessage("Data berhasil disimpan.", "Selamat!", 1000);
                }
            });
        }

        $('#m_topbar_notification_icon').on('click', function () {
            var url = "<?= Url::to(['site/update-notification']); ?>";
            $.ajax({
                url: url,
                data: {},
                type: 'post',
                beforeSend: function(){},
                success: function(data){
                    console.log(data);
                    $('#badge-notif').hide();
                }
            });
        });

        $('#filterperiod').on('apply.daterangepicker', function(ev, picker) {            
            getSurvey();
        });

        $('#filterperioddashboard').on('apply.daterangepicker', function(ev, picker) {            
            getDashboard();
        });
        </script>

        <?php $this->endBody() ?>
    </body>
    <!-- end::Body -->
</html>
<?php $this->endPage() ?>