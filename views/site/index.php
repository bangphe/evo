<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Spg;
use app\models\Team;
use app\components\MyFormatter;
use app\models\SpgLocation;

$this->title = 'Dashboard';
?>

<div class="site-index">
    <div class="row">
        <div class="col-md-7">
            <div class="m-portlet m-portlet--full-height ">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Data Lokasi SPG
                            </h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="tab-content">
                        <div class="table-responsive">
                            <table id="tablegrid" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <td class="m-widget11__label">No.</td>
                                        <td class="m-widget11__label">Nama SPG</td>
                                        <td class="m-widget11__label">Tanggal</td>
                                        <td class="m-widget11__label">Kecamatan</td>
                                        <td class="m-widget11__label">Latitude</td>
                                        <td class="m-widget11__label">Longitude</td>
                                        <td class="m-widget11__label">Aksi</td>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="m-portlet m-portlet--full-height m-portlet--fit ">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Latest Updates
                            </h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <div class="m-form m-form--label-align-right">
                            <div class="row align-items-center">
                                <div class="col-xl-12 order-2 order-xl-1">
                                    <div class="form-group m-form__group row align-items-center">
                                        <div class="col-md-12">
                                            <div class="m-form__group m-form__group--inline" style="margin-left: 20px;">
                                                <div class="m-form__label">
                                                    <label>Date:</label>
                                                </div>
                                                <div class="m-form__control">
                                                    <input type="text" class="form-control" style="width: 180px;" id="filterperioddashboard" autocomplete="off" data-bind="event: {change: home.changefilter}">
                                                </div>
                                            </div>
                                            <div class="d-md-none m--margin-bottom-10"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="m-widget4 m-widget4--chart-bottom" style="min-height: 350px">
                        <div class="m-widget4__item">
                            <div class="m-widget4__ext">                             
                                <a href="#" class="m-widget4__icon m--font-brand">
                                    <i class="flaticon-interface-3"></i>
                                </a>
                            </div>
                            <div class="m-widget4__info">
                                <span class="m-widget4__text">
                                Total Survey
                                </span>                                      
                            </div>
                            <div class="m-widget4__ext">
                                <span id="countSurvey" class="m-widget4__number m--font-accent"><?= $countSurvey; ?></span>
                            </div>
                        </div>
                        <div class="m-widget4__item">
                            <div class="m-widget4__ext">                             
                                <a href="#" class="m-widget4__icon m--font-brand">
                                <i class="flaticon-folder-4"></i>
                                </a>
                            </div>
                            <div class="m-widget4__info">
                                <span class="m-widget4__text">
                                Total Stock Terjual
                                </span>                                      
                            </div>
                            <div class="m-widget4__ext">
                                <span class="m-widget4__stats m--font-info">
                                    <span id="countStock" class="m-widget4__number m--font-accent"><?= $countStock; ?></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <!--begin:: Packages-->
            <div class="m-portlet m--bg-info m-portlet--bordered-semi m-portlet--full-height ">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text m--font-light">
                                Stock
                            </h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">
                            
                        </ul>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <!--begin::Widget 29-->
                    <div class="m-widget29">
                    <div class="m-widget_content" style="<?= SpgLocation::showDashboard($role, 'KOTA SURABAYA'); ?>">
                            <h3 class="m-widget_content-title">Data Penjualan Per Hari - SURABAYA <?= MyFormatter::formatBlnThn(date('Y-m-d')); ?></h3>
                            <div class="m-widget_content-items">
                                <div class="m-widget_content-item">
                                    <span>CC</span>
                                    <span class="m--font-accent"><?= $countCCSub; ?></span>
                                </div>
                                <div class="m-widget_content-item">
                                    <span>ECC</span>
                                    <span class="m--font-brand"><?= $countECSub; ?></span>
                                </div>
                                <div class="m-widget_content-item">
                                    <span>Vol</span>
                                    <span><?= $countVolSub; ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="m-widget_content" style="<?= SpgLocation::showDashboard($role, 'KABUPATEN JOMBANG'); ?>">
                            <h3 class="m-widget_content-title">Data Penjualan Per Hari - JOMBANG <?= MyFormatter::formatBlnThn(date('Y-m-d')); ?></h3>
                            <div class="m-widget_content-items">
                                <div class="m-widget_content-item">
                                    <span>CC</span>
                                    <span class="m--font-accent"><?= $countCCJbg; ?></span>
                                </div>
                                <div class="m-widget_content-item">
                                    <span>ECC</span>
                                    <span class="m--font-brand"><?= $countECJbg; ?></span>
                                </div>
                                <div class="m-widget_content-item">
                                    <span>Vol</span>
                                    <span><?= $countVolJbg; ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="m-widget_content" style="<?= SpgLocation::showDashboard($role, 'KABUPATEN JEMBER'); ?>">
                            <h3 class="m-widget_content-title">Data Penjualan Per Hari - JEMBER <?= MyFormatter::formatBlnThn(date('Y-m-d')); ?></h3>
                            <div class="m-widget_content-items">
                                <div class="m-widget_content-item">
                                    <span>CC</span>
                                    <span class="m--font-accent"><?= $countCCJbr; ?></span>
                                </div>
                                <div class="m-widget_content-item">
                                    <span>ECC</span>
                                    <span class="m--font-brand"><?= $countECJbr; ?></span>
                                </div>
                                <div class="m-widget_content-item">
                                    <span>Vol</span>
                                    <span><?= $countVolJbr; ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end::Widget 29-->
                </div>
            </div>
            <!--end:: Packages-->
        </div>
    </div>
</div>

<script>
var home = {};
home.changefilter = function() {
    getDashboard();
}

function InitDateRangePicker(selector) {
    $('#'+selector).daterangepicker({
        startDate: moment(),
        endDate: moment(),
        locale: {
            format: 'DD MMM YYYY'
        },
    });
}

function getDashboard() {
    var ctrl = '<?= Yii::$app->controller->id; ?>';
    var url = '<?= Url::to([Yii::$app->controller->id.'/'.'getdashboard']); ?>';
    var start = moment($('#filterperioddashboard').data('daterangepicker').startDate).format("YYYY-MM-DD");
    var end = moment($('#filterperioddashboard').data('daterangepicker').endDate).format("YYYY-MM-DD");

    $.ajax({
        url: url,
        data: 'start='+start+'&end='+end,
        type: 'get',
        beforeSend: function(){},
        success: function(data){
            console.log(data);
            var spanSurvey = document.getElementById("countSurvey");
            spanSurvey.textContent = data.countSurvey;

            var spanStock = document.getElementById("countStock");
            spanStock.textContent = data.countStock;
        }
    });
}

$(document).ready(function() {
    InitDateRangePicker('filterperioddashboard');
    var currentDate = new Date();
    var start = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate());
    var end = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate());
    $("#filterperioddashboard").data('daterangepicker').setStartDate(start);
    $("#filterperioddashboard").data('daterangepicker').setEndDate(end);
});
</script>