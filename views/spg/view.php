<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Spg */

$this->title = 'Detil SPG | ' . $model->nama_spg;
$this->params['breadcrumbs'][] = ['label' => 'Data SPG', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="spg-view">
    <div class="m-portlet m-portlet--mobile">   
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        <?= $this->title; ?>
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <?= Html::a('<span><i class="fa fa-pencil-alt"></i><span>Edit Data</span></span>', ['update', 'id' => $model->id], ['class' => 'btn btn-info m-btn m-btn--custom m-btn--icon m-btn--air']) ?>
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet__body">
            <?= Yii::$app->session->getFlash('info'); ?>
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'nama_spg',
                    [
                        'label'  => 'Tim',
                        'value'  => function ($data) {
                            return $data->team->nama_team;
                        },
                    ],
                    'username',
                    // 'password',
                    // 'kecamatan',
                    // 'location',
                    // 'longitude',
                    // 'latitude',
                    'status',
                    'created_at',
                ],
            ]) ?>
        </div>
    </div>
</div>
