<?php

use app\models\Kecamatan;
use app\models\Team;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Spg */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="spg-form">
    <?php $form = ActiveForm::begin([
        'id' => 'spg-form',
        'options' => [
            'class' => 'm-form m-form--fit m-form--label-align-right'
        ]
    ]); ?>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'team_id')->dropDownList(Team::getDataTeam(), ['class'=>'form-control m-input m-input--square', 'onchange'=>'getKecamatan(this.value)']) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'nama_spg')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'password')->passwordInput(['maxlength' => true, 'value' => '']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'status')->dropDownList($model->getDataStatus(), ['prompt'=>'-- Pilih Status --', 'class'=>'form-control m-input m-input--square select2']) ?>
        </div>
    </div>

    <div class="m-form__actions m-form__actions">
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <?= Html::submitButton('<i class="fa fa-check"></i> Save', ['class' => 'btn btn-success']) ?>
                <button type="button" class="btn btn-secondary" onclick="goBack()"><i class="fa fa-times"></i> Cancel</button>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>

<script>
    var model = {};
    model.data = ko.observableArray([]);
    
    var url = '<?= Url::to(['/journey/getkecamatan']); ?>';
    function getKecamatan(id) {        
        $.ajax({
            url: url,
            type: "GET",
            cache: false,
            data: { "team": id },
            success: function (res) {
                model.data(res.data);
                
                var data = $.map(model.data(), function(obj) {
                    return { id: obj.name, text: obj.name };
                });
                // console.log("dt :: ", data);
                $("#spg-kecamatan").empty();
                $("#spg-kecamatan").select2({data: data});
            },
            error: function (data) {
                console.log(data);
            }
        });
    }
    
    $(document).ready(function () {
        getKecamatan($('#spg-team_id').val());
    });
</script>