<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Spg */

$this->title = 'Tambah SPG';
$this->params['breadcrumbs'][] = ['label' => 'Data SPG', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="spg-create">
    <div class="m-portlet m-portlet--tab">  
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="fa fa-users"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        <?= $this->title; ?>
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>
