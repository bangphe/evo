<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use app\components\MyFormatter;

$this->title = 'Tracking SPG';
?>

<div class="site-index">
    <div class="row">
        <div class="col-md-12">
            <div class="m-portlet m-portlet--full-height ">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Data Lokasi SPG
                            </h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="tab-content">
                        <div class="table-responsive">
                            <table id="tablegrid" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <td class="m-widget11__label">No.</td>
                                        <td class="m-widget11__label">Nama SPG</td>
                                        <td class="m-widget11__label">Tanggal</td>
                                        <td class="m-widget11__label">Kecamatan</td>
                                        <td class="m-widget11__label">Latitude</td>
                                        <td class="m-widget11__label">Longitude</td>
                                        <td class="m-widget11__label">Aksi</td>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>