<?php

use app\components\MyFormatter;

/* @var $this yii\web\View */
/* @var $model app\models\Spg */

$this->title = 'Detil Lokasi SPG | ' . $model->spg->nama_spg;
$this->params['breadcrumbs'][] = ['label' => 'Dashboard', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

$exp_kecamatan = explode(',', $model->journey->kecamatan);
$journey_kecamatan = $exp_kecamatan[0];
?>
<div class="row">
    <div class="col-md-12">
        <div class="m-portlet m-portlet--danger m-portlet--head-solid-bg m-portlet--bordered ">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                            <i class="flaticon-placeholder-2"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                            Data Lokasi SPG
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    
                </div>
            </div>
            <div class="m-portlet__body">
                <!--begin::Widget 29-->
                <div class="m-widget29">
                    <div class="m-widget_content">
                        <h3 class="m-widget_content-title">Data Journey | <?= $model->spg->nama_spg.' - '.$model->spg->team->nama_team; ?></h3>
                        <div class="m-widget_content-items">
                            <div class="m-widget_content-item">
                                <span>Week</span>
                                <span><?= $model->journey->week; ?></span>
                            </div>
                            <div class="m-widget_content-item">
                                <span>Journey Date</span>
                                <span><?= MyFormatter::formatBlnThn($model->journey->journey_date); ?></span>
                            </div>
                            <div class="m-widget_content-item">
                                <span>Tujuan (Kecamatan)</span>
                                <span><?= $model->kecamatan_survey; ?></span>
                            </div>
                            <div class="m-widget_content-item">
                                <span>Lokasi SPG (Kecamatan)</span>
                                <span class="m--font-danger"><?= $model->longitude.','.$model->latitude; ?></span>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="m-widget_content">
                        <h3 class="m-widget_content-title">Taxes info</h3>
                        <div class="m-widget_content-items">
                            <div class="m-widget_content-item">
                                <span>Total</span>
                                <span class="m--font-accent">22.50</span>
                            </div>
                            <div class="m-widget_content-item">
                                <span>Change</span>
                                <span class="m--font-brand">+15%</span>
                            </div>
                            <div class="m-widget_content-item">
                                <span>Count</span>
                                <span>701</span>
                            </div>
                        </div>
                    </div>
                    <div class="m-widget_content">
                        <h3 class="m-widget_content-title">Partners Sale</h3>
                        <div class="m-widget_content-items">
                            <div class="m-widget_content-item">
                                <span>Total</span>
                                <span class="m--font-accent">$680</span>
                            </div>
                            <div class="m-widget_content-item">
                                <span>Change</span>
                                <span class="m--font-brand">+15%</span>
                            </div>
                            <div class="m-widget_content-item">
                                <span>Licenses</span>
                                <span>29</span>
                            </div>
                        </div>
                    </div> -->

                    <div class="m-widget_content">
                        <div id="map" style="height: 400px;"></div>
                    </div>
                </div>
                <!--end::Widget 29-->
            </div>
        </div>
    </div>
</div>

<script>
    var ori = '<?= $model->kecamatan_survey.', '.$model->spg->team->area; ?>';
    var dest = '<?= $model->latitude.', '.$model->longitude; ?>';

    mapboxgl.accessToken = 'pk.eyJ1IjoiYmFuZ3BoZSIsImEiOiJja2pjajM5ZDQyOHk2MnFwOW1nampicW0wIn0.5Mft7-uJSOcaaeUuTRGvRw';
    var map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v10',
        center: [-112.73338560851812, -7.304973859703068], // starting position
        zoom: 9
    });
    
    map.on('load', function() {
        var directions = new MapboxDirections({
            accessToken: mapboxgl.accessToken,
            // profile: 'mapbox/cycling',
            controls: {
                // inputs: false,
                insructions: false,
                profileSwitcher: false,
            }
        });
        map.addControl(directions, 'top-left');

        directions.setOrigin(ori);
        directions.setDestination(dest);
    });
</script>