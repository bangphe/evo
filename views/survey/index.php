<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data Survey SPG';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="spg-survey-index">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        <?= $this->title; ?>
                    </h3>
                </div>
                <div class="m-form m-form--label-align-right">
                    <div class="row align-items-center">
                        <div class="col-xl-12 order-2 order-xl-1">
                            <div class="form-group m-form__group row align-items-center">
                                <div class="col-md-12">
                                    <div class="m-form__group m-form__group--inline" style="margin-left: 20px;">
                                        <div class="m-form__label">
                                            <label>Date:</label>
                                        </div>
                                        <div class="m-form__control">
                                            <input type="text" class="form-control" style="width: 180px;" id="filterperiod" autocomplete="off" data-bind="event: {change: home.changefilter}">
                                        </div>
                                    </div>
                                    <div class="d-md-none m--margin-bottom-10"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <?= Html::a('<span><i class="fa fa-download"></i><span>Export Data</span></span>', ['exportcsv'], [
                            'id' => 'btn-export',
                            'class' => 'btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air',
                            'data-start' => $start,
                            'data-end' => $end,
                        ]) ?>
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet__body">
            <?= Yii::$app->session->getFlash('info'); ?>
            
            <!--begin: Datatable -->
            <table id="tablegrid" class="table table-striped- table-bordered table-hover table-checkable" width="100%">
                <thead>
                    <tr>
                        <th width="10px">No</th>
                        <th width="90px">Nama SPG</th>
                        <th width="40px">Week</th>
                        <th width="40px">Kecamatan</th>
                        <th width="40px">Product</th>
                        <th width="40px">Nama Pelanggan</th>
                        <th width="40px">Lokasi</th>
                        <th width="40px">Jumlah Beli</th>
                        <th width="40px">Umur</th>
                        <th width="40px">Pekerjaan</th>
                        <th width="40px">No. Telpon</th>
                        <th width="40px">Tgl Input</th>
                        <th width="40px">Rokok yg Dipakai</th>
                        <th width="40px">Tempat Beli</th>
                        <th width="40px">Brand Awareness</th>
                        <th width="40px">Komentar Harga</th>
                        <th width="40px">Komentar Rasa</th>
                        <th width="40px">Komentar Kemasan</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<script>
var home = {};
home.changefilter = function() {
    getSurvey();
}

function InitDateRangePicker(selector) {
    $('#'+selector).daterangepicker({
        startDate: moment(),
        endDate: moment(),
        locale: {
            format: 'DD MMM YYYY'
        },
    });
}

function getSurvey() {
    var ctrl = '<?= Yii::$app->controller->id; ?>';
    var url = '<?= Url::to([Yii::$app->controller->id.'/'.'getdata']); ?>';
    var start = moment($('#filterperiod').data('daterangepicker').startDate).format("YYYY-MM-DD");
    var end = moment($('#filterperiod').data('daterangepicker').endDate).format("YYYY-MM-DD");

    var btn = document.getElementById("btn-export");
    var href = btn.getAttribute("href");
    btn.setAttribute("data-start", start);
    btn.setAttribute("data-end", end);
    btn.setAttribute("href", href + "?start="+start+"&end="+end);

    // setTimeout(function(){ $('#tablegrid').DataTable().ajax.url(url+"?start="+start+"&end="+end).load(); }, 3000);
    $('#tablegrid').DataTable().ajax.url(url+"?start="+start+"&end="+end).load();
}

$(document).ready(function() {
    InitDateRangePicker('filterperiod');
    var currentDate = new Date();
    var start = new Date(currentDate.getFullYear(), currentDate.getMonth(), 1);
    var end = new Date(currentDate.getFullYear(), currentDate.getMonth() + 1, 0);
    $("#filterperiod").data('daterangepicker').setStartDate(start);
    $("#filterperiod").data('daterangepicker').setEndDate(end);
});
</script>