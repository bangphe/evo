<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SpgSurvey */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Spg Surveys', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="spg-survey-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'spg_id',
            'week',
            'area',
            'product',
            'kecamatan',
            'lokasi',
            'tgl_input',
            'nama_pelanggan',
            'umur',
            'jenis_kelamin',
            'alamat:ntext',
            'pekerjaan',
            'no_telpon',
            'jumlah_beli',
            'rokok_yang_dipakai',
            'tempat_beli_rokok',
            'tahu_rokok_evo',
            'komentar_harga',
            'komentar_rasa',
            'komentar_kemasan',
            'rayon',
            'created_at',
        ],
    ]) ?>

</div>
