<?php

use app\models\Kabupaten;
use app\models\Kecamatan;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Team */
/* @var $form yii\widgets\ActiveForm */

$js = '
// $(document.body).on("change", "#team-area", function(){
//     alert(this.value);
// });

// $("#team-area").on("select2:selecting", function(e) {
//     var id = e.params.args.data.id;
//     var kecArray = _.filter(model.data(), function(o) { 
//         return o.kabupaten_id == id; 
//     });
    
//     var data = $.map(kecArray, function(obj) {
//         return { id: obj.id, text: obj.name };
//     });
//     $("#team-kecamatan").empty();
//     $("#team-kecamatan").select2({data: data});
// });
';

$this->registerJs($js);
?>

<div class="team-form">
    <?php $form = ActiveForm::begin([
        'id' => 'team-form',
        'options' => [
            'class' => 'm-form m-form--fit m-form--label-align-right'
        ]
    ]); ?>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'nama_team')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'area')->dropDownList(Kabupaten::getDataKabupaten(), ['class'=>'form-control m-input m-input--square select2']) ?>
        </div>
    </div>
    
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'status')->dropDownList($model->getDataStatus(), ['prompt'=>'-- Pilih Status --', 'class'=>'form-control m-input m-input--square select2']) ?>
        </div>
    </div>

    <div class="m-form__actions m-form__actions">
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <?= Html::submitButton('<i class="fa fa-check"></i> Save', ['class' => 'btn btn-success']) ?>
                <button type="button" class="btn btn-secondary" onclick="goBack()"><i class="fa fa-times"></i> Cancel</button>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>

<script>
    var model = {};
    model.data = ko.observableArray([]);
    
    var url = '<?= Url::to(['/journey/getkecamatan']); ?>';        
    function getKecamatan() {
		$.ajax({
            url: url,
            type: "GET",
            cache: false,
            success: function (res) {
                // console.log(res);
				model.data(res.data);
            },
            error: function (data) {
                console.log(data);
            }
        });
    }
    
    $(document).ready(function () {
        // getKecamatan();
        // $('#team-area').val('3578'); // Select the option with a value of '1'
        // $('#team-area').trigger('change'); // Notify any JS components that the value changed   
    });
</script>