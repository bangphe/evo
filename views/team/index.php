<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data Tim';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="team-index">
    <div class="m-portlet m-portlet--mobile">   
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        <?= $this->title; ?>
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <?= Html::a('<span><i class="fa fa-plus"></i><span>Tambah Data</span></span>', ['create'], ['class' => 'btn btn-success m-btn m-btn--custom m-btn--icon m-btn--air']) ?>
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet__body">
            <?= Yii::$app->session->getFlash('info'); ?>
            <!--begin: Datatable -->
            <table id="tablegrid" class="table table-striped- table-bordered table-hover table-checkable" width="100%">
                <thead>
                    <tr>
                        <th width="10px">No</th>
                        <th width="90px">Nama Tim</th>
                        <th width="40px">Area</th>
                        <!-- <th width="40px">Kecamatan</th> -->
                        <th width="80px">Status</th>
                        <th width="80px">Aksi</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
