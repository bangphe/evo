<?php

namespace app\components;

use Yii;
use yii\i18n\Formatter;
use yii\helpers\Html;
use yii\base\Widget;

class MyFormatter extends Widget {

	public static function success($message){
        return '<div class="alert alert-success alert-dismissible show m-alert m-alert--square m-alert--air" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            </button>
            '.$message.'
        </div>';
    }

    public static function success2($message){
        return '<div class="alert alert-success alert-dismissible fade show" role="alert" style="margin-top: 20px;">
          '.$message.'
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>';
    }

    public static function error($message){
        return '<div class="alert alert-danger alert-dismissible fade show" role="alert">
          '.$message.'
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>';
    }

    public static function warning($message){
        return '<div class="alert alert-warning alert-dismissible fade show" role="alert">
          '.$message.'
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>';
    }

    public static function info($message){
        return '<div class="alert alert-info alert-dismissible fade show" role="alert">
          '.$message.'
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>';
    }
    
    public static function formatTanggal($value)
    {
        if($value != NULL) {
            $date = explode('-',$value);
            $time = substr($date[2], 3);
            $bulan = '';
            switch($date[1])
            {
                case '01':
                    $bulan = 'January';
                    break;
                case '02':
                    $bulan = 'February';
                    break;
                case '03':
                    $bulan = 'March';
                    break;
                case '04':
                    $bulan = 'April';
                    break;
                case '05':
                    $bulan = 'May';
                    break;
                case '06':
                    $bulan = 'June';
                    break;
                case '07':
                    $bulan = 'July';
                    break;
                case '08':
                    $bulan = 'August';
                    break;
                case '09':
                    $bulan = 'September';
                    break;
                case '10':
                    $bulan = 'October';
                    break;
                case '11':
                    $bulan = 'Nopember';
                    break;
                case '12':
                    $bulan = 'December';
                    break;
            }

            return substr($date[2],0,2).' '.$bulan.' '.$date[0].' at '.$time;
        }
    }

    public static function formatBlnThn($value)
    {
        if($value != NULL) {
            $date = explode('-',$value);
            $bulan = '';
            switch($date[1])
            {
                case '01':
                    $bulan = 'Januari';
                    break;
                case '02':
                    $bulan = 'Februari';
                    break;
                case '03':
                    $bulan = 'Maret';
                    break;
                case '04':
                    $bulan = 'April';
                    break;
                case '05':
                    $bulan = 'Mei';
                    break;
                case '06':
                    $bulan = 'Juni';
                    break;
                case '07':
                    $bulan = 'Juli';
                    break;
                case '08':
                    $bulan = 'Agustus';
                    break;
                case '09':
                    $bulan = 'September';
                    break;
                case '10':
                    $bulan = 'Oktober';
                    break;
                case '11':
                    $bulan = 'Nopember';
                    break;
                case '12':
                    $bulan = 'Desember';
                    break;
            }

            return substr($date[2],0,2).' '.$bulan.' '.$date[0];
        }
    }

    public static function formatTanggalSingkat($value)
    {
        if($value != NULL) {
            $date = explode('-',$value);
            $bulan = '';
            switch($date[1])
            {
                case '01':
                    $bulan = 'Jan';
                    break;
                case '02':
                    $bulan = 'Feb';
                    break;
                case '03':
                    $bulan = 'Mar';
                    break;
                case '04':
                    $bulan = 'Apr';
                    break;
                case '05':
                    $bulan = 'Mei';
                    break;
                case '06':
                    $bulan = 'Jun';
                    break;
                case '07':
                    $bulan = 'Jul';
                    break;
                case '08':
                    $bulan = 'Agus';
                    break;
                case '09':
                    $bulan = 'Sep';
                    break;
                case '10':
                    $bulan = 'Okt';
                    break;
                case '11':
                    $bulan = 'Nop';
                    break;
                case '12':
                    $bulan = 'Des';
                    break;
            }

            return substr($date[2],0,2).' '.$bulan.' '.$date[0];
        }
    }

    public static function formatTanggalSingkatEng($value)
    {
        if($value != NULL) {
            $date = explode('-',$value);
            $bulan = '';
            switch($date[1])
            {
                case '01':
                    $bulan = 'Jan';
                    break;
                case '02':
                    $bulan = 'Feb';
                    break;
                case '03':
                    $bulan = 'Mar';
                    break;
                case '04':
                    $bulan = 'Apr';
                    break;
                case '05':
                    $bulan = 'May';
                    break;
                case '06':
                    $bulan = 'Jun';
                    break;
                case '07':
                    $bulan = 'Jul';
                    break;
                case '08':
                    $bulan = 'Aug';
                    break;
                case '09':
                    $bulan = 'Sep';
                    break;
                case '10':
                    $bulan = 'Oct';
                    break;
                case '11':
                    $bulan = 'Nov';
                    break;
                case '12':
                    $bulan = 'Dec';
                    break;
            }

            return substr($date[2],0,2).' '.$bulan.' '.$date[0];
        }
    }

    public static function formatTanggalNoTahun($value)
    {
        if($value != NULL) {
            $date = explode('-',$value);
            $bulan = '';
            switch($date[1])
            {
                case '01':
                    $bulan = 'Jan';
                    break;
                case '02':
                    $bulan = 'Feb';
                    break;
                case '03':
                    $bulan = 'Mar';
                    break;
                case '04':
                    $bulan = 'Apr';
                    break;
                case '05':
                    $bulan = 'Mei';
                    break;
                case '06':
                    $bulan = 'Jun';
                    break;
                case '07':
                    $bulan = 'Jul';
                    break;
                case '08':
                    $bulan = 'Agus';
                    break;
                case '09':
                    $bulan = 'Sep';
                    break;
                case '10':
                    $bulan = 'Okt';
                    break;
                case '11':
                    $bulan = 'Nop';
                    break;
                case '12':
                    $bulan = 'Des';
                    break;
            }

            return substr($date[2],0,2).' '.$bulan;
        }
    }

    public static function formatMonthOnly($value)
    {
        if($value != NULL) {
            $date = explode('-',$value);
            $bulan = '';
            switch($date[1])
            {
                case '01':
                    $bulan = 'Jan';
                    break;
                case '02':
                    $bulan = 'Feb';
                    break;
                case '03':
                    $bulan = 'Mar';
                    break;
                case '04':
                    $bulan = 'Apr';
                    break;
                case '05':
                    $bulan = 'Mei';
                    break;
                case '06':
                    $bulan = 'Jun';
                    break;
                case '07':
                    $bulan = 'Jul';
                    break;
                case '08':
                    $bulan = 'Agus';
                    break;
                case '09':
                    $bulan = 'Sep';
                    break;
                case '10':
                    $bulan = 'Okt';
                    break;
                case '11':
                    $bulan = 'Nop';
                    break;
                case '12':
                    $bulan = 'Des';
                    break;
            }

            return $bulan;
        }
    }

    public static function active($id, $mode='c')
    {
    	/* mode conf:
        /* c = controller name
        /* a = action in controller name ($add)
        /* m = module name
        /* ------- ----------------------*/

        $status = '';

        if($mode=='c'){
            if(in_array(Yii::$app->controller->id, $id)){
                $status = 'active';
            }
        }elseif($mode=='a'){
            $_0 = Yii::$app->urlManager->parseRequest(Yii::$app->request)[0];
            $_1 = Yii::$app->urlManager->parseRequest(Yii::$app->request)[1];
            
            if($_0==null && $_1==null){
                $url = 'site'; // is home :)
            }
            //if($_0!=null && $_1!=null){
             //   $url = $_0.'/'.$_1['id']; // detail page with id
            //}
            else{
                $url = $_0;
            }

            if(in_array($url, $id)){
                $status = 'active';
            }
        }elseif($mode=='m'){
            if(in_array(Yii::$app->controller->module->id, $id)){
                $status = 'active';
            }
        }

        return $status;
    }

    static public function slug($text)
    {
        // untuk merubah Hallo Nasrul Hadi, menjadi
        // hallo-nasrul-hadi

        $text = preg_replace('~[^\pL\d]+~u', '-', $text);
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        $text = preg_replace('~[^-\w]+~', '', $text);
        $text = trim($text, '-');
        $text = preg_replace('~-+~', '-', $text);
        $text = strtolower($text);

        if (empty($text))
            return 'not-set';

        return $text;
    }

    public static function setTimeElapse($created_time) {
        date_default_timezone_set('Asia/Jakarta'); //Change as per your default time
        $str = strtotime($created_time);
        $today = strtotime(date('Y-m-d H:i:s'));

        // It returns the time difference in Seconds...
        $time_differnce = $today-$str;

        // To Calculate the time difference in Years...
        $years = 60*60*24*365;

        // To Calculate the time difference in Months...
        $months = 60*60*24*30;

        // To Calculate the time difference in Days...
        $days = 60*60*24;

        // To Calculate the time difference in Hours...
        $hours = 60*60;

        // To Calculate the time difference in Minutes...
        $minutes = 60;

        if(intval($time_differnce/$years) > 1)
        {
            return intval($time_differnce/$years)." years ago";
        }else if(intval($time_differnce/$years) > 0)
        {
            return intval($time_differnce/$years)." year ago";
        }else if(intval($time_differnce/$months) > 1)
        {
            return intval($time_differnce/$months)." months ago";
        }else if(intval(($time_differnce/$months)) > 0)
        {
            return intval(($time_differnce/$months))." month ago";
        }else if(intval(($time_differnce/$days)) > 1)
        {
            return intval(($time_differnce/$days))." days ago";
        }else if (intval(($time_differnce/$days)) > 0) 
        {
            return intval(($time_differnce/$days))." day ago";
        }else if (intval(($time_differnce/$hours)) > 1) 
        {
            return intval(($time_differnce/$hours))." hours ago";
        }else if (intval(($time_differnce/$hours)) > 0) 
        {
            return intval(($time_differnce/$hours))." hour ago";
        }else if (intval(($time_differnce/$minutes)) > 1) 
        {
            return intval(($time_differnce/$minutes))." minutes ago";
        }else if (intval(($time_differnce/$minutes)) > 0) 
        {
            return intval(($time_differnce/$minutes))." minute ago";
        }else if (intval(($time_differnce)) > 1) 
        {
            return intval(($time_differnce))." seconds ago";
        }else
        {
            return "few seconds ago";
        }
    }
}