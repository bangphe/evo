<?php

namespace app\controllers;

use yii\base\Exception;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;

class ApilocationController extends ActiveController
{
    public $modelClass = 'app\models\SpgLocation';

    /* Declare actions supported by APIs */
    public function actions()
    {
        $actions = parent::actions();
        // unset($actions['create']);
        // unset($actions['update']);
        // unset($actions['delete']);
        // unset($actions['view']);
        unset($actions['index']);

        return $actions;
    }

    public function actionIndex()
    {
        $model = new $this->modelClass;
        if (!empty($_GET)) {
            foreach ($_GET as $key => $value) {
                if (!$model->hasAttribute($key)) {
                    throw new \yii\web\HttpException(404, 'Invalid attribute:' . $key);
                }
            }
            try {
                $provider = new ActiveDataProvider([
                    'query' => $model->find()->where($_GET),
                    'pagination' => false
                ]);
            } catch (Exception $ex) {
                throw new \yii\web\HttpException(500, 'Internal server error');
            }
    
            if ($provider->getCount() <= 0) {
                throw new \yii\web\HttpException(404, 'No entries found with this query string');
            } else {
                return $provider;
            }
        } else {
            $provider = new ActiveDataProvider([
                'query' => $model->find(),
                'pagination' => false
            ]);

            return $provider;
        }
    }
}
?>