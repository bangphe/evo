<?php

namespace app\controllers;

use Yii;
use app\models\SpgSurvey;
use app\models\Team;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\MyFormatter;
use yii\filters\AccessControl;
use yii\web\Response;

/**
 * SurveyController implements the CRUD actions for SpgSurvey model.
 */
class SurveyController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                // 'ruleConfig' => [
                //     'class' => AccessRule::className(),
                // ],
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'getdata', 'view', 'delete', 'export', 'exportcsv'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
    }

    /**
     * Lists all SpgSurvey models.
     * @return mixed
     */
    public function actionIndex()
    {
        $start = date('Y-m-01');
        $end = date('Y-m-t');
        $dataProvider = new ActiveDataProvider([
            'query' => SpgSurvey::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'start' => $start,
            'end' => $end,
        ]);
    }

    public function actionGetdata()
    {
        ini_set('memory_limit', '512M');
        ini_set('max_execution_time', '300'); //300 seconds = 5 minutes

        $start = date('Y-m-01');
        $end = date('Y-m-t');

        if (isset($_GET['start'], $_GET['end'])) {
            $start = $_GET['start'];
            $end = $_GET['end'];
        }
        
        $role = Yii::$app->user->identity->role->role;
        $team_id = Yii::$app->user->identity->team_id;
        if ($role == 'super-admin') {
            $query = SpgSurvey::find();
            if ($start != "" && $end != "") {
                $query = SpgSurvey::find()->where(['between', 'tgl_input', $start, $end]);
            }
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination'=>false
            ]);
        } elseif ($role == 'admin-regional') {
            $locations = explode(',', Yii::$app->user->identity->location);
            $teams_id = [];
            // get query teams per location
            $query = Team::find();
            foreach($locations as $loc) {
                $query->orFilterWhere(['like', 'area', $loc]);
            }
            $teams = $query->all();
            foreach($teams as $team) {
                array_push($teams_id, $team->id);
            }

            $dataJourneyPerTeam = (new \yii\db\Query())
                ->select(['id'])
                ->from('spg_journey')
                ->where(['team_id' => $teams_id])
                ->all();
            
            $journeyIds = [];
            if ($dataJourneyPerTeam != NULL) {
                foreach($dataJourneyPerTeam as $row) {
                    array_push($journeyIds, $row['id']);
                }
            }

            $query = SpgSurvey::find()->where(['journey_id' => $journeyIds]);
            if ($start != "" && $end != "") {
                $query = SpgSurvey::find()->where(['journey_id' => $journeyIds])
                    ->andWhere(['between', 'tgl_input', $start, $end]);
            }
            
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination'=>false
            ]);
        } else {
            $dataJourneyPerTeam = (new \yii\db\Query())
                ->select(['id'])
                ->from('spg_journey')
                ->where(['team_id' => $team_id])
                ->all();
            
            $journeyIds = [];
            if ($dataJourneyPerTeam != NULL) {
                foreach($dataJourneyPerTeam as $row) {
                    array_push($journeyIds, $row['id']);
                }
            }

            $query = SpgSurvey::find()->where(['journey_id' => $journeyIds]);
            if ($start != "" && $end != "") {
                $query = SpgSurvey::find()->where(['journey_id' => $journeyIds])
                    ->andWhere(['between', 'tgl_input', $start, $end]);
            }
            
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination'=>false
            ]);
        }

        $result = [];
        $data = [];
        $i = 1;
        foreach ($dataProvider->getModels() as $value) {
            $nestedData = [];
            $nestedData[] = $i;
            $nestedData[] = $value->spg['nama_spg'];
            $nestedData[] = $value['week'];
            $nestedData[] = $value['kecamatan'];
            $nestedData[] = $value['product'];
            $nestedData[] = $value['nama_pelanggan'];
            $nestedData[] = $value['lokasi'];
            $nestedData[] = $value['jumlah_beli'];
            $nestedData[] = $value['umur'];
            $nestedData[] = $value['pekerjaan'];
            $nestedData[] = $value['no_telpon'];
            $nestedData[] = MyFormatter::formatBlnThn($value['tgl_input']);
            $nestedData[] = $value['rokok_yang_dipakai'];
            $nestedData[] = $value['tempat_beli_rokok'];
            $nestedData[] = $value['tahu_rokok_evo'];
            $nestedData[] = $value['komentar_harga'];
            $nestedData[] = $value['komentar_rasa'];
            $nestedData[] = $value['komentar_kemasan'];
            // $nestedData[] = Spg::getLabelStatus($value['status']);
            // $nestedData[] = SpgJourney::getActionButton($value['id']);

            $data[] = $nestedData;
            $i++;
        }

        $result = array(
            "data" => $data,
        );

        // echo json_encode($result);
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $result;
    }

    // Export to excel
    public function actionExport()
    {
        ini_set('memory_limit', '8192M'); 
        ini_set('max_execution_time', '300'); //300 seconds = 5 minutes
        
        $start = '';
        $end = '';

        if (isset($_GET['start'], $_GET['end'])) {
            $start = $_GET['start'];
            $end = $_GET['end'];
        }

        $model = [];
        //get data
        $role = Yii::$app->user->identity->role->role;
        $team_id = Yii::$app->user->identity->team_id;
        if ($role == 'super-admin') {
            $model = SpgSurvey::find()->orderBy('id ASC')->all();
            if ($start != "" && $end != "") {
                $model = SpgSurvey::find()->where(['between', 'tgl_input', $start, $end])->orderBy('id ASC')->all();
            }
        } elseif ($role == 'admin-regional') {
            $locations = explode(',', Yii::$app->user->identity->location);
            $teams_id = [];
            // get query teams per location
            $query = Team::find();
            foreach($locations as $loc) {
                $query->orFilterWhere(['like', 'area', $loc]);
            }
            $teams = $query->all();
            foreach($teams as $team) {
                array_push($teams_id, $team->id);
            }

            $dataJourneyPerTeam = (new \yii\db\Query())
                ->select(['id'])
                ->from('spg_journey')
                ->where(['team_id' => $teams_id])
                ->all();
            
            $journeyIds = [];
            if ($dataJourneyPerTeam != NULL) {
                foreach($dataJourneyPerTeam as $row) {
                    array_push($journeyIds, $row['id']);
                }
            }
            
            $model = SpgSurvey::find()->where(['journey_id' => $journeyIds])->all();
            if ($start != "" && $end != "") {
                $model = SpgSurvey::find()->where(['journey_id' => $journeyIds])
                    ->andWhere(['between', 'tgl_input', $start, $end]);
            }
        } else {
            $dataJourneyPerTeam = (new \yii\db\Query())
                ->select(['id'])
                ->from('spg_journey')
                ->where(['team_id' => $team_id])
                ->all();
            
            $journeyIds = [];
            if ($dataJourneyPerTeam != NULL) {
                foreach($dataJourneyPerTeam as $row) {
                    array_push($journeyIds, $row['id']);
                }
            }
            
            $model = SpgSurvey::find()->where(['journey_id' => $journeyIds])->all();
            if ($start != "" && $end != "") {
                $model = SpgSurvey::find()->where(['journey_id' => $journeyIds])
                    ->andWhere(['between', 'tgl_input', $start, $end]);
            }
        }

        //read 2007 format
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load(Yii::getAlias('@webroot').'/report/Format_Survey.xlsx');

        $spreadsheet->getProperties()->setTitle("Export-Survey");
        $spreadsheet->getProperties()->setSubject("Export-Survey");
        $spreadsheet->getProperties()->setDescription("Export-Survey");

        $spreadsheet->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
        $spreadsheet->setActiveSheetIndex(0);

        // set styles from array
        $styleBorders = [
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
                'right' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
                'left' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],
        ];

        $styleBordersRightAlignment = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
            ],
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
                'right' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
                'left' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],
        ];

        $styleBordersLeftAlignment = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
            ],
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
                'right' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
                'left' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],
        ];
        
        // set rows begin
        $row = 4;
        if ($model != null) {
            foreach ($model as $key => $data) {
                // set cell value
                $sheet = $spreadsheet->getActiveSheet();
                $sheet->setCellValue('A'.$row, ($key+1));
                $sheet->setCellValue('B'.$row, $data->week);
                $sheet->setCellValue('C'.$row, $data->area);
                $sheet->setCellValue('D'.$row, $data->product);
                $sheet->setCellValue('E'.$row, $data->spg->nama_spg);
                $sheet->setCellValue('F'.$row, $data->kecamatan);
                $sheet->setCellValue('G'.$row, $data->lokasi);
                $sheet->setCellValue('H'.$row, $data->tgl_input);
                $sheet->setCellValue('I'.$row, $data->nama_pelanggan);
                $sheet->setCellValue('J'.$row, $data->umur);
                $sheet->setCellValue('K'.$row, $data->jenis_kelamin);
                $sheet->setCellValue('L'.$row, $data->alamat);
                $sheet->setCellValue('M'.$row, $data->pekerjaan);
                $sheet->setCellValue('N'.$row, $data->no_telpon);
                $sheet->setCellValue('O'.$row, $data->jumlah_beli);
                $sheet->setCellValue('P'.$row, $data->rokok_yang_dipakai);
                $sheet->setCellValue('Q'.$row, $data->tahu_rokok_evo);
                $sheet->setCellValue('R'.$row, $data->tempat_beli_rokok);
                $sheet->setCellValue('S'.$row, $data->komentar_harga);
                $sheet->setCellValue('T'.$row, $data->komentar_rasa);
                $sheet->setCellValue('U'.$row, $data->komentar_kemasan);
                $sheet->setCellValue('V'.$row, $data->area);

                // set style cell
                $sheet->getStyle('A'.$row)->applyFromArray($styleBorders);
                $sheet->getStyle('B'.$row)->applyFromArray($styleBorders);
                $sheet->getStyle('C'.$row)->applyFromArray($styleBorders);
                $sheet->getStyle('D'.$row)->applyFromArray($styleBorders);
                $sheet->getStyle('E'.$row)->applyFromArray($styleBorders);
                $sheet->getStyle('F'.$row)->applyFromArray($styleBorders);
                $sheet->getStyle('G'.$row)->applyFromArray($styleBordersRightAlignment);
                $sheet->getStyle('H'.$row)->applyFromArray($styleBordersRightAlignment);
                $sheet->getStyle('I'.$row)->applyFromArray($styleBordersRightAlignment);
                $sheet->getStyle('J'.$row)->applyFromArray($styleBorders);
                $sheet->getStyle('K'.$row)->applyFromArray($styleBorders);
                $sheet->getStyle('L'.$row)->applyFromArray($styleBorders);
                $sheet->getStyle('M'.$row)->applyFromArray($styleBorders);
                $sheet->getStyle('N'.$row)->applyFromArray($styleBorders);
                $sheet->getStyle('O'.$row)->applyFromArray($styleBorders);
                $sheet->getStyle('P'.$row)->applyFromArray($styleBorders);
                $sheet->getStyle('Q'.$row)->applyFromArray($styleBorders);
                $sheet->getStyle('R'.$row)->applyFromArray($styleBorders);
                $sheet->getStyle('S'.$row)->applyFromArray($styleBorders);
                $sheet->getStyle('T'.$row)->applyFromArray($styleBorders);
                $sheet->getStyle('U'.$row)->applyFromArray($styleBorders);
                $sheet->getStyle('V'.$row)->applyFromArray($styleBorders);

                $row++;
            }
            // End: writing values

            // set footer cell
            // $spreadsheet->getActiveSheet()->setCellValue('D'.$row, 'Rp '.number_format($totalIuran, 2, ',', '.')); // Total iuran
            // $spreadsheet->getActiveSheet()->getStyle('D'.$row)->getFont()->setBold(true);
            // $spreadsheet->getActiveSheet()->getStyle('D'.$row)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);

            // $spreadsheet->getActiveSheet()->setCellValue('F'.$row, 'Rp '.number_format($totalDenda, 2, ',', '.')); // Total denda
            // $spreadsheet->getActiveSheet()->getStyle('F'.$row)->getFont()->setBold(true);
            // $spreadsheet->getActiveSheet()->getStyle('F'.$row)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);

            // $spreadsheet->getActiveSheet()->setCellValue('G'.$row, 'Rp '.number_format($total, 2, ',', '.')); // Total tunggakan
            // $spreadsheet->getActiveSheet()->getStyle('G'.$row)->getFont()->setBold(true);
            // $spreadsheet->getActiveSheet()->getStyle('G'.$row)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        }

        //rename sheet
        $spreadsheet->getActiveSheet()->setTitle('Data Survey');

        $objWriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx'); //in 2007 format

        // Redirect output to a client's web browser (Excel5)
        $filename = 'Rekap-Survey-'.date('YmdHi').'.xlsx';
        $path = Yii::getAlias('@webroot').'/report/';
        if (!is_dir($path)) {
            mkdir($path);
        }
        $fullpath = Yii::getAlias('@webroot').'/report/'.$filename;
        
        $objWriter->save($fullpath);

        return Yii::$app->response->sendFile($fullpath)->on(Response::EVENT_AFTER_SEND, function ($e){
            unlink($e->data);
        }, $fullpath);
    }
    
    public function actionExportcsv()
    {
        ini_set('memory_limit', '8192M'); 
        ini_set('max_execution_time', '300'); //300 seconds = 5 minutes
        
        $model = [];
        $start = date('Y-m-01');
        $end = date('Y-m-t');

        if (isset($_GET['start'], $_GET['end'])) {
            $start = $_GET['start'];
            $end = $_GET['end'];
        }
        //get data
        $role = Yii::$app->user->identity->role->role;
        $team_id = Yii::$app->user->identity->team_id;
        
        $header = [
            "Nama User",
            "Tanggal",
            "Timestamp",
            "Week",
            "AO",
            "Kecamatan",
            "Venue",
            "Lokasi",
            "Nama Customer",
            "Umur",
            "Jenis Kelamin",
            "Alamat Customer",
            "Pekerjaan",
            "No. Telp",
            "Jumlah Beli (pack)",
            "SOB",
            "Brand Awareness",
            "Tempat  membeli rokok",
            "Komentar Harga",
            "Komentar Rasa",
            "Komentar Kemasan",
            "Latitude",
            "Longitude"
        ];

        if ($role == 'super-admin') {
            $model = SpgSurvey::find()->select([
                'spg_id',
                'tgl_input',
                'created_at',
                'week',
                'lokasi',
                'area',
                'kecamatan',
                'venue',
                'nama_pelanggan',
                'umur',
                'jenis_kelamin',
                'alamat',
                'pekerjaan',
                'no_telpon',
                'jumlah_beli',
                'rokok_yang_dipakai',
                'tahu_rokok_evo',
                'tempat_beli_rokok',
                'komentar_harga',
                'komentar_rasa',
                'komentar_kemasan',
                'latitude',
                'longitude'
            ])->orderBy('id ASC')->all();
            if ($start != "" && $end != "") {
                $model = SpgSurvey::find()->select([
                    'spg_id',
                    'tgl_input',
                    'created_at',
                    'week',
                    'lokasi',
                    'area',
                    'kecamatan',
                    'venue',
                    'nama_pelanggan',
                    'umur',
                    'jenis_kelamin',
                    'alamat',
                    'pekerjaan',
                    'no_telpon',
                    'jumlah_beli',
                    'rokok_yang_dipakai',
                    'tahu_rokok_evo',
                    'tempat_beli_rokok',
                    'komentar_harga',
                    'komentar_rasa',
                    'komentar_kemasan',
                    'latitude',
                    'longitude'
                ])->where(['between', 'tgl_input', $start, $end])->orderBy('id ASC')->all();
            }
        } elseif ($role == 'admin-regional') {
            $locations = explode(',', Yii::$app->user->identity->location);
            $teams_id = [];
            // get query teams per location
            $query = Team::find();
            foreach($locations as $loc) {
                $query->orFilterWhere(['like', 'area', $loc]);
            }
            $teams = $query->all();
            foreach($teams as $team) {
                array_push($teams_id, $team->id);
            }

            $dataJourneyPerTeam = (new \yii\db\Query())
                ->select(['id'])
                ->from('spg_journey')
                ->where(['team_id' => $teams_id])
                ->all();
            
            $journeyIds = [];
            if ($dataJourneyPerTeam != NULL) {
                foreach($dataJourneyPerTeam as $row) {
                    array_push($journeyIds, $row['id']);
                }
            }
            
            $model = SpgSurvey::find()->select([
                'spg_id',
                'tgl_input',
                'created_at',
                'week',
                'lokasi',
                'area',
                'kecamatan',
                'venue',
                'nama_pelanggan',
                'umur',
                'jenis_kelamin',
                'alamat',
                'pekerjaan',
                'no_telpon',
                'jumlah_beli',
                'rokok_yang_dipakai',
                'tahu_rokok_evo',
                'tempat_beli_rokok',
                'komentar_harga',
                'komentar_rasa',
                'komentar_kemasan',
                'latitude',
                'longitude'
            ])->where(['journey_id' => $journeyIds])->all();
            if ($start != "" && $end != "") {
                $model = SpgSurvey::find()->select([
                    'spg_id',
                    'tgl_input',
                    'created_at',
                    'week',
                    'lokasi',
                    'area',
                    'kecamatan',
                    'venue',
                    'nama_pelanggan',
                    'umur',
                    'jenis_kelamin',
                    'alamat',
                    'pekerjaan',
                    'no_telpon',
                    'jumlah_beli',
                    'rokok_yang_dipakai',
                    'tahu_rokok_evo',
                    'tempat_beli_rokok',
                    'komentar_harga',
                    'komentar_rasa',
                    'komentar_kemasan',
                    'latitude',
                    'longitude'
                ])->where(['journey_id' => $journeyIds])
                ->andWhere(['between', 'tgl_input', $start, $end])->all();
            }
        } else {
            $dataJourneyPerTeam = (new \yii\db\Query())
                ->select(['id'])
                ->from('spg_journey')
                ->where(['team_id' => $team_id])
                ->all();
            
            $journeyIds = [];
            if ($dataJourneyPerTeam != NULL) {
                foreach($dataJourneyPerTeam as $row) {
                    array_push($journeyIds, $row['id']);
                }
            }
            
            $model = SpgSurvey::find()->select([
                'spg_id',
                'tgl_input',
                'created_at',
                'week',
                'lokasi',
                'area',
                'kecamatan',
                'venue',
                'nama_pelanggan',
                'umur',
                'jenis_kelamin',
                'alamat',
                'pekerjaan',
                'no_telpon',
                'jumlah_beli',
                'rokok_yang_dipakai',
                'tahu_rokok_evo',
                'tempat_beli_rokok',
                'komentar_harga',
                'komentar_rasa',
                'komentar_kemasan',
                'latitude',
                'longitude'
            ])->where(['journey_id' => $journeyIds])->all();
            if ($start != "" && $end != "") {
                $model = SpgSurvey::find()->select([
                    'spg_id',
                    'tgl_input',
                    'created_at',
                    'week',
                    'lokasi',
                    'area',
                    'kecamatan',
                    'venue',
                    'nama_pelanggan',
                    'umur',
                    'jenis_kelamin',
                    'alamat',
                    'pekerjaan',
                    'no_telpon',
                    'jumlah_beli',
                    'rokok_yang_dipakai',
                    'tahu_rokok_evo',
                    'tempat_beli_rokok',
                    'komentar_harga',
                    'komentar_rasa',
                    'komentar_kemasan',
                    'latitude',
                    'longitude'
                ])->where(['journey_id' => $journeyIds])
                ->andWhere(['between', 'tgl_input', $start, $end])->all();
            }
        }
        
        // die(var_dump($model));
        $filename = "Report-Survey-".date('YmdHi').".csv";
        $newModel = [];
        foreach ($model as $key => $subArr) {
            $newModel[$key][] = $subArr->spg['nama_spg'];
            $newModel[$key][] = $subArr['tgl_input'];
            $newModel[$key][] = date('H:i', strtotime($subArr['created_at']));
            $newModel[$key][] = $subArr['week'];
            $newModel[$key][] = $subArr->spg->team['area'];
            $newModel[$key][] = $subArr['kecamatan'];
            $newModel[$key][] = $subArr['venue'];
            $newModel[$key][] = $subArr['lokasi'];
            $newModel[$key][] = $subArr['nama_pelanggan'];
            $newModel[$key][] = $subArr['umur'];
            $newModel[$key][] = $subArr['jenis_kelamin'];
            $newModel[$key][] = $subArr['alamat'];
            $newModel[$key][] = $subArr['pekerjaan'];
            $newModel[$key][] = "=\"" .$subArr["no_telpon"]. "\""; //$subArr['no_telpon'];
            $newModel[$key][] = $subArr['jumlah_beli'];
            $newModel[$key][] = $subArr['rokok_yang_dipakai'];
            $newModel[$key][] = $subArr['tahu_rokok_evo'];
            $newModel[$key][] = $subArr['tempat_beli_rokok'];
            $newModel[$key][] = $subArr['komentar_harga'];
            $newModel[$key][] = $subArr['komentar_rasa'];
            $newModel[$key][] = $subArr['komentar_kemasan'];
            $newModel[$key][] = $subArr['latitude'];
            $newModel[$key][] = $subArr['longitude'];
        }

        $fp = fopen('php://output', 'w');
        
        // $list = array (
        //   array("Peter", "Griffin" ,"Oslo", "Norway"),
        //   array("Glenn", "Quagmire", "Oslo", "Norway")
        // );
        // die(var_dump($list));
        
        header('Content-Type: application/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename='.$filename);

        fputcsv($fp, $header);

        foreach ($newModel as $data)
        {
            fputcsv($fp, $data);
        }
        
        fclose($fp);

        exit;
    }

    /**
     * Displays a single SpgSurvey model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SpgSurvey model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SpgSurvey();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing SpgSurvey model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing SpgSurvey model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SpgSurvey model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SpgSurvey the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SpgSurvey::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
?>