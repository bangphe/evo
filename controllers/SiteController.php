<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\Spg;
use app\components\AccessRule;
use app\components\MyFormatter;
use app\models\SpgLocation;
use app\models\SpgSurvey;
use app\models\Team;
use yii\helpers\Html;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'login'],
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['login', 'logout'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['index', 'view', 'checkdata', 'update-notification', 'getdashboard'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionUpdateNotification()
    {
        $model = SpgLocation::getUnread();
        $location_ids = [];

        if ($model != NULL) {
            foreach($model as $m) {
                if ($m['is_read'] == NULL) {
                    $m['is_read'] = 'yes';
                    array_push($location_ids, $m['id']);
                }
            }
            
            SpgLocation::updateAll([   // field to be updated in first array
                'is_read' => 1,
            ],
            [  // conditions in second array
                'id' => $location_ids,
            ]);
        }

        $result = [];
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        // if ($modelDetail->save()) {
            $result = ["result" => "success", "message" => "OK"];
            return $result;
        // } else {
        //     $result = ["result" => "error", "message" => $modelDetail->getErrors()];
        //     return $result;
        // }
    }

    public function actionGetdata()
    {
        $role = Yii::$app->user->identity->role->role;
        $team_id = Yii::$app->user->identity->team_id;
        
        if ($role == 'super-admin') {
            $commandLokasi = Yii::$app->db->createCommand("SELECT * FROM spg_location WHERE id IN ( SELECT max(id) FROM spg_location GROUP BY spg_id, journey_id )")->queryAll();
            $dataLokasi = $commandLokasi; //SpgLocation::find()->all();
        } elseif ($role == 'admin-regional') {
            $locations = explode(',', Yii::$app->user->identity->location);
            $teams_id = [];
            // get query teams per location
            $query = Team::find();
            foreach($locations as $loc) {
                $query->orFilterWhere(['like', 'area', $loc]);
            }
            $teams = $query->all();
            foreach($teams as $team) {
                array_push($teams_id, $team->id);
            }

            $dataJourneyPerTeam = (new \yii\db\Query())
                ->select(['id'])
                ->from('spg_journey')
                ->where(['team_id' => $teams_id])
                ->all();
            
            $journeyIds = [];
            if ($dataJourneyPerTeam != NULL) {
                foreach($dataJourneyPerTeam as $row) {
                    array_push($journeyIds, $row['id']);
                }
            }
            
            $journeyId = "'" . implode("','", $journeyIds) . "'";
            
            $commandLokasi = Yii::$app->db->createCommand("SELECT * FROM spg_location WHERE id IN ( SELECT max(id) FROM spg_location GROUP BY spg_id, journey_id ) AND journey_id IN (" . $journeyId . ") ")->queryAll();
            $dataLokasi = $commandLokasi;
        } else {
            $dataJourneyPerTeam = (new \yii\db\Query())
                ->select(['id'])
                ->from('spg_journey')
                ->where(['team_id' => $team_id])
                ->all();
            
            $journeyIds = [];
            if ($dataJourneyPerTeam != NULL) {
                foreach($dataJourneyPerTeam as $row) {
                    array_push($journeyIds, $row['id']);
                }
            }
            $journeyId = "'" . implode("','", $journeyIds) . "'";
            
            $commandLokasi = Yii::$app->db->createCommand("SELECT * FROM spg_location WHERE id IN ( SELECT max(id) FROM spg_location GROUP BY spg_id, journey_id ) AND journey_id IN (" . $journeyId . ") ")->queryAll();
            $dataLokasi = $commandLokasi;
        }

        $result = [];
        $data = [];
        $i = 1;
        foreach ($dataLokasi as $value) {
            $spg = Spg::findOne($value['spg_id']);
            $team = Team::findOne($spg['team_id']);

            $nestedData = [];
            $nestedData[] = $i;
            $nestedData[] = $spg['nama_spg'].' | '.$team['nama_team'];
            // $nestedData[] = $value->team['nama_team'];
            $nestedData[] = MyFormatter::formatTanggalSingkat($value['created_at']);
            $nestedData[] = $value['kecamatan_survey'];
            $nestedData[] = $value['latitude'];
            $nestedData[] = $value['longitude'];
            $nestedData[] = Html::a('<i class="fa fa-search"></i>',['view','id'=>$value['id']],['class'=>'btn btn-primary m-btn m-btn--icon m-btn--icon-only']);

            $data[] = $nestedData;
            $i++;
        }

        $result = array(
            "data" => $data,
        );

        // echo json_encode($result);
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $result;
    }

    public function actionGetdashboard()
    {
        $role = Yii::$app->user->identity->role->role;
        $team_id = Yii::$app->user->identity->team_id;
        $start = date("Y-m-d");
        $end = date("Y-m-d");

        if (isset($_GET['start'], $_GET['end'])) {
            $start = $_GET['start'];
            $end = $_GET['end'];
        }

        if ($role == 'super-admin') {
            $countSurvey = SpgSurvey::find()->where(['between', 'tgl_input', $start, $end])->count();
            // count stock per day
            $commandStock = Yii::$app->db->createCommand("SELECT sum(jumlah_beli) FROM spg_survey WHERE tgl_input >= '" . $start . "' AND tgl_input <= '" . $end . "' ");
        } elseif ($role == 'admin-regional') {
            $locations = explode(',', Yii::$app->user->identity->location);
            $teams_id = [];
            // get query teams per location
            $query = Team::find();
            foreach($locations as $loc) {
                $query->orFilterWhere(['like', 'area', $loc]);
            }
            $teams = $query->all();
            foreach($teams as $team) {
                array_push($teams_id, $team->id);
            }

            $dataJourneyPerTeam = (new \yii\db\Query())
                ->select(['id'])
                ->from('spg_journey')
                ->where(['team_id' => $teams_id])
                ->all();
            
            $journeyIds = [];
            if ($dataJourneyPerTeam != NULL) {
                foreach($dataJourneyPerTeam as $row) {
                    array_push($journeyIds, $row['id']);
                }
            }
            
            $journeyId = "'" . implode("','", $journeyIds) . "'";
            // count stock per day
            $countSurvey = SpgSurvey::find()->where(['between', 'tgl_input', $start, $end])->andWhere(['journey_id' => $journeyIds])->count();
            $commandStock = Yii::$app->db->createCommand("SELECT sum(jumlah_beli) FROM spg_survey WHERE tgl_input >= '" . $start . "' AND tgl_input <= '" . $end . "' AND journey_id IN (" . $journeyId . ") ");
        } else {
            $dataJourneyPerTeam = (new \yii\db\Query())
                ->select(['id'])
                ->from('spg_journey')
                ->where(['team_id' => $team_id])
                ->all();
            
            $journeyIds = [];
            if ($dataJourneyPerTeam != NULL) {
                foreach($dataJourneyPerTeam as $row) {
                    array_push($journeyIds, $row['id']);
                }
            }
            $journeyId = "'" . implode("','", $journeyIds) . "'";
            
            // count stock per day
            $countSurvey = SpgSurvey::find()->where(['between', 'tgl_input', $start, $end])->andWhere(['journey_id' => $journeyIds])->count();
            $commandStock = Yii::$app->db->createCommand("SELECT sum(jumlah_beli) FROM spg_survey WHERE tgl_input >= '" . $start . "' AND tgl_input <= '" . $end . "' AND journey_id IN (" . $journeyId . ") ");
        }

        $countStock = $commandStock->queryScalar() == NULL ? 0 : $commandStock->queryScalar();

        $result = [];

        $result = array(
            "countSurvey" => (int)$countSurvey,
            "countStock" => (int)$countStock,
        );

        // echo json_encode($result);
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $result;
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $role = Yii::$app->user->identity->role->role;
        $team_id = Yii::$app->user->identity->team_id;
        $date_now = date("Y-m-d");
        
        $areaSub = Team::find()->where(['like', 'area', '%SURABAYA%', false])->all();
        $areaJbg = Team::find()->where(['like', 'area', '%JOMBANG%', false])->all();
        $areaJbr = Team::find()->where(['like', 'area', '%JEMBER%', false])->all();
        
        $countCCSub = 0;
        $countECSub = 0;
        $countVolSub = 0;
        
        $countCCJbg = 0;
        $countECJbg = 0;
        $countVolJbg = 0;
        
        $countCCJbr = 0;
        $countECJbr = 0;
        $countVolJbr = 0;

        // count stock cc per day        
        if ($areaSub != NULL) {
            $teams_sub_id = [];
            foreach($areaSub as $v) {
                array_push($teams_sub_id, $v->id);
            }
            $teamsId = "'" . implode("','", $teams_sub_id) . "'";

            $commandCCSub = Yii::$app->db->createCommand("SELECT count(*) as jumlah_beli FROM spg_survey JOIN spg ON spg.id = spg_survey.spg_id WHERE tgl_input >= '" . $date_now . "' AND jumlah_beli >= '0' AND spg.team_id IN (" . $teamsId . ") ");
            $countCCSub = $commandCCSub->queryScalar() == NULL ? 0 : $commandCCSub->queryScalar();
            // count stock EC per day
            $commandECSub = Yii::$app->db->createCommand("SELECT count(*) as jumlah_beli FROM spg_survey JOIN spg ON spg.id = spg_survey.spg_id WHERE tgl_input >= '" . $date_now . "' AND jumlah_beli > '0' AND spg.team_id IN (" . $teamsId . ") ");
            $countECSub = $commandECSub->queryScalar() == NULL ? 0 : $commandECSub->queryScalar();
            // count stock Vol per day
            $commandVolSub = Yii::$app->db->createCommand("SELECT sum(jumlah_beli) FROM spg_survey JOIN spg ON spg.id = spg_survey.spg_id WHERE tgl_input >= '" . $date_now . "' AND spg.team_id IN (" . $teamsId . ") ");
            $countVolSub = $commandVolSub->queryScalar() == NULL ? 0 : $commandVolSub->queryScalar();   
        }
        
        if ($areaJbg != NULL) {
            $teams_sda_id = [];
            foreach($areaJbg as $v) {
                array_push($teams_sda_id, $v->id);
            }
            $teamsId = "'" . implode("','", $teams_sda_id) . "'";

            $commandCCSda = Yii::$app->db->createCommand("SELECT count(*) as jumlah_beli FROM spg_survey JOIN spg ON spg.id = spg_survey.spg_id WHERE tgl_input >= '" . $date_now . "' AND jumlah_beli >= '0' AND spg.team_id IN (" . $teamsId . ") ");
            $countCCSda = $commandCCSda->queryScalar() == NULL ? 0 : $commandCCSda->queryScalar();
            // count stock EC per day
            $commandECSda = Yii::$app->db->createCommand("SELECT count(*) as jumlah_beli FROM spg_survey JOIN spg ON spg.id = spg_survey.spg_id WHERE tgl_input >= '" . $date_now . "' AND jumlah_beli > '0' AND spg.team_id IN (" . $teamsId . ") ");
            $countECSda = $commandECSda->queryScalar() == NULL ? 0 : $commandECSda->queryScalar();
            // count stock Vol per day
            $commandVolSda = Yii::$app->db->createCommand("SELECT sum(jumlah_beli) FROM spg_survey JOIN spg ON spg.id = spg_survey.spg_id WHERE tgl_input >= '" . $date_now . "' AND spg.team_id IN (" . $teamsId . ") ");
            $countVolSda = $commandVolSda->queryScalar() == NULL ? 0 : $commandVolSda->queryScalar();
        }
    
        if ($areaJbr != NULL) {
            $teams_bgk_id = [];
            foreach($areaJbr as $v) {
                array_push($teams_bgk_id, $v->id);
            }
            $teamsId = "'" . implode("','", $teams_bgk_id) . "'";

            $commandCCBgk = Yii::$app->db->createCommand("SELECT count(*) as jumlah_beli FROM spg_survey JOIN spg ON spg.id = spg_survey.spg_id WHERE tgl_input >= '" . $date_now . "' AND jumlah_beli >= '0' AND spg.team_id IN (" . $teamsId . ") ");
            $countCCBgk = $commandCCBgk->queryScalar() == NULL ? 0 : $commandCCBgk->queryScalar();
            // count stock EC per day
            $commandECBgk = Yii::$app->db->createCommand("SELECT count(*) as jumlah_beli FROM spg_survey JOIN spg ON spg.id = spg_survey.spg_id WHERE tgl_input >= '" . $date_now . "' AND jumlah_beli > '0' AND spg.team_id IN (" . $teamsId . ") ");
            $countECBgk = $commandECBgk->queryScalar() == NULL ? 0 : $commandECBgk->queryScalar();
            // count stock Vol per day
            $commandVolBgk = Yii::$app->db->createCommand("SELECT sum(jumlah_beli) FROM spg_survey JOIN spg ON spg.id = spg_survey.spg_id WHERE tgl_input >= '" . $date_now . "' AND spg.team_id IN (" . $teamsId . ") ");
            $countVolBgk = $commandVolBgk->queryScalar() == NULL ? 0 : $commandVolBgk->queryScalar();
        }

        if ($role == 'super-admin') {
            $countSurvey = SpgSurvey::find()->where(['>=', 'tgl_input', $date_now])->count();
            // count stock per day
            $commandStock = Yii::$app->db->createCommand("SELECT sum(jumlah_beli) FROM spg_survey WHERE tgl_input >= '" . $date_now . "'");
        } elseif ($role == 'admin-regional') {
            $locations = explode(',', Yii::$app->user->identity->location);
            $teams_id = [];
            // get query teams per location
            $query = Team::find();
            foreach($locations as $loc) {
                $query->orFilterWhere(['like', 'area', $loc]);
            }
            $teams = $query->all();
            foreach($teams as $team) {
                array_push($teams_id, $team->id);
            }

            $dataJourneyPerTeam = (new \yii\db\Query())
                ->select(['id'])
                ->from('spg_journey')
                ->where(['team_id' => $teams_id])
                ->all();
            
            $journeyIds = [];
            if ($dataJourneyPerTeam != NULL) {
                foreach($dataJourneyPerTeam as $row) {
                    array_push($journeyIds, $row['id']);
                }
            }
            
            $journeyId = "'" . implode("','", $journeyIds) . "'";
            // count stock per day
            $countSurvey = SpgSurvey::find()->where(['>=', 'tgl_input', $date_now])->andWhere(['journey_id' => $journeyIds])->count();
            $commandStock = Yii::$app->db->createCommand("SELECT sum(jumlah_beli) FROM spg_survey WHERE tgl_input >= '" . $date_now . "' AND journey_id IN (" . $journeyId . ") ");
        } else {
            $dataJourneyPerTeam = (new \yii\db\Query())
                ->select(['id'])
                ->from('spg_journey')
                ->where(['team_id' => $team_id])
                ->all();
            
            $journeyIds = [];
            if ($dataJourneyPerTeam != NULL) {
                foreach($dataJourneyPerTeam as $row) {
                    array_push($journeyIds, $row['id']);
                }
            }
            $journeyId = "'" . implode("','", $journeyIds) . "'";
            
            // count stock per day
            $countSurvey = SpgSurvey::find()->where(['>=', 'tgl_input', $date_now])->andWhere(['journey_id' => $journeyIds])->count();
            $commandStock = Yii::$app->db->createCommand("SELECT sum(jumlah_beli) FROM spg_survey WHERE tgl_input >= '" . $date_now . "' AND journey_id IN (" . $journeyId . ") ");
        }

        $countStock = $commandStock->queryScalar() == NULL ? 0 : $commandStock->queryScalar();

        return $this->render('index', [
            'countSurvey' => $countSurvey,
            'countStock' => $countStock,
            'countCCSub' => $countCCSub,
            'countECSub' => $countECSub,
            'countVolSub' => $countVolSub,
            'countCCJbg' => $countCCJbg,
            'countECJbg' => $countECJbg,
            'countVolJbg' => $countVolJbg,
            'countCCJbr' => $countCCJbr,
            'countECJbr' => $countECJbr,
            'countVolJbr' => $countVolJbr,
            'role' => $role,
            'team_id' => $team_id,
        ]);
    }

    public function actionCheckdata()
    {
        /* Send a string after a random number of seconds (2-10) */
        // sleep(rand(2,10));
        // echo("Hi! Have a random number: " . rand(1,10));
        $message = "";
        $result = [];
        $data = [];
        $message = "no data";
        $model = SpgLocation::find()->all();
        // die(var_dump($model));

        if ($model != null && count($model) > 0) {
            $nestedData = [];

            foreach ($model as $detail) {
                $nestedData['data'] = $detail;
                $nestedData['nama_spg'] = $detail->spg->nama_spg;
                $data[] = $nestedData;
                $message = "success";
            }
        }

        $result = array(
            "details" => $data,
            "message" => $message,
        );

        // echo json_encode($result);
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $result;
    }

    /**
     * Displays a single Spg model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        // $model = SpgLocation::find()
        //     ->andWhere(['>=', 'created_at', $myDate])
        //     ->andWhere(['status' => 'active'])
        //     ->andWhere(['user_id' => Yii::$app->user->identity->id])
        //     ->all();
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        $this->layout = '@app/views/layouts/blank';
        
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Finds the Spg model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Spg the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SpgLocation::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
?>