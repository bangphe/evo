<?php

namespace app\controllers;

use Yii;
use yii\rest\ActiveController;
use app\models\Spg;
use app\models\LoginSpgForm;
use app\models\LoginTeamleadForm;
use app\models\TeamLeader;
use app\models\Kabupaten;

class ApispgController extends ActiveController
{
    public $modelClass = 'app\models\Spg';

    /* Declare actions supported by APIs (Added in api/modules/v1/components/controller.php too) */
    // public function actions(){
    //     $actions = parent::actions();
    //     unset($actions['create']);
    //     unset($actions['update']);
    //     unset($actions['delete']);
    //     unset($actions['view']);
    //     unset($actions['index']);
    //     return $actions;
    // }

    /* Declare methods supported by APIs */
    // protected function verbs(){
    //     return [
    //         'create' => ['POST'],
    //         'update' => ['PUT', 'PATCH','POST'],
    //         'delete' => ['DELETE'],
    //         'view' => ['GET'],
    //         'index'=>['GET'],
    //     ];
    // }

    // public function actionIndex($count = 10){
    //     /** @var TweetLastfinder $tweetLastFinder */
    //     $tweetLastFinder = Yii::$app->get('tweetlastfinder');

    //     /**
    //      * @var TweetShow $tweetShow
    //      */
    //     $tweetShow = Yii::$app->get('tweetshow');

    //     // This will return in JSON:
    //     return $tweetLastFinder->findLastTweets($count);
    // }

    /**
     * Spg login.
     *
     * @return mixed
     */
    public function actionLogin(){
        $loginSpg = new LoginSpgForm();
        $loginTeamlead = new LoginTeamleadForm();
        $params = Yii::$app->request->post();
        if (isset($params['username'], $params['password'])) {
            $username = $params['username'];
            $password = $params['password'];

            // login spg
            $loginSpg->username = $username;
            $loginSpg->password = $password;

            // login team leader
            $loginTeamlead->username = $username;
            $loginTeamlead->password = $password;
            if ($loginSpg->login()) {
                $spg = Spg::findByUsername($loginSpg->username);
                $response['message'] = 'You are now logged in!';
                $response['user'] = $spg;
                $response['team_name'] = $spg->team->nama_team;
                $response['kabupaten'] = Kabupaten::find()->where(['name'=>$spg->team->area])->one();
                $response['user_type'] = 'spg';
            } elseif ($loginTeamlead->login()) {
                $team_leader = TeamLeader::findByUsername($loginTeamlead->username);
                $response['message'] = 'You are now logged in!';
                $response['user'] = $team_leader;
                $response['team_name'] = $team_leader->team->nama_team;
                $response['kabupaten'] = Kabupaten::find()->where(['name'=>$team_leader->team->area])->one();
                $response['user_type'] = 'team_leader';
            } else {
                $loginSpg->validate();
                $response['errors'] = $loginSpg->getErrors();
            }
        } else {
            $response['message'] = 'Username & Password must be filled!';
        }

        return $response;
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        $response['message'] = 'You are now logged out!';
        return $response;
    }
}
?>