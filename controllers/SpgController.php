<?php

namespace app\controllers;

use Yii;
use app\models\Spg;
use app\models\Team;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\MyFormatter;
use yii\filters\AccessControl;

/**
 * SpgController implements the CRUD actions for Spg model.
 */
class SpgController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                // 'ruleConfig' => [
                //     'class' => AccessRule::className(),
                // ],
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'getdata', 'view', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
    }

    /**
     * Lists all Spg models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Spg::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionGetdata()
    {
        $role = Yii::$app->user->identity->role->role;
        $team_id = Yii::$app->user->identity->team_id;
        if ($role == 'super-admin') {
            $dataProvider = new ActiveDataProvider([
                'query' => Spg::find(),
                'pagination'=>false
            ]);
        } elseif ($role == 'admin-regional') {
            $teams_id = [];
            $locations = explode(',', Yii::$app->user->identity->location);
            // get query teams per location
            $query = Team::find();
            foreach($locations as $loc) {
                $query->orFilterWhere(['like', 'area', $loc]);
            }
            $teams = $query->all();
            foreach($teams as $team) {
                array_push($teams_id, $team->id);
            }
            
            $dataProvider = new ActiveDataProvider([
                'query' => Spg::find()->where(['team_id'=>$teams_id]),
                'pagination'=>false
            ]);
        } else {
            $dataProvider = new ActiveDataProvider([
                'query' => Spg::find()->where(['team_id'=>$team_id]),
                'pagination'=>false
            ]);
        }

        $result = [];
        $data = [];
        $i = 1;
        foreach ($dataProvider->getModels() as $value) {
            $nestedData = [];
            $nestedData[] = $i;
            $nestedData[] = $value['nama_spg'];
            $nestedData[] = $value->team['nama_team'];
            $nestedData[] = $value['username'];
            // $nestedData[] = $value['kecamatan'];
            // $nestedData[] = $value['location'];
            $nestedData[] = Spg::getLabelStatus($value['status']);
            $nestedData[] = Spg::getActionButton($value['id']);

            $data[] = $nestedData;
            $i++;
        }

        $result = array(
            "data" => $data,
        );

        // echo json_encode($result);
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $result;
    }

    /**
     * Displays a single Spg model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Spg model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Spg();

        if ($model->load(Yii::$app->request->post())) {
            $model->password = md5($model->password);
            $model->created_at = date('Y-m-d H:i:s');
            if ($model->save()) {
                Yii::$app->session->setFlash('info', MyFormatter::success('<strong>Selamat!</strong> Data telah tersimpan.'));
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Spg model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $password = $model->password;

        if ($model->load(Yii::$app->request->post())) {
            if ($model->password != "") {
                $model->password = md5($model->password);
            } else {
                $model->password = $password;
            }
            
            if ($model->save()) {
                Yii::$app->session->setFlash('info', MyFormatter::success('<strong>Selamat!</strong> Data telah tersimpan.'));
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Spg model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->status = Spg::STATUS_NON_ACTIVE;
        $model->save(false);
        
        Yii::$app->session->setFlash('info', MyFormatter::success('<strong>Selamat!</strong> Data telah berhasil dihapus.'));

        return $this->redirect(['index']);
    }

    /**
     * Finds the Spg model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Spg the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Spg::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
?>