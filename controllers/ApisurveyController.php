<?php

namespace app\controllers;

use Yii;
use yii\base\Exception;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;
use app\models\Spg;
use app\models\SpgJourney;
use app\models\Team;
use app\models\SpgSurvey;
use yii\helpers\Url;
use yii\web\ServerErrorHttpException;

class ApisurveyController extends ActiveController
{
    public $modelClass = 'app\models\SpgSurvey';

    /* Declare actions supported by APIs */
    public function actions()
    {
        $actions = parent::actions();
        unset($actions['create']);
        // unset($actions['update']);
        // unset($actions['delete']);
        // unset($actions['view']);
        unset($actions['index']);

        return $actions;
    }

    public function actionCreate()
    {
        $modelSurvey = new SpgSurvey();
        $modelSurvey->load(Yii::$app->getRequest()->getBodyParams(), '');

        $date_now = date("Y-m-d");
        $modelJourney = SpgJourney::find()->where(['id' => $modelSurvey->journey_id])->one();
        $totalStockJourney = (int)$modelJourney->stock;

        $commandStock = Yii::$app->db->createCommand("SELECT SUM(jumlah_beli) as jumlah_beli FROM spg_survey WHERE spg_id = ".$modelSurvey->spg_id." AND journey_id = ".$modelSurvey->journey_id." AND tgl_input >= '" . $date_now . "' ");
        $countStock = $commandStock->queryScalar() == NULL ? 0 : (int)$commandStock->queryScalar();
        $totalStock = $countStock + (int)$modelSurvey->jumlah_beli;

        if ($totalStock <= $totalStockJourney) {
            if ($modelSurvey->save()) {
                $response = Yii::$app->getResponse();
                $response->setStatusCode(201);
                $id = implode(',', array_values($modelSurvey->getPrimaryKey(true)));
                $response->getHeaders()->set('Location', Url::toRoute(['view', 'id' => $id], true));
            } elseif (!$modelSurvey->hasErrors()) {
                throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
            }
        } else {
            throw new \yii\web\HttpException(400, 'Stock untuk hari ini telah habis');
        }

        return $modelSurvey;
    }

    public function actionIndex()
    {
        $model = new $this->modelClass;
        if (!empty($_GET)) {
            foreach ($_GET as $key => $value) {
                if (!$model->hasAttribute($key)) {
                    throw new \yii\web\HttpException(404, 'Invalid attribute:' . $key);
                }
            }
            try {
                $provider = new ActiveDataProvider([
                    'query' => $model->find()->where($_GET),
                    'pagination' => false
                ]);
            } catch (Exception $ex) {
                throw new \yii\web\HttpException(500, 'Internal server error');
            }
    
            if ($provider->getCount() <= 0) {
                throw new \yii\web\HttpException(404, 'No entries found with this query string');
            } else {
                return $provider;
            }
        } else {
            $provider = new ActiveDataProvider([
                'query' => $model->find(),
                'pagination' => false
            ]);

            return $provider;
        }
    }
    
    public function actionDashboard()
    {
        $spg_id = $_GET['spg_id'];
        $date_now = date('Y-m-d');
        $spg = $this->findSpg($spg_id);

        if ($spg != NULL) {
            // count stock cc per day
            $commandCC = Yii::$app->db->createCommand("SELECT count(*) as jumlah_beli FROM spg_survey WHERE spg_id = " . $spg_id . " AND tgl_input >= '" . $date_now . "' AND jumlah_beli >= '0'");
            $countCC = $commandCC->queryScalar() == NULL ? 0 : $commandCC->queryScalar();
            // count stock EC per day
            $commandEC = Yii::$app->db->createCommand("SELECT count(*) as jumlah_beli FROM spg_survey WHERE spg_id = " . $spg_id . " AND tgl_input >= '" . $date_now . "' AND jumlah_beli > '0'");
            $countEC = $commandEC->queryScalar() == NULL ? 0 : $commandEC->queryScalar();
            // count stock Vol per day
            $commandVol = Yii::$app->db->createCommand("SELECT sum(jumlah_beli) FROM spg_survey WHERE spg_id = " . $spg_id . " AND tgl_input >= '" . $date_now . "'");
            $countVol = $commandVol->queryScalar() == NULL ? 0 : $commandVol->queryScalar();
            // count survey
            $commandSurvey = Yii::$app->db->createCommand("SELECT COUNT(*) FROM spg_survey WHERE spg_id = " . $spg_id . " AND tempat_beli_rokok IS NOT NULL AND tahu_rokok_evo IS NOT NULL AND komentar_harga IS NOT NULL AND komentar_rasa IS NOT NULL AND komentar_kemasan IS NOT NULL AND tgl_input >= '" . $date_now . "'");
            $countSurvey = $commandSurvey->queryScalar() == NULL ? 0 : $commandSurvey->queryScalar();
            
            return [
                'totalCC' => $countCC,
                'totalEC' => $countEC,
                'totalVol' => $countVol,
                'totalSurvey' => $countSurvey,
            ];
        }
    }

    public function actionDashboardTeamLeader()
    {
        $team_id = $_GET['team_id'];
        $date_now = date('Y-m-d');
        $team = $this->findTeam($team_id);

        if ($team != NULL) {
            $dataJourneyPerTeam = (new \yii\db\Query())
                ->select(['id'])
                ->from('spg_journey')
                ->where(['team_id' => $team_id])
                ->all();
            
            $journeyIds = [];
            if ($dataJourneyPerTeam != NULL) {
                foreach($dataJourneyPerTeam as $row) {
                    array_push($journeyIds, $row['id']);
                }
            }
            $journeyId = "'" . implode("','", $journeyIds) . "'";

            // count stock cc per day
            $commandCC = Yii::$app->db->createCommand("SELECT count(*) as jumlah_beli FROM spg_survey WHERE journey_id IN (" . $journeyId . ") AND tgl_input >= '" . $date_now . "' AND jumlah_beli >= '0'");
            $countCC = $commandCC->queryScalar() == NULL ? 0 : $commandCC->queryScalar();
            $rowsCC = (new \yii\db\Query())
                ->select(['*'])
                ->from('spg_survey')
                ->innerJoin('spg', 'spg.id = spg_survey.spg_id')
                ->where(['journey_id' => $journeyIds])
                ->andWhere(['>=', 'tgl_input', date('Y-m-d')])
                ->andWhere(['>=', 'jumlah_beli', 0])
                ->all();
            // count stock EC per day
            $commandEC = Yii::$app->db->createCommand("SELECT count(*) as jumlah_beli FROM spg_survey WHERE journey_id IN (" . $journeyId . ") AND tgl_input >= '" . $date_now . "' AND jumlah_beli > '0'");
            $countEC = $commandEC->queryScalar() == NULL ? 0 : $commandEC->queryScalar();
            $rowsEC = (new \yii\db\Query())
                ->select(['*'])
                ->from('spg_survey')
                ->innerJoin('spg', 'spg.id = spg_survey.spg_id')
                ->where(['journey_id' => $journeyIds])
                ->andWhere(['>=', 'tgl_input', date('Y-m-d')])
                ->andWhere(['>', 'jumlah_beli', 0])
                ->all();
            // count stock Vol per day
            $commandVol = Yii::$app->db->createCommand("SELECT sum(jumlah_beli) FROM spg_survey WHERE journey_id IN (" . $journeyId . ") AND tgl_input >= '" . $date_now . "'");
            $countVol = $commandVol->queryScalar() == NULL ? 0 : $commandVol->queryScalar();
            $rowsVol = (new \yii\db\Query())
                ->select(['*'])
                ->from('spg_survey')
                ->innerJoin('spg', 'spg.id = spg_survey.spg_id')
                ->where(['journey_id' => $journeyIds])
                ->andWhere(['>=', 'tgl_input', date('Y-m-d')])
                ->all();
            // count stock Survey per day
            $commandSurvey = Yii::$app->db->createCommand("SELECT COUNT(*) FROM spg_survey WHERE journey_id IN (" . $journeyId . ") AND tempat_beli_rokok IS NOT NULL AND tahu_rokok_evo IS NOT NULL AND komentar_harga IS NOT NULL AND komentar_rasa IS NOT NULL AND komentar_kemasan IS NOT NULL AND tgl_input >= '" . $date_now . "'");
            $countSurvey = $commandSurvey->queryScalar() == NULL ? 0 : $commandSurvey->queryScalar();
            $rowsSurvey = (new \yii\db\Query())
                ->select(['*'])
                ->from('spg_survey')
                ->innerJoin('spg', 'spg.id = spg_survey.spg_id')
                ->where(['journey_id' => $journeyIds])
                ->andWhere(['>=', 'tgl_input', date('Y-m-d')])
                ->andWhere(['IS NOT', 'tempat_beli_rokok', NULL])
                ->andWhere(['IS NOT', 'tahu_rokok_evo', NULL])
                ->andWhere(['IS NOT', 'komentar_harga', NULL])
                ->andWhere(['IS NOT', 'komentar_rasa', NULL])
                ->andWhere(['IS NOT', 'komentar_kemasan', NULL])
                ->all();
            
            return [
                'totalCC' => $countCC,
                'rowsCC' => $rowsCC,
                'totalEC' => $countEC,
                'rowsEC' => $rowsEC,
                'totalVol' => $countVol,
                'rowsVol' => $rowsVol,
                'totalSurvey' => $countSurvey,
                'rowsSurvey' => $rowsSurvey,
            ];
        }
    }

    /**
     * Finds the Spg model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Spg the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findSpg($id)
    {
        if (($model = Spg::findOne($id)) !== null) {
            return $model;
        }

        throw new \yii\web\HttpException(500, 'Internal server error');
    }

    /**
     * Finds the Spg model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Spg the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findTeam($id)
    {
        if (($model = Team::findOne($id)) !== null) {
            return $model;
        }

        throw new \yii\web\HttpException(500, 'Internal server error');
    }
}
?>