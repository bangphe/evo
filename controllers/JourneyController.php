<?php

namespace app\controllers;

use Yii;
use app\models\MultipleForm;
use app\models\Spg;
use app\models\SpgJourney;
use app\models\Kecamatan;
use app\models\Team;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\MyFormatter;
use app\models\Kabupaten;
use app\models\SpgSurvey;
use app\models\SpgVenue;
use Exception;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/**
 * JourneyController implements the CRUD actions for SpgJourney model.
 */
class JourneyController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                // 'ruleConfig' => [
                //     'class' => AccessRule::className(),
                // ],
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'getdata', 'view', 'delete', 'getkecamatan', 'getteam'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
    }

    /**
     * Lists all SpgJourney models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => SpgJourney::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionGetdata()
    {
        $role = Yii::$app->user->identity->role->role;
        $team_id = Yii::$app->user->identity->team_id;
        if ($role == 'super-admin') {
            $dataProvider = new ActiveDataProvider([
                'query' => SpgJourney::find(),
                'pagination'=>false
            ]);
        } elseif ($role == 'admin-regional') {
            $teams_id = [];
            $locations = explode(',', Yii::$app->user->identity->location);
            // get query teams per location
            $query = Team::find();
            foreach($locations as $loc) {
                $query->orFilterWhere(['like', 'area', $loc]);
            }
            $teams = $query->all();
            foreach($teams as $team) {
                array_push($teams_id, $team->id);
            }
            
            $dataProvider = new ActiveDataProvider([
                'query' => SpgJourney::find()->where(['team_id'=>$teams_id]),
                'pagination'=>false
            ]);
        } else {
            $dataProvider = new ActiveDataProvider([
                'query' => SpgJourney::find()->where(['team_id'=>$team_id]),
                'pagination'=>false
            ]);
        }

        $result = [];
        $data = [];
        $i = 1;
        foreach ($dataProvider->getModels() as $value) {
            $command = Yii::$app->db->createCommand('SELECT sum(jumlah_beli) FROM spg_survey WHERE `journey_id` = '.$value['id'].' '); // please use a prepared statement instead, just a proof of concept
            $stock_akhir = $command->queryScalar();
            $stock = $value['stock'] - $stock_akhir;

            $nestedData = [];
            $nestedData[] = $i;
            $nestedData[] = $value->spg['nama_spg'];
            $nestedData[] = $value['week'];
            $nestedData[] = $value['kecamatan'];
            $nestedData[] = MyFormatter::formatBlnThn($value['journey_date']);
            $nestedData[] = (int)$stock;
            $nestedData[] = SpgJourney::getActionButton($value['id']);

            $data[] = $nestedData;
            $i++;
        }

        $result = array(
            "data" => $data,
        );

        // echo json_encode($result);
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $result;
    }

    public function actionGetkecamatan()
    {
        $team_id = $_GET['team'];
        $query = Kecamatan::find();
        if($team_id != "") {
            $team = Team::find()->where(['id'=>$team_id])->one();
            $kab = Kabupaten::find()->where(['name'=>$team['area']])->one();
            $query = Kecamatan::find()->where(['kabupaten_id'=>$kab['id']]);
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>false
        ]);

        $result = [];
        $data = [];
        foreach ($dataProvider->getModels() as $value) {
            $data[] = $value;
        }

        $result = array(
            "data" => $data,
        );

        // echo json_encode($result);
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $result;
    }

    public function actionGetteam()
    {
        // $spg_id = $_GET['spg'];
        // $spg = Spg::find()->where(['id'=>$spg_id])->one();
        // $team = $spg->team->nama_team;
        // $area = $spg->team->area;
        $team_id = $_GET['team_id'];
        $team = Team::find()->where(['id'=>$team_id])->one();
        $area = $team->area;
        $kab = Kabupaten::find()->where(['name'=>$area])->one();
        $data_kecamatan = Kecamatan::find()->where(['kabupaten_id'=>$kab['id']])->orderBy(['name'=>SORT_ASC])->all();
        $data_spg = Spg::find()->where(['team_id'=>$team_id])->orderBy(['nama_spg'=>SORT_ASC])->all();

        $result = array(
            "team" => $team,
            "area" => $area,
            "data_kecamatan" => $data_kecamatan,
            "data_spg" => $data_spg,
        );

        // echo json_encode($result);
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $result;
    }

    /**
     * Displays a single SpgJourney model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SpgJourney model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $modelSpg = new Spg;
        $modelsSpgJourney = [new SpgJourney];
        $modelsSpgVenue = [[new SpgVenue]];
        if (isset($_POST['Spg'])) {
            // die(var_dump($_POST));
            // $spg_id = $_POST['Spg']['id'];
            $list_kecamatan = [];
            $team_id = $_POST['Spg']['id_team'];
            $id_spgs = $_POST['Spg']['id_spgs'];
            $data_spg_per_team = Spg::find()->where(['IN', 'id', $id_spgs])->all();

            $modelsSpgJourney = MultipleForm::createMultiple(SpgJourney::classname());
            MultipleForm::loadMultiple($modelsSpgJourney, Yii::$app->request->post());

            // validate all models
            $valid = MultipleForm::validateMultiple($modelsSpgJourney);

            if (isset($_POST['SpgVenue'][0][0])) {
                foreach ($_POST['SpgVenue'] as $indexJourney => $venues) {
                    foreach ($venues as $indexVenue => $venue) {
                        array_push($list_kecamatan, $venue['kecamatan']);
                        $data['SpgVenue'] = $venue;
                        $modelVenue = new SpgVenue;
                        $modelVenue->load($data);
                        $modelsSpgVenue[$indexJourney][$indexVenue] = $modelVenue;
                        // $valid = $modelVenue->validate();
                    }
                }
            }
            // die(var_dump($modelsSpgVenue[0][0]));

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    $flag = false;
                    foreach ($modelsSpgJourney as $i => $modelSpgJourney) {
                        foreach ($data_spg_per_team as $team) {
                            $journey = new SpgJourney();
                            $journey->spg_id = $team->id;
                            $journey->team_id = $team_id;
                            $journey->week = $_POST['SpgJourney'][$i]['week'];
                            $journey->journey_date = $_POST['SpgJourney'][$i]['journey_date'];
                            $journey->kecamatan = implode(",", array_unique($list_kecamatan)); //implode(",", $_POST['SpgJourney'][$i]['kecamatan']);
                            $journey->stock = $_POST['SpgJourney'][$i]['stock'];
                            $journey->created_at = date('Y-m-d H:i:s');

                            // if (!($flag = $journey->save(false))) {
                            //     break;
                            // }
                            if ($journey->save(false)) {
                                if (isset($modelsSpgVenue[$i]) && is_array($modelsSpgVenue[$i])) {
                                    foreach ($modelsSpgVenue[$i] as $indexVenue => $modelVenue) {
                                        $venue = new SpgVenue();
                                        $venue->kecamatan = $modelVenue->kecamatan;
                                        $venue->venue = $modelVenue->venue;
                                        $venue->journey_id = $journey->id;
                                        $venue->created_at = date('Y-m-d H:i:s');
                                        if (!($flag = $venue->save(false))) {
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if ($flag) {
                        $transaction->commit();
                        Yii::$app->session->setFlash('info', MyFormatter::success('<strong>Selamat!</strong> Data telah tersimpan.'));
                        return $this->redirect(['index']);
                    } else {
                        $transaction->rollBack();
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                }
            }
        }

        return $this->render('create', [
            'modelSpg' => $modelSpg,
            'modelsSpgJourney' => (empty($modelsSpgJourney)) ? [new SpgJourney()] : $modelsSpgJourney,
            'modelsSpgVenue' => (empty($modelsSpgVenue)) ? [[new SpgVenue()]] : $modelsSpgVenue,
        ]);
    }

    /**
     * Updates an existing SpgJourney model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $modelsSpgJourney = SpgJourney::find()->where(['id'=>$id])->all();
        $modelSpg = Spg::findOne($modelsSpgJourney[0]->spg_id);
        $modelsSpgJourney[0]->kecamatan = explode(",", $modelsSpgJourney[0]->kecamatan);
        $modelsSpgVenue = [$modelsSpgJourney[0]->spgVenues];
        $kabupaten = Kabupaten::find()->where(['name'=>$modelsSpgJourney[0]->team->area])->one();
        $oldVenues = [];

        if (!empty($modelsSpgJourney)) {
            foreach ($modelsSpgJourney as $indexJourney => $modelJourney) {
                $venues = $modelJourney->spgVenues;
                $oldVenues = ArrayHelper::merge(ArrayHelper::index($venues, 'id'), $oldVenues);
            }
        }
        // die(var_dump($_POST));
        if (isset($_POST['SpgJourney'])) {
            // reset
            $list_kecamatan = [];
            $modelsSpgVenue = [];

            // validate person and houses models
            $valid = $modelsSpgJourney[0]->validate();
            // $valid = Model::validateMultiple($modelsSpgJourney) && $valid;

            $venuesIDs = [];
            if (isset($_POST['SpgVenue'][0][0])) {
                foreach ($_POST['SpgVenue'] as $indexJourney => $journeys) {
                    $venuesIDs = ArrayHelper::merge($venuesIDs, array_filter(ArrayHelper::getColumn($journeys, 'id')));
                    foreach ($journeys as $indexVenue => $venue) {
                        array_push($list_kecamatan, $venue['kecamatan']);
                        $data['SpgVenue'] = $venue;
                        $modelVenue = (isset($venue['id']) && isset($oldVenues[$venue['id']])) ? $oldVenues[$venue['id']] : new SpgVenue();
                        $modelVenue->load($data);
                        $modelVenue->journey_id = (int)$_POST['SpgJourney'][0]['id'];
                        $modelsSpgVenue[$indexJourney][$indexVenue] = $modelVenue;
                        $valid = $modelVenue->validate();
                    }
                }
            }

            $oldVenuesIDs = ArrayHelper::getColumn($oldVenues, 'id');
            $deletedVenuesIDs = array_diff($oldVenuesIDs, $venuesIDs);

            if ($valid) {
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    $journey = $this->findModel($id);
                    $journey->week = $_POST['SpgJourney'][0]['week'];
                    $journey->journey_date = $_POST['SpgJourney'][0]['journey_date'];
                    $journey->stock = $_POST['SpgJourney'][0]['stock'];
                    $journey->kecamatan = implode(",", array_unique($list_kecamatan));

                    if ($flag = $journey->save(false)) {

                        if (!empty($deletedVenuesIDs)) {
                            SpgVenue::deleteAll(['id' => $deletedVenuesIDs]);
                        }

                        foreach ($modelsSpgJourney as $indexJourney => $modelJourney) {
                            if ($flag === false) {
                                break;
                            }

                            if (isset($modelsSpgVenue[$indexJourney]) && is_array($modelsSpgVenue[$indexJourney])) {
                                foreach ($modelsSpgVenue[$indexJourney] as $indexJourney => $modelVenue) {
                                    $modelVenue->journey_id = $modelJourney->id;
                                    $modelVenue->created_at = date('Y-m-d H:i:s');
                                    if (!($flag = $modelVenue->save(false))) {
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    if ($flag) {
                        $transaction->commit();
                        Yii::$app->session->setFlash('info', MyFormatter::success('<strong>Selamat!</strong> Data telah tersimpan.'));
                        return $this->redirect(['index']);
                    } else {
                        $transaction->rollBack();
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                }
            }
        }

        return $this->render('update', [
            'modelSpg' => $modelSpg,
            'kabupaten' => $kabupaten,
            'modelsSpgJourney' => $modelsSpgJourney,
            'modelsSpgVenue' => (empty($modelsSpgVenue)) ? [[new SpgVenue()]] : $modelsSpgVenue,
        ]);
    }

    /**
     * Deletes an existing SpgJourney model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SpgJourney model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SpgJourney the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SpgJourney::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
?>