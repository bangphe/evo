<?php

namespace app\controllers;

use Yii;
use app\models\SpgLocation;
use app\models\Spg;
use app\models\SpgSurvey;
use app\models\Team;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\MyFormatter;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\helpers\Html;

/**
 * TrackingController implements the CRUD actions for SpgLocation model.
 */
class TrackingController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                // 'ruleConfig' => [
                //     'class' => AccessRule::className(),
                // ],
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'getdata', 'view', 'delete', 'export', 'exportcsv'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
    }

    /**
     * Lists all SpgSurvey models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => SpgSurvey::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionGetdata()
    {
        $role = Yii::$app->user->identity->role->role;
        $team_id = Yii::$app->user->identity->team_id;
        
        if ($role == 'super-admin') {
            $commandLokasi = Yii::$app->db->createCommand("SELECT * FROM spg_location WHERE id IN ( SELECT max(id) FROM spg_location GROUP BY spg_id, journey_id )")->queryAll();
            $dataLokasi = $commandLokasi; //SpgLocation::find()->all();
        } elseif ($role == 'admin-regional') {
            $locations = explode(',', Yii::$app->user->identity->location);
            $teams_id = [];
            // get query teams per location
            $query = Team::find();
            foreach($locations as $loc) {
                $query->orFilterWhere(['like', 'area', $loc]);
            }
            $teams = $query->all();
            foreach($teams as $team) {
                array_push($teams_id, $team->id);
            }

            $dataJourneyPerTeam = (new \yii\db\Query())
                ->select(['id'])
                ->from('spg_journey')
                ->where(['team_id' => $teams_id])
                ->all();
            
            $journeyIds = [];
            if ($dataJourneyPerTeam != NULL) {
                foreach($dataJourneyPerTeam as $row) {
                    array_push($journeyIds, $row['id']);
                }
            }
            
            $journeyId = "'" . implode("','", $journeyIds) . "'";
            
            $commandLokasi = Yii::$app->db->createCommand("SELECT * FROM spg_location WHERE id IN ( SELECT max(id) FROM spg_location GROUP BY spg_id, journey_id ) AND journey_id IN (" . $journeyId . ") ")->queryAll();
            $dataLokasi = $commandLokasi;
        } else {
            $dataJourneyPerTeam = (new \yii\db\Query())
                ->select(['id'])
                ->from('spg_journey')
                ->where(['team_id' => $team_id])
                ->all();
            
            $journeyIds = [];
            if ($dataJourneyPerTeam != NULL) {
                foreach($dataJourneyPerTeam as $row) {
                    array_push($journeyIds, $row['id']);
                }
            }
            $journeyId = "'" . implode("','", $journeyIds) . "'";
            
            $commandLokasi = Yii::$app->db->createCommand("SELECT * FROM spg_location WHERE id IN ( SELECT max(id) FROM spg_location GROUP BY spg_id, journey_id ) AND journey_id IN (" . $journeyId . ") ")->queryAll();
            $dataLokasi = $commandLokasi;
        }

        $result = [];
        $data = [];
        $i = 1;
        foreach ($dataLokasi as $value) {
            $spg = Spg::findOne($value['spg_id']);
            $team = Team::findOne($spg['team_id']);

            $nestedData = [];
            $nestedData[] = $i;
            $nestedData[] = $spg['nama_spg'].' | '.$team['nama_team'];
            // $nestedData[] = $value->team['nama_team'];
            $nestedData[] = MyFormatter::formatTanggalSingkat($value['created_at']);
            $nestedData[] = $value['kecamatan_survey'];
            $nestedData[] = $value['latitude'];
            $nestedData[] = $value['longitude'];
            $nestedData[] = Html::a('<i class="fa fa-search"></i>',['view','id'=>$value['id']],['class'=>'btn btn-primary m-btn m-btn--icon m-btn--icon-only']);

            $data[] = $nestedData;
            $i++;
        }

        $result = array(
            "data" => $data,
        );

        // echo json_encode($result);
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $result;
    }

    public function actionView($id)
    {
        // $model = SpgLocation::find()
        //     ->andWhere(['>=', 'created_at', $myDate])
        //     ->andWhere(['status' => 'active'])
        //     ->andWhere(['user_id' => Yii::$app->user->identity->id])
        //     ->all();
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the Spg model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Spg the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SpgLocation::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
?>